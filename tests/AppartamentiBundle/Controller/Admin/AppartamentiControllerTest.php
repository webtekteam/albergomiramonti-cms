<?php
// 22/04/17, 9.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Tests\AppartamentiBundle\Tests\Controller;

use Tests\AppBundle\Base\BaseController;

class AppartamentiControllerTest extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/appartamenti');
        $this->addUrl('listJson', '/admin/appartamenti/json');
        $this->addUrl('delete', '/admin/appartamenti/delete/{id}');
        $this->addUrl('delete', '/admin/appartamenti/delete/{id}/force');
        $this->addUrl('restore', '/admin/appartamenti/restore/{id}');
        $this->addUrl('new', '/admin/appartamenti/new');
        $this->addUrl('edit', '/admin/appartamenti/edit/{id}');

    }

}