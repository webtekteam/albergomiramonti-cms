<?php
// 22/04/17, 9.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Tests\TagBundle\Tests\Controller;

use Tests\AppBundle\Base\BaseController;

class CategoriaTagControllerTest extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/categorie-tag');
        $this->addUrl('listJson', '/admin/categorie-tag/json');
        $this->addUrl('delete', '/admin/categorie-tag/delete/{id}');
        $this->addUrl('delete', '/admin/categorie-tag/delete/{id}/force');
        $this->addUrl('restore', '/admin/categorie-tag/restore/{id}');
        $this->addUrl('new', '/admin/categorie-tag/new');
        $this->addUrl('edit', '/admin/categorie-tag/edit/{id}');

    }

}