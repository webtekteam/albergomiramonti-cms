<?php
// 27/04/17, 12.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Tests\AppartamentiBundle\Tests\Controller;


use Tests\AppBundle\Base\BaseController;

class GroupAttributeController extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/brands');
        $this->addUrl('listJson', '/admin/brands/json');
        $this->addUrl('delete', '/admin/brands/toggle-enabled/{id}');
        $this->addUrl('delete', '/admin/brands/delete/{id}');
        $this->addUrl('delete', '/admin/brands/delete/{id}/force');
        $this->addUrl('restore', '/admin/brands/restore/{id}');
        $this->addUrl('new', '/admin/brands/new');
        $this->addUrl('edit', '/admin/brands/edit/{id}');

    }

}