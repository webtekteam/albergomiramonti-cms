<?php
/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Tests\AppBundle\Base;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @coversNothing
 */
class BaseController extends WebTestCase
{

    /**
     * @var Client
     */
    protected $loggedSuperAdmin = false;
    protected $unloggedClient = false;

    protected $urls = false;

    protected $notices;

    private $Item;

    private $searchUrl = [];
    private $replaceUrl = [];

    public function __destruct()
    {

        if (count($this->notices)) {
            $className = get_class($this);
            $className = explode('\\', $className);
            $className = array_pop($className);
            echo "\033[31m";
            echo "\n";
            echo $className;
            echo "\n";
            foreach ($this->notices as $note) {
                echo $note;
                echo "\n";
            }
        }
    }

    public function setUp()
    {

        $this->loggedSuperAdmin = static::createClient(
            [],
            [
                'PHP_AUTH_USER' => 'system',
                'PHP_AUTH_PW' => 'webtek1216;',
            ]
        );
        $this->unloggedClient = static::createClient();
        $this->loggedSuperAdmin->followRedirects();
        $this->setUpCrudController();
        $this->getFirstOfJson();
    }

    public function addUrl($key, $url)
    {

        $this->urls[$key] = $url;
    }

    public function setUpCrudController()
    {
    }

    public function testAccessDenied()
    {

        if ($this->urls) {
            foreach ($this->urls as $k => $url) {
                $replacedUrl = $this->replaceUrl($url);
                if ($replacedUrl) {
                    /* @var Crawler $crawler */
                    $crawler = $this->unloggedClient->request('GET', $replacedUrl);
                    $this->assertFalse(
                        $this->unloggedClient->getResponse()->isSuccessful(),
                        'L\'utente non loggato non deve poter accedere a ' . $url
                    );
                } else {
                    $this->notices[] = __FUNCTION__ . ': Non posso testare l\'url ' . $url;
                }
            }
        } else {
            $this->notices[] = __FUNCTION__ . ': Nessun url in elenco';
        }
    }

    public function testAccessGranted()
    {

        if ($this->urls) {
            foreach ($this->urls as $k => $url) {
                $replacedUrl = $this->replaceUrl($url);
                if ($replacedUrl) {
                    /* @var Crawler $crawlerSA */
                    $crawlerSA = $this->loggedSuperAdmin->request('GET', $replacedUrl);
                    $this->assertTrue(
                        $this->loggedSuperAdmin->getResponse()->isSuccessful(),
                        'L\'utente super admin dovrebbe poter accedere a ' . $url
                    );
                } else {
                    $this->notices[] = __FUNCTION__ . ': Non posso testare l\'url ' . $url;
                }
            }
        } else {
            $this->notices[] = __FUNCTION__ . ': Nessun url in elenco';
        }
    }

    private function getFirstOfJson()
    {

        if (isset($this->urls['listJson'])) {
            $crawler = $this->loggedSuperAdmin->request('GET', $this->urls['listJson']);
            $this->assertTrue(
                $this->loggedSuperAdmin->getResponse()->isSuccessful(),
                'Non ho ricevuto risposta corretta all\'url ' . $this->urls['listJson']
            );
            $this->assertTrue(
                $this->loggedSuperAdmin->getResponse()->headers->contains(
                    'Content-Type',
                    'application/json'
                ),
                'mi aspettavo il "Content-Type:application/json"'
            );
            $pageContent = $this->loggedSuperAdmin->getResponse()->getContent();
            $jsonList = json_decode($pageContent, true);
            if (isset($jsonList['data'][0])) {
                $this->Item = $jsonList['data'][0];
            }
            $this->searchUrl = ['{id}'];
            if ($this->Item && isset($this->Item['id'])) {
                $this->replaceUrl = [$this->Item['id']];
            } else {
                $this->replaceUrl = [false];
            }
        } else {
            $this->notices[] = __FUNCTION__ . ': Non posso testare l\'elenco in json del CRUD';
        }
    }

    private function replaceUrl($url)
    {

        if (strpos($url, '{id}')) {
            if ($this->replaceUrl[0]) {
                return str_replace($this->searchUrl, $this->replaceUrl, $url);
            }

            return false;
        }

        return $url;
    }
}
