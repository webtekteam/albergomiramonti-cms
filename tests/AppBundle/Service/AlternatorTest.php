<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace Tests\AppBundle\Service;

use AppBundle\Service\Alternator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @coversNothing
 */
class AlternatorTest extends KernelTestCase
{
    /** @var Alternator $service */
    private $service;

    /** @var ObjectManager $em */
    private $em;

    protected function setUp()
    {
        self::bootKernel();

        $container = static::$kernel->getContainer();

        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->service = $container->get('app.alternator');
    }

    public function testRun()
    {
        $home = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('home');
        $page_few_locales_w_hreflang = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('few_locales_w_hreflang');
        $page_few_locales_wo_hreflang = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('few_locales_wo_hreflang');
        $page_single_locale = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('single_locale');

        $home_result = [
            'en' => '/en',
            'it' => '/',
            'de' => '/de',
            'fr' => '/fr',
            'x-default' => '/en',
        ];
        $page_few_locales_w_hreflang_result = [
            'en' => '/en/few-locales-w-hreflang',
            'it' => '/poche-lingue-con-hreflang',
            'fr' => '/fr/few-locales-w-hreflang',
            'x-default' => '/en/few-locales-w-hreflang',
        ];
        $page_few_locales_wo_hreflang_result = [
            'it' => '/poche-lingue-senza-hreflang',
            'de' => '/de/few-locales-wo-hreflang',
            'fr' => '/fr/few-locales-wo-hreflang',
            'x-default' => '/poche-lingue-senza-hreflang',
        ];

        $res = $this->service->generateAlts($home);
        $this->assertSame($home_result, $res);

        $res = $this->service->generateAlts($page_few_locales_w_hreflang);
        $this->assertSame($page_few_locales_w_hreflang_result, $res);

        $res = $this->service->generateAlts($page_few_locales_wo_hreflang);
        $this->assertSame($page_few_locales_wo_hreflang_result, $res);

        $res = $this->service->generateAlts($page_single_locale);
        $this->assertSame(2, count($res));
    }
}
