<?php
$file = realpath(__DIR__.'/../../../current/clearopcache');
if (file_get_contents($file) && function_exists('opcache_reset')) {
    opcache_reset();
    file_put_contents($file, 0);
    file_put_contents(__DIR__.'/../../../cache.log', date('d-m-Y H:i:s').' - cache cleared'."\n", FILE_APPEND);
}

use Symfony\Component\HttpFoundation\Request;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../var/bootstrap.php.cache';
$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);
date_default_timezone_set('Europe/Rome');
// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
