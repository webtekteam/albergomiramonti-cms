$(function() {

  if (typeof(Cookies.get('cookie-consent-webtek')) == 'undefined') {

    HandleBarHelper.compile('//public/lib/cookieBar.jstpl', function(compiled) {

      lang = '';
      var cookieParams = {
        'read': _('Leggi'),
        'link': lang + '/cookies',
        'accept': _('Accetta'),
        'message': _('Questo sito fa utilizzo di cookie per migliorare la navigazione')
      };

      $(compiled(cookieParams)).appendTo('body');

      $(window).one('scroll', function(e) {
        doConsent();
      });

      $(document).on('click', function(e) {
        doConsent();
      });

    });


  }

});

function doConsent() {
  Cookies.set('cookie-consent-webtek', 'yes', { expires: 30 });
  $('#cookieBar').remove();
  if (typeof('afterCookieConsent')) {
    afterCookieConsent();
  }
}