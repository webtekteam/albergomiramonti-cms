var video_element;

$(function() {

  if ($('#scroll_button').find('a').attr('href') == '#') {
    $('#scroll_button a').on('click', function(e) {
      $('html, body').
          animate({
            scrollTop: $(this).closest('section').height() -
            $('.navbar-default').height(),
          }, 500, 'linear');
    });
  } else {
    $('#scroll_button a').on('click', function(e) {
      e.preventDefault();
      $('html, body').
          animate({
            scrollTop: $($(this).attr('href')).offset().top -
            $('.navbar-default').height(),
          }, 500, 'linear');
    });
  }
  if ($('.text_with_background_video').length) {
    video_element = $('.text_with_background_video video');
    if (window.matchMedia('(max-width: 640px)').matches) {
      $('.text_with_background_video video').remove();
    } else {
      if (!$('.text_with_background_video video').length)
        video_element.insertBefore(
            '.text_with_background_video .overlay_video');
      video_element.get(0).play();
    }
  }
});

$(window).resize(function(event) {
  if ($('.text_with_background_video').length) {
    if (window.matchMedia('(max-width: 640px)').matches) {
      $('.text_with_background_video video').remove();
    } else {
      if (!$('.text_with_background_video video').length)
        video_element.insertBefore(
            '.text_with_background_video .overlay_video');
      video_element.get(0).play();
    }
  }
});
