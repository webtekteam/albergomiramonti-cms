$(function() {

  if ($('#' + instance).find('#link_button').find('a').attr('href') == '#') {
    $('#link_button a').on('click', function(e) {
      $('html, body').
          animate({scrollTop: $(this).closest('section').height() - 111}, 800,
              'linear');
    });
  }

  if ($('#' + instance).length) {
    AOS.init({
      disable: 'mobile',
    });
  }

});
