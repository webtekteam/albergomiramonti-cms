$(document).ready(function() {
  $('#' + instance + ' .menu-laterale .canOpen > a').click(function(e) {
    e.preventDefault();
    $(this).parent('li').toggleClass('opened');
  });

  $('#' + instance + ' .menuSelectMobile').change(function() {
    window.location.href = $(this).val();
  });
});