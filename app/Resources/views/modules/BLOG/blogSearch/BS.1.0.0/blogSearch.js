$(function() {
  var domElement = $('#' + instance);
  if (domElement.length) {
    function search() {
      var query = domElement.find('#query').val();
      window.location.href = '/blog/search?query=' +
          encodeURIComponent(domElement.find('#query').val());
    }

    domElement.find('#query').on('keypress', function(e) {
      if (e.keyCode == 13) {
        search();
      }
    });
    domElement.find('i').click(function() {
      search();
    });
  }
});
