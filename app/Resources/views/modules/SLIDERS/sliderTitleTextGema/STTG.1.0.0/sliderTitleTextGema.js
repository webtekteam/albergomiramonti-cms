/**
 * Created by gabricom on 22/03/2017.
 */
$(function() {
  if ($('#' + instance).length) {
    var instanza = $('#' + instance);

    instanza.find('.slider').slick({
      'arrows': true,
      'slidesToShow': 2,
      'prevArrow': $('#' + instance + ' .slick-precedente'),
      'nextArrow': $('#' + instance + ' .slick-successivo'),
      'infinite': false,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
          },
        },
      ],
    });
    objectFitImages();
    $('#' + instance + ' .match').matchHeight();
    $(window).resize();
    AOS.init({
      disable: 'mobile',
    });
  }
});
