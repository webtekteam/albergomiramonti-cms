$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).
        find('.sezione.thumbnails').
        on('mouseenter', '.col-lg-3', function(event) {
          event.preventDefault();
          $(this).find('.box-hover').stop(true, true).fadeIn('slow');
        });

    $('#' + instance).
        find('.sezione.thumbnails').
        on('mouseleave', '.col-lg-3', function(event) {
          event.preventDefault();
          $(this).find('.box-hover').stop(true, true).fadeOut('slow');
        });

  }

});
