$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find('.slider-customers').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      infinite: false,
      arrows: true,
      prevArrow: '<i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i>',
      nextArrow: '<i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>',
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 499,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }
});