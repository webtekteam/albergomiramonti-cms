$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find('.slider').on('init', function(event) {
      event.preventDefault();

      AOS.init();
    });

    $('#' + instance).find('.slider-cards .slider').slick({
      slidesToShow: 3,
      centerMode: true,
      arrows: false,
      infinite: true,
      dots: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            centerMode: true,
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 991,
          settings: {
            centerMode: false,
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 767,
          settings: {
            centerMode: false,
            slidesToShow: 1,
          },
        },
      ],
    });

    $('#' + instance).find('.thumbnail').matchHeight();
  }

});