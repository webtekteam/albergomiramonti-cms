<?php
// 06/12/17, 10.44
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Behat\AziendaBundle\Admin\Context;

use Behat\AppBundle\Traits\Behat;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;

class AziendaContext extends RawMinkContext implements Context
{

    use KernelDictionary;
    use Behat;

    /**
     * @BeforeScenario
     */
    public function clearData()
    {

        $this->deleteByEmail('gianiaz+1@gmail.com');
    }

    private function deleteByEmail($email)
    {

        $em = $this->getEntityManager();
        $User = $em->getRepository('AppBundle:User')->findOneBy(['username' => $email]);
        if ($User) {
            $Anagrafica = $em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(
                ['user' => $User]
            );
            if ($Anagrafica) {
                // initiate an array for the removed listeners
                $originalEventListeners = [];
                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {
                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;
                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }
                }
                // remove the entity
                $em->remove($Anagrafica);
            }
            $em->remove($User);
        }
        $em->flush();
    }

}