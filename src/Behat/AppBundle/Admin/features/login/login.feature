Feature: Authentication
  In order to gain access to the management area
  As an admin user
  I need to be able to login and logout

  Scenario: Log in
    Given there is an admin user "admin" with password "admin"
    Given I am on the login page
    And I fill the login form with "admin" and "admin"
    Then I should see "Bentornato"