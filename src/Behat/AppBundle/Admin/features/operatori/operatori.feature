Feature: Operatori
  In order to manage users
  As an admin user
  I need to be able to access to all crud actions

  Background:
    Given there is an admin user "admin" with password "admin"
    Given I am on the login page
    And I fill the login form with "admin" and "admin"
    And I am on "/admin/"
    And I open "side-menu-amministrazione" menu
    And I wait for "side-menu-users" to appear
    And I open "side-menu-users" menu

  @javascript
  Scenario: List operatori
    Given I wait for table to load
    Then I should see "Gestione Operatori"

  @javascript
  Scenario: Create new operator
    Given I click on "nuovo"
    Then I should see "Gestione operatori: Nuovo"
    And I fill in data "nome" with "Mario"
    And I fill in data "cognome" with "Verdi"
    And I fill in data "email" with "mario@verdi.it"
    And I fill in data "username" with "marioverdi"
    And I fill in data "password1" with "webtek2015!"
    And I fill in data "password2" with "webtek2015!"
    And I press "Salva"
    Then I should see "Gestione operatori"
    And I should see "creato"

  @javascript
  Scenario: Create new user and fail
    Given I click on "nuovo"
    Then I should see "Gestione operatori: Nuovo"
    And I fill in data "nome" with "Mario"
    And I fill in data "cognome" with "Verdi"
    And I fill in data "email" with "mario@verdi.it"
    And I fill in data "username" with "marioverdi"
    And I fill in data "password1" with "webtek2015!"
    And I fill in data "password2" with "webtek2015!"
    And I press "Salva"
    Given I click on "nuovo"
    Then I should see "Gestione operatori: Nuovo"
    And I fill in data "nome" with "Mario"
    And I fill in data "cognome" with "Verdi"
    And I fill in data "email" with "mario@verdi.it"
    And I fill in data "username" with "marioverdi"
    And I fill in data "password1" with "webtek2015!"
    And I fill in data "password2" with "webtek2015!"
    And I press "Salva"
    Then I should see "Gestione operatori: Nuovo"
    Then I should see "Questo indirizzo è già presente in database"

  @javascript
  Scenario: Edit user
    Given I click on "nuovo"
    Then I should see "Gestione operatori: Nuovo"
    And I fill in data "nome" with "Mario"
    And I fill in data "cognome" with "Verdi"
    And I fill in data "email" with "mario@verdi.it"
    And I fill in data "username" with "marioverdi"
    And I fill in data "password1" with "webtek2015!"
    And I fill in data "password2" with "webtek2015!"
    And I press "Salva"
    Then I should see "Gestione operatori"
    And I should see "creato"
    Given I wait for table to load
    And I edit the "mario@verdi.it" row
    Then I should see "Gestione operatori: Modifica"
    And I fill in data "nome" with "Luigi"
    And I press "Salva"
    Then I should see "Gestione operatori"
    And I should see "modificato"

  @javascript
  Scenario: Delete an operator
    Given I click on "nuovo"
    Then I should see "Gestione operatori: Nuovo"
    And I fill in data "nome" with "Mario"
    And I fill in data "cognome" with "Verdi"
    And I fill in data "email" with "mario@verdi.it"
    And I fill in data "username" with "marioverdi"
    And I fill in data "password1" with "webtek2015!"
    And I fill in data "password2" with "webtek2015!"
    And I press "Salva"
    Then I should see "Gestione operatori"
    And I should see "creato"
    Given I wait for table to load
    And I search "mario@verdi.it" in datatable
    Given I delete the "mario@verdi.it" row
    And I wait for confirmation modal
    And I confirm the modal
    Then I should see "Gestione operatori"
    And I should see "eliminato"