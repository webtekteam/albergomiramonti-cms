<?php
/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Behat\AppBundle\Admin\Context;

use AppBundle\Entity\User;
use Behat\AppBundle\Admin\Page\LoginPage;
use Behat\AppBundle\Traits\Behat;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

class BaseContext extends RawMinkContext implements Context
{

    use KernelDictionary;
    use Behat;

    /**
     * @var LoginPage
     */
    private $loginPage;

    /**
     * BaseContext constructor.
     */
    public function __construct(LoginPage $loginPage)
    {

        $this->loginPage = $loginPage;
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {

        $em = $this->getEntityManager();
        $User = $em->getRepository('AppBundle:User')->findOneBy(['username' => 'admin']);
        if ($User) {
            $em->remove($User);
        }
        $User = $em->getRepository('AppBundle:User')->findOneBy(['username' => 'mariorossi']);
        if ($User) {
            $em->remove($User);
        }
        $em->flush();
    }

    /**
     * @Given there is an admin user :username with password :password
     *
     * @param mixed $username
     * @param mixed $password
     */
    public function thereIsAnAdminUserWithPassword($username, $password)
    {

        $user = new User();
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setRole('ROLE_SUPER_ADMIN');
        $user->setIsEnabled(true);
        $user->setNome('Mario');
        $user->setCognome('Rossi');
        $user->setEmail('mario@rossi.it');
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }

    /**
     * @Given I am on the login page
     */
    public function iAmOnTheLoginPage()
    {

        $this->visitPath('/admin/login');
    }
}
