<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Behat\AppBundle\Admin\Page;

use Behat\Mink\Element\Element;
use Behat\Mink\Element\NodeElement;
use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

require_once 'vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

class BasePage extends Page
{
    public function findXpathDataAttr($value, $element = 'name', Element $container = null, $all = false)
    {
        $method = 'find';
        if ($all) {
            $method = 'findAll';
        }
        if ($container) {
            $selector = sprintf('//*[@data-%s="%s"]', $element, $value);
            $domElement = $container->$method(
                'xpath',
                $selector
            );
        } else {
            $selector = sprintf('//*[@data-%s="%s"]', $element, $value);
            $domElement = $this->$method(
                'xpath',
                $selector
            );
        }
        assertNotNull($domElement, sprintf('Non ho trovato l\'elemento data-%s con valore %s', $element, $value));

        return $domElement;
    }

    public function findXpathDataAttrContains($value, $contains = '', $element = 'name', $container = null)
    {
        if (!$contains) {
            throwException(new \Exception('Testo vuoto'));
        }
        $selector = sprintf("//*[@data-%s=\"%s\" and contains(.,'%s')]", $element, $value, $contains);
        if ($container) {
            $domElement = $container->find(
                'xpath',
                $selector
            );
        } else {
            $domElement = $this->find(
                'xpath',
                $selector
            );
        }
        assertNotNull(
            $selector,
            sprintf(
                'Non ho trovato l\'elemento data-%s con valore %s contenente il testo "%s"',
                $element,
                $value,
                $contains
            )
        );

        return $domElement;
    }
}
