<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\EntityListeners({"AppBundle\EntityListener\NewsTranslationListener"})
 * @ORM\Table(name="news_translations")
 */
class NewsTranslation
{
    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $metaTitle;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $metaDescription;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $visits;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortUrl;

    public function isEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @return mixed
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param mixed $shortUrl
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {
        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {
        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {
        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {
        return $this->isEnabled();
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param mixed $visits
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;
    }
}
