<?php
// 03/03/17, 11.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentoRepository")
 * @ORM\Table(name="commenti")
 */
class Commento
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\SoftDeletable\SoftDeletable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\Type(
     *     type="numeric"
     * )
     * @ORM\Column(type="integer")
     */
    private $parent;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $messaggio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\News", inversedBy="commenti")
     */
    private $news;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {

        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getMessaggio()
    {

        return $this->messaggio;
    }

    /**
     * @param mixed $messaggio
     */
    public function setMessaggio($messaggio)
    {

        $this->messaggio = $messaggio;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {

        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {

        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setNews($news)
    {

        $this->news = $news;
    }


}