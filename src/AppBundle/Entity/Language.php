<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 16/12/16
 * Time: 10.16
 */

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LanguageRepository")
 * @ORM\Table(name="language")
 */
class Language
{

    use Loggable;
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=2)
     */
    private $shortCode;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $languageName;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $locale;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabledPublic;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @return mixed
     */
    public function getShortCode()
    {

        return $this->shortCode;
    }

    /**
     * @param mixed $shortCode
     */
    public function setShortCode($shortCode)
    {

        $this->shortCode = $shortCode;
    }

    /**
     * @return mixed
     */
    public function getLanguageName()
    {

        return $this->languageName;
    }

    /**
     * @param mixed $languageName
     */
    public function setLanguageName($languageName)
    {

        $this->languageName = $languageName;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {

        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {

        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getIsEnabledPublic()
    {

        return $this->isEnabledPublic;
    }

    /**
     * @param mixed $isEnabledPublic
     */
    public function setIsEnabledPublic($isEnabledPublic)
    {

        $this->isEnabledPublic = $isEnabledPublic;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }


}