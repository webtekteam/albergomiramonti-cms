<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 14.17
 */

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="utenti")
 * @Gedmo\Loggable
 * @UniqueEntity(fields={"email", "role"}, message="operatori.email_gia_usata")
 * @UniqueEntity(fields={"username"}, message="Username già utilizzato")
 * @Vich\Uploadable()
 */
class User implements UserInterface, \Serializable
{

    use Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $cognome;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned()
     */
    private $password;

    /**
     *
     * @Assert\NotBlank(groups={"Creazione"})
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Versioned()
     * @Assert\NotBlank(message = "operatori.username_not_empty")
     * @Assert\Regex("/^[a-zA-Z0-9]{5,}$/")
     */
    private $username;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned()
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned()
     */
    private $profileText;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @Vich\UploadableField(mapping="operatori", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;

    /**
     * @return string
     */
    public function getUploadDir()
    {

        return 'files/operatori/' . $this->getId() . '/';

    }

    public function getProfileText()
    {

        return $this->profileText;
    }

    public function setProfileText($profileText)
    {

        $this->profileText = $profileText;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }


    public function getId()
    {

        return $this->id;
    }

    public function getCognome()
    {

        return $this->cognome;
    }

    public function setCognome($cognome)
    {

        $this->cognome = $cognome;
    }

    public function setPassword($password)
    {

        if ($password) {
            $this->password = $password;
        }
    }

    public function getEmail()
    {

        return $this->email;
    }

    public function setEmail($email)
    {

        $this->email = $email;
    }

    public function getNome()
    {

        return $this->nome;
    }

    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    public function getUsername()
    {

        return $this->username;
    }

    public function setUsername($username)
    {

        $this->username = $username;
    }

    public function getRole()
    {

        return $this->role;
    }


    public function getPassword()
    {

        return $this->password;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {

        $this->plainPassword = null;
    }

    public function getPlainPassword()
    {

        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {

        if ($plainPassword) {
            $this->plainPassword = $plainPassword;
            // garantisce che la password venga settata dal listener di Doctrine
            $this->password = null;
        }
    }

    public function setRole($role)
    {

        $this->role = $role;
    }

    public function getUpdatedAt()
    {

        return $this->updatedAt;
    }

    public function getCreatedAt()
    {

        return $this->createdAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->updatedAt = $updatedAt;
    }

    function __toString()
    {

        return (string)$this->getNome();
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }


    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    public function getRoles()
    {

        if ($this->getRole()) {
            return [$this->getRole()];
        } else {
            return ['ROLE_USER'];
        }
    }

    public function getProfilePicture()
    {

        if ($this->getListImgFileName()) {
            return '/' . $this->getUploadDir() . $this->getListImgFileName();
        } else {
            return '/media/users/user.png';
        }
    }


    /** @see \Serializable::serialize() */
    public function serialize()
    {

        return serialize(
            [
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt,
            ]
        );
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {

        list (
            $this->id, $this->username, $this->password, // see section on salt below
            // $this->salt
            )
            = unserialize($serialized);
    }

}