<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GalleryRepository")
 * @ORM\Table(name="gallery")
 */
class Gallery
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GalleryItems", mappedBy="gallery", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GalleryCategory", inversedBy="galleries")
     */
    private $category;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sort;

    public function __construct()
    {

        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    public function __toString()
    {

        return (string)$this->getId();
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        /**
         * @var $object Gallery
         */
        $object = $context->getObject();
        $translations = $object->getTranslations();
        foreach ($translations as $translation) {
            if (false == $translation->getTitolo()) {
                $context->buildViolation('gallery.errors.titolo_empty_no_lang')->atPath(
                    'translations[' . $translation->getLocale() . '].titolo'
                )->addViolation();
            }
        }

    }

    /**
     * @return mixed
     */
    public function getItems()
    {

        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items)
    {

        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {

        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {

        $this->category = $category;
    }


}
