<?php
// 12/01/17, 16.21
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use AppBundle\Traits\Seo;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\EntityListeners({"AppBundle\EntityListener\CategoriaGalleryTranslationListener"})
 * @ORM\Table(name="gallerycategory_translations")
 */
class GalleryCategoryTranslation
{

    use ORMBehaviours\Translatable\Translation;
    use Seo;
    use Loggable;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testo;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {

        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {

        $this->testo = $testo;
    }


}