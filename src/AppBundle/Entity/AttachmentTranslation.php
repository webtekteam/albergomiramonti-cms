<?php
// 02/01/17, 11.01
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="attachments_translations")
 */
class AttachmentTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $alt;

    /**
     * @return mixed
     */
    public function getAlt()
    {

        return $this->alt;
    }

    /**
     * @param mixed $alt
     */
    public function setAlt($alt)
    {

        $this->alt = $alt;
    }
}