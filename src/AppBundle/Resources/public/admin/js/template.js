// 30/01/17, 15.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
var module_name = 'template';
var srcTimeout = false;

$(function() {

  /**
   * Click sul bottone per rimuovere un modulo da un template
   */
  $(document).on('click', '.btn-remove-module', function(e) {
    e.preventDefault();

    var $btn = $(this);
    /** La riga che rappresenta il modulo **/
    var $riga = $(this).closest('.row');
    /** Il panel in cui si trova **/
    var $panel = $riga.closest('.panel');
    /** Il nome del contenitore **/
    var nomeContainer = $panel.data('name');
    /** Il nome descrittivo del modulo **/
    var titolo = $riga.find('strong').text();
    /** tupla di template e contenitore **/
    var tpl = $('.btn-template.btn-success').data('ref').split('-');
    var tipo = '';
    switch (tpl[1]) {
      case 'CONTENT':
        tipo = _('Template di contenuto', module_name);
        break;
      case 'HEADER':
        tipo = _('Headers', module_name);
        break;
      case 'FOOTER':
        tipo = _('Footers', module_name);
        break;
    }

    var opts = {
      'type': 'danger',
      'titolo': _('Rimozione modulo', module_name),
      'content': _(
          'Vuoi eliminare il modulo "<strong>%s</strong>" dal container "<strong>%s</strong>" per il %s <strong>%s</strong>',
          module_name, titolo, nomeContainer, tipo, tpl[0]),
      'OK': 'Ok',
      'CANCEL': 'Annulla',
      'onOK': function($modal) {
        $modal.modal('hide');
        var opts = {
          'type': 'danger',
          'message': 'Attendere prego...',
          'icon': 'fa-spin fa-spinner',
        };
        HandleBarHelper.lockScreen();

        var url = '/admin/' + module_name + '/remove_module/' + $btn.data('id');
        $.ajax({
          url: url,
          dataType: 'json',
          success: function(ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {
              $riga.remove();
            } else {
              var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': ret_data.errors.join('<br />'),
                'OK': 'Ok',
                'callback': null,
              };
              HandleBarHelper.alert(opts);
            }
          },
        });
      },
      'onCANCEL': null,
    };
    HandleBarHelper.confirm(opts);
  });

  /** Evento custom che lancia la ricerca tra i moduli disponibili **/
  $('#srcModule').on('searchNow', function() {
    searchNow();
  });

  /** click sul bottone a X di fianco al bottone di ricerca,
   *  viene resettato il campo e tutti i moduli da mostrare di conseguenza **/
  $('.clear-src').on('click', function() {
    var $div = $(this).closest('div');
    var $input = $div.find('input');
    $input.val('');
    $input.trigger('searchNow');
  });

  /** Durante la digitazione nel campo di ricerca moduli viene fatta partire la funzione di
   * ricerca dopo un certo delay **/
  $('#srcModule').on('keyup', function(e) {
    var $input = $(this);
    clearTimeout(srcTimeout);
    srcTimeout = setTimeout(function() {
      $input.trigger('searchNow');
    }, 300);
  });

  /** Click sul bottone di un template, che scaturisce il caricamento via ajax dei moduli assegnati al
   * template selezionato
   */
  $('.btn-template').on('click', function(e) {
    e.preventDefault();

    // deseleziono l'eventuale selezione precedente.
    $('.btn-template').removeClass('btn-success');
    $('.btn-template').find('.fa').remove();

    // cambio l'aspetto del bottone selezionato
    $(this).addClass('btn-success');
    $(this).html('<i class="fa fa-check"></i>&nbsp;' + $(this).text());

    // dal data-ref del bottone selezionato estraggo i dati che mi servono
    // per reperire i moduli caricati per questo template
    var tipo = $(this).data('ref').split('-');

    var type = tipo[1];
    var template = tipo[0];

    loadModules(template, type);

  });

  // al caricamento chiamo la funzione per dare l'impostazione visiva iniziale.
  searchNow(false);
});

/**
 * La funzione search now fa una ricerca testuale nei vari div ".module" e nasconde quelli
 * che non soddisfano la ricerca.
 */
function searchNow() {

  var $input = $('#srcModule');

  $container = $('.elencoModuli');

  $container.find('.module').removeClass('hide');
  $container.find('.module').removeClass('even');

  if ($input.val()) {
    $container.find('.module').
        not(':contains(' + $input.val() + ')').
        addClass('hide');
  }

  // effetto zebra per una visualizzazione migliore
  $container.find('.module:not(.hide):even').addClass('even');

  $('.elencoModuli').find('.panel').each(function() {
    var numeroModuli = $(this).find('.module:not(".hide")').length;
    $(this).find('.badge').html(numeroModuli);
    if ($input.val()) {
      if (numeroModuli > 0) {
        $(this).find('.panel-collapse').addClass('in');
      } else {
        $(this).find('.panel-collapse').removeClass('in');
      }
    }
  });

  if (!$input.val()) {
    $('.elencoModuli').find('.panel-collapse').removeClass('in');
  }

}

/**
 * @param template nome del template (es. home, contatti ecc)
 * @param type nome del tipo di template (HEADER|FOOTER|CONTENT)
 */
function loadModules(template, type) {

  var data = {
    'type': type,
    'template': template,
  };
  $.ajax({
    type: 'POST',
    url: '/admin/' + module_name + '/load_configured_modules',
    data: data,
    dataType: 'json',
    success: function(ret_data) {
      if (ret_data.result) {

        var modules = ret_data.moduli;
        var sezioni = ret_data.sezioni;

        // contenitore generale di tutti i panel che verranno generati
        var $contentContainer = $('#contentContainer');
        // svuoto il container per resettare prima di cominciare a popolarlo
        $contentContainer.empty();

        // versione compilata del panel da mostrare per ogni contenitore
        var compiledPanel = HandleBarHelper.compile('panel');

        if (sezioni.length) {

          // per ogni sezione viene creato un panel che in seguito verrà riempito.
          for (var i = 0; i < sezioni.length; i++) {
            $(compiledPanel({'title': sezioni[i]})).appendTo($contentContainer);
          }

          // versione compilata della riga di un singolo modulo
          var compiledModulo = HandleBarHelper.compile('rigaModulo');

          // per ogni modulo viene appeso al relativo panel precedentemente creato
          for (var i in modules) {
            var modulo = modules[i];
            console.log(modulo);
            $(compiledModulo(modulo)).
                appendTo($('#container-' + modulo['parent']));
          }

          var idPannelli = [];

          // iniziliazzazione del sortable sul panel per permettere il riordino dei moduli
          // all'interno del panel
          $('#contentContainer').find('.panel-body').each(function() {
            var $panel = $(this);
            idPannelli.push('#' + $panel.attr('id'));
            $panel.sortable({
              placeholder: 'ui-state-highlight',
              handle: '.btn-move',
              receive: function(event, ui) {
                $helper = ui.helper;
                $helper.removeClass('ui-draggable');
                $helper.removeAttr('style');
                $helper.find('div:eq(0)').
                    removeClass('col-md-4').
                    addClass('col-md-2');
                $helper.find('div:eq(1)').
                    removeClass('col-md-6').
                    addClass('col-md-9');
                $helper.find('div:eq(2)').
                    removeClass('col-md-2').
                    addClass('col-md-1 text-right btn-container');
                $('<a class="btn btn-danger btn-remove-module"><i class="fa fa-trash"></i></a>').
                    appendTo($helper.find('.col-md-1'));
                $('<a href="#" target="_blank" class="btn btn-warning btn-edit"><i class="fa fa-pencil"></i></a>').
                    appendTo($helper.find('.col-md-1'));
                insertModule($helper.data('code'),
                    $panel.closest('.panel').data('name'),
                    $('.btn-template.btn-success').data('ref'), $helper);
              },
              stop: function(event, ui) {
                var doAjaxCall = true;
                $panel.find('.module').each(function() {
                  if (typeof($(this).attr('id')) == 'undefined') {
                    doAjaxCall = false;
                    return false;
                  }
                });

                if (doAjaxCall) {
                  saveSortStatus($panel);
                }
              },

            });
          });

          // abilito le ancore per il drag dei moduli disponibili
          $('.elencoModuli').find('.btn-move').removeClass('disabled');

          //$('.elencoModuli').draggable('destroy');
          console.log(idPannelli.join(','));
          $('.elencoModuli .row').draggable({
            'handle': '.btn-move',
            'connectToSortable': idPannelli.join(','),
            'appendTo': 'body',
            'revert': 'invalid',
            'start': function(event, ui) {
              ui.helper.removeClass('even');
              ui.helper.css({
                'width': $('.elencoModuli').
                    find('.row:visible:first').
                    width(),
              });
            },
            'helper': 'clone',
            /*
            'helper': function() {
              $('<div style="width:400px" id="helper"></div>').appendTo('body');
              return $('#helper').get(0);
            }*/
          });

          console.log(idPannelli);

        } else {
          var opts = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': _(
                'Il template selezionato non contiene nessun pannello configurabile, ricontrolla il twig',
                module_name),
            'OK': 'Ok',
            'callback': null,
          };
          HandleBarHelper.alert(opts);

        }

      } else {

        var opts = {
          'type': 'danger',
          'titolo': _('Attenzione'),
          'content': ret_data.errors.join('<br />'),
          'OK': 'Ok',
          'callback': null,
        };
        HandleBarHelper.alert(opts);

      }
    },
  });

}

function insertModule(codiceModulo, nomePannello, tuplaTemplate) {

  var triplaNome = codiceModulo.split('|');
  category = triplaNome[0];
  codiceModulo = triplaNome[2];
  nomeModulo = triplaNome[1];
  tuplaTemplate = tuplaTemplate.split('-');
  var template = tuplaTemplate[0];
  var type = tuplaTemplate[1];

  console.log(codiceModulo, nomePannello, template, type);

  var data = {
    'module': nomeModulo,
    'category': category,
    'codice': codiceModulo,
    'container': nomePannello,
    'template': template,
    'type': type,
  };

  $.ajax({
    type: 'POST',
    url: '/admin/' + module_name + '/insert_template',
    data: data,
    dataType: 'json',
    success: function(ret_data) {
      if (ret_data.result) {
        $helper.attr('id', 'sort_' + ret_data.id);
        $helper.find('.btn-remove-module').data('id', ret_data.id);
        $helper.find('.btn-edit').
            attr('href', '/admin/layoutcontent/edit/' + ret_data.id);
        saveSortStatus($helper.closest('.panel-body'));
      } else {
        var opts = {
          'type': 'danger',
          'titolo': _('Attenzione'),
          'content': ret_data.errors.join('<br />'),
          'OK': 'Ok',
          'callback': null,
        };
        HandleBarHelper.alert(opts);
      }
    },
  });
}

function saveSortStatus($panel) {

  var sorted = $panel.sortable('serialize');
  var opts = {
    'type': 'danger',
    'message': _('Attendere prego...'),
    'icon': 'fa-spin fa-spinner',
  };
  HandleBarHelper.lockScreen();
  console.log(sorted);
  $.ajax({
    type: 'POST',
    url: '/admin/' + module_name + '/save_sort_order',
    data: sorted,
    dataType: 'json',
    success: function(ret_data) {
      if (ret_data.result) {
        HandleBarHelper.unlockScreen();
      } else {
        var opts = {
          'type': 'danger',
          'titolo': _('Attenzione'),
          'content': ret_data.errors.join('<br />'),
          'OK': 'Ok',
          'callback': null,
        };
        HandleBarHelper.alert(opts);
      }
    },
  });

}