<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use AppBundle\Entity\Template;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class TemplateLoader
{
    /**
     * @var EntityManager
     */
    private $em;

    private $rootDir;

    private $webDir;

    /**
     * @var Languages
     */
    private $languages;

    private $locale;

    private $env;

    private $AdditionalData;

    private $layout;

    private $sass;

    private $FormatterContainer;

    private $twig;

    private $isAdmin;

    /**
     * TemplateLoader constructor.
     */
    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->rootDir = $this->container->get('kernel')
            ->getRootDir();
        $this->webDir = $this->container->get('app.web_dir')
            ->get();
        $this->em = $this->container->get('doctrine')
            ->getManager();
        $this->twig = $this->container->get('templating');

        $this->request = $requestStack->getCurrentRequest();
        $this->locale = $this->request->getLocale();

        $this->layout = $this->container->getParameter('generali')['layout'];
        $this->sass = $this->container->getParameter('parametri-avanzati')[('sass')];

        $this->authorizationChecker = $this->container->get('security.authorization_checker');

        $this->isAdmin = $this->authorizationChecker->isGranted('ROLE_MANAGE_LAYOUT_CONTENT');
    }

    public function getTwigs($tpl, $langs, $AdditionalData = [])
    {
        $this->AdditionalData = $AdditionalData;

        // dump($tpl, json_encode($langs), $AdditionalData);

        $repo = $this->em->getRepository('AppBundle:Template');

        $sections = ['HEADER', 'CONTENT', 'FOOTER'];

        $structure = [];
        $structure['sections'] = [];
        $structure['assets'] = [];
        $structure['assets'] = $this->loadAssets();

        $data = [];

        foreach ($sections as $type) {
            $structure['sections'][$type]['twig'] = $this->getTypeFileName($type, $tpl);
            $structure['sections'][$type]['widgets'] = [];

            $filterTpl = basename($structure['sections'][$type]['twig']);
            $filterTpl = explode('.', $filterTpl);
            $filterTpl = array_shift($filterTpl);

            $filters = ['template' => $filterTpl, 'type' => $type, 'isEnabled' => 1, 'layout' => $this->layout];

            // dump($filters);

            $data = array_merge($data, $repo->findBy($filters, ['position' => 'ASC']));
        }

        foreach ($data as $template) {
            if (!isset($structure['sections'][$template->getType()]['widgets'][$template->getContainer()])) {
                $structure['sections'][$template->getType()]['widgets'][$template->getContainer()] = [];
            }

            $widget = $this->getWidget($template, $langs);

            if (isset($widget['assets']['js']) && is_array($widget['assets']['js'])) {
                $structure['assets']['js'] = array_merge($structure['assets']['js'], $widget['assets']['js']);
            }
            if (isset($widget['assets']['css']) && is_array($widget['assets']['css'])) {
                $structure['assets']['css'] = array_merge($structure['assets']['css'], $widget['assets']['css']);
            }

            $structure['sections'][$template->getType()]['widgets'][$template->getContainer()][] = $widget;
        }

        foreach ($sections as $section) {
            if (!isset($structure['sections'][$section])) {
                $structure['sections'][$section] = ['twig' => $this->getTypeFileName($section, $tpl), 'widgets' => []];
            }
        }

        if (!isset($structure['assets']['css']) || !$structure['assets']['css']) {
            $structure['assets']['css'] = [];
        }
        if (!isset($structure['assets']['js']) || !$structure['assets']['js']) {
            $structure['assets']['js'] = [];
        }

        $structure['assets']['css'] = array_unique($structure['assets']['css']);
        $structure['assets']['js'] = array_unique($structure['assets']['js']);

        return $structure;
    }

    public function getWidget(Template $template, $langs, $env = 'public')
    {
        $this->languages = $langs;
        $this->env = $env;

        $widget = [];
        $widget['name'] = $template->getModule();
        $widget['id'] = $template->getId();
        $widget['category'] = $template->getCategory();
        $widget['instance'] = $template->getInstance();
        $widget['requiredRole'] = $template->getRequiredRole();
        $widget['config'] = $template->getConfig();
        $widget['config']['POSITION'] = $template->getPosition();
        $widget['codice'] = $template->getCodice();
        //$widget['assets'] = (isset($widget[]))[];
        $this->compile($widget, $env);

        return $widget;
    }

    public function processBackendData($backend_data, $config)
    {
        if (!isset($backend_data['class']) || !$backend_data['class']) {
            return;
        }

        $class = $backend_data['class'];

        $method = 'getData';
        if (isset($backend_data['method'])) {
            $method = $backend_data['method'];
        }

        $refetch = false;
        if (isset($backend_data['refetch']) && $backend_data['refetch']) {
            $refetch = $backend_data['refetch'];
        }

        if ($refetch || !isset($this->FormatterContainer[$class])) {
            $Obj = new $class($this->container, $this->request, $config, $this->AdditionalData);

            $this->FormatterContainer[$class] = $Obj;
        } else {
            // se l'oggetto l'ho già creato ce l'ho qui, cosi mi posso passare dati da un blocco all'altro
            $Obj = $this->FormatterContainer[$class];

            $Obj->setRequest($this->request);
            $Obj->setWidgetConfig($config);
            $Obj->setAdditionalData($this->AdditionalData);
        }

        $data = $Obj->$method();

        return $data;
    }

    public function compileStyle(&$widget, $vars = [])
    {
        $tempDir = $this->rootDir . '/../var/tmp/';

        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0755, true);
        }

        $tempDir = realpath($tempDir);

        $finder = new Finder();
        $fs = new Filesystem();

        $directories = $finder->directories()
            ->date('until 2 hours ago')
            ->in($tempDir);

        $directoryDaEliminare = [];

        foreach ($directories as $directory) {
            $directoryDaEliminare[] = $directory->getRealPath();
        }

        foreach ($directoryDaEliminare as $directory) {
            $fs->remove($directory);
        }

        $scssModuleFile = $widget['directory'] . '/' . $widget['name'] . '.scss';

        if (file_exists($scssModuleFile)) {
            $tempScssFile = $tempDir . '/' . session_id() . '.scss';
            $tempCssFile = $tempDir . '/' . session_id() . '.css';

            $scssContent = file_get_contents($widget['directory'] . '/' . $widget['name'] . '.scss');

            if ($vars) {
                $scssContent = strtr($scssContent, $vars);
            }

            $tempFileContent = '@import "' . str_replace('\\', '/',
                    $this->rootDir) . '/Resources/views/public/layouts' . '/' . $this->layout . '/styles/common/variables";';
            $tempFileContent .= "\n";
            $tempFileContent .= '#' . $widget['name'] . '_' . $widget['instance'] . ' {';
            $tempFileContent .= "\n";
            $tempFileContent .= $scssContent;
            $tempFileContent .= '}';

            file_put_contents($tempScssFile, $tempFileContent);

            $cmd = $this->sass . ' --sourcemap=none -t compressed ' . $tempScssFile . ' ' . $tempCssFile;

            $process = new Process($cmd);
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $widget['css'] = file_get_contents($tempCssFile);
        } else {
            $widget['css'] = '';
        }
    }

    private function getTypeFileName($type, $tpl = 'default')
    {
        if (!in_array($type, ['HEADER', 'CONTENT', 'FOOTER'])) {
            throw new \Exception("Il type passato non è valido ('.$type.')");
        }

        $tpl = preg_replace('/[^A-Za-z0-9_\-]/', '', basename($tpl));

        $file = $this->rootDir . '/' . 'Resources/views/public/layouts' . '/' . $this->layout . '/' . $type . '/' . $tpl . '.html.twig';

        if (file_exists($file)) {
            return str_replace($this->rootDir . '/' . 'Resources/views' . '/', '', $file);
        }

        return 'public/layouts' . '/' . $this->layout . '/' . $type . '/' . 'default.html.twig';
    }

    private function mergeCampo($campo, $language, &$widget, $debug = false)
    {
        $grouped = isset($widget['meta']['config'][$campo]['group']) ? $widget['meta']['config'][$campo]['group'] : false;

        switch ($widget['meta']['config'][$campo]['type']) {
            case 'boolean':
            case 'simpleText':
            case 'simpleTextarea':
            case 'mediumText':

                $widget['config'][$language][$campo] = $this->mergeText($campo, $language, $widget, null, $debug);

                if ($grouped) {
                    $widget['config'][$language][$grouped][$campo] = $widget['config'][$language][$campo];
                }

                break;
            case 'link':
                $this->mergeLink($campo, $language, $widget, null, $debug);

                break;
            case 'file':
            case 'backgroundImage':
            case 'image':
                $data = $this->mergeBGImage($campo, $language, $widget, null, $debug);
                if ($data) {
                    $widget['config'][$language][$campo] = $data;
                }
                if ($grouped) {
                    if ($data) {
                        $widget['config'][$language][$grouped][$campo] = $data;
                    }
                }

                break;
            case 'arrayOf':
                $this->mergeArrayOf($campo, $language, $widget, null, $debug);

                break;
        }
    }

    private function mergeText($campo, $language, &$widget, $presetData = null, $debug = false)
    {
        $default = ['val' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'];

        if (isset($widget['meta']['config'][$campo]['default'])) {
            $default['val'] = $widget['meta']['config'][$campo]['default'];
        }

        $multiLang = isset($widget['meta']['config'][$campo]['multilang']) && $widget['meta']['config'][$campo]['multilang'];
        $grouped = isset($widget['meta']['config'][$campo]['group']) ? $widget['meta']['config'][$campo]['group'] : false;
        $mandatory = isset($widget['meta']['config'][$campo]['mandatory']) && $widget['meta']['config'][$campo]['mandatory'];

        if ($debug) {
        }

        $data = false;

        if ('it' == $language || $multiLang) {
            if ($presetData) {
                $data = $presetData + $default;
            } elseif (isset($widget['config'][$language][$campo])) {
                $data = $widget['config'][$language][$campo] + $default;
            } elseif ($mandatory) {
                $data = $default;
            } else {
                $data = ['val' => false];
            }
        } else {
            if ($grouped) {
                if ($presetData) {
                    $data = $presetData + $default;
                } elseif (isset($widget['config']['it'][$campo])) {
                    $data = $widget['config']['it'][$campo] + $default;
                } else {
                    $data = ['val' => false];
                }
            } else {
                if ($presetData) {
                    $data = $presetData + $default;
                } elseif (isset($widget['config']['it'][$campo])) {
                    $data = $widget['config']['it'][$campo] + $default;
                } else {
                    $data = ['val' => false];
                }
            }
        }

        if ($debug) {
        }

        return $data;
    }

    private function mergeLink($campo, $language, &$widget, $presetData = null, $debug = true)
    {
        if ($debug) {
        }

        $default = [
            'link' => 'http://www.example.org',
            'label' => 'Example.org',
            'target' => '_self',
            'rel' => 'follow',
        ];

        $multiLang = isset($widget['meta']['config'][$campo]['multilang']) && $widget['meta']['config'][$campo]['multilang'];
        $grouped = isset($widget['meta']['config'][$campo]['group']) ? $widget['meta']['config'][$campo]['group'] : false;
        $mandatory = isset($widget['meta']['config'][$campo]['mandatory']) && $widget['meta']['config'][$campo]['mandatory'];

        if ($debug) {
        }

        $data = false;

        if ('it' == $language || $multiLang) {
            if ($presetData) {
            } elseif (isset($widget['config'][$language][$campo])) {
                $data = $widget['config'][$language][$campo] + $default;
            } elseif ($mandatory) {
                $data = $default;
            } else {
                $data = ['link' => false];
            }
        } else {
            if ($grouped) {
                if (isset($widget['config']['it'][$grouped][$campo])) {
                    $data = $widget['config']['it'][$grouped][$campo] + $default;
                } else {
                    $data = $default;
                }
            } else {
                if (isset($widget['config']['it'][$campo]) && $widget['config']['it'][$campo]) {
                    $data = $widget['config']['it'][$campo] + $default;
                } else {
                    $data = $default;
                }
            }
        }

        if ($data) {
            $widget['config'][$language][$campo] = $data;
        }

        if ($grouped) {
            if ($data) {
                $widget['config'][$language][$grouped][] = $data;
            }
        }

        if ($debug) {
        }
    }

    private function mergeBGImage($campo, $language, &$widget, $presetData = null, $debug = false)
    {
        if ($debug) {
        }

        if (!isset($widget['meta']['config'][$campo]['dimensioni'])) {
            $widget['meta']['config'][$campo]['dimensioni'] = '300x200';
        }

        $urlPlaceholder = 'http://fpoimg.com/' . $widget['meta']['config'][$campo]['dimensioni'] . '?text=WebtekCMS';

        $default = [
            'src' => false,
            'basename' => '',
        ];

        $multiLang = isset($widget['meta']['config'][$campo]['multilang']) && $widget['meta']['config'][$campo]['multilang'];
        $grouped = isset($widget['meta']['config'][$campo]['group']) ? $widget['meta']['config'][$campo]['group'] : false;
        $mandatory = isset($widget['meta']['config'][$campo]['mandatory']) && $widget['meta']['config'][$campo]['mandatory'];

        if ($debug) {
        }

        $data = false;

        if ('it' == $language || $multiLang) {
            if ($presetData) {
                $data = $presetData + $default;
                $data['basename'] = basename($data['src']);
            } elseif (isset($widget['config'][$language][$campo])) {
                $data = $widget['config'][$language][$campo] + $default;
                $data['basename'] = basename($data['src']);
            } elseif ($mandatory) {
                $data = $default;
                $data['src'] = $urlPlaceholder;
            } else {
                $data = $default;
            }
        } else {
            if ($grouped) {
                if (isset($widget['config']['it'][$campo])) {
                    $data = $widget['config']['it'][$campo] + $default;
                } else {
                    $data = $default;
                }
            } else {
                if ($presetData) {
                    $data = $presetData + $default;
                    $data['basename'] = basename($data['src']);
                } elseif (isset($widget['config']['it'][$campo]) && $widget['config']['it'][$campo]) {
                    $data = $widget['config']['it'][$campo] + $default;
                } else {
                    $data = $default;
                }
            }
        }

        $data['src'] = str_replace('\\', '/', $data['src']);

        if ($debug) {
        }

        return $data;
    }

    private function compile(&$widget)
    {
        $this->getTemplateFile($widget);

        $this->getMetaData($widget);

        $dbgField = 'qty1';
        $dbgField = '';

        if (isset($widget['meta']['config'])) {
            foreach ($widget['meta']['config'] as $k => $data) {
                foreach ($this->languages as $shortLocale => $longLocale) {
                    if (!isset($data['allow_add']) || !$data['allow_add']) {
                        $this->mergeCampo($data['var'], $shortLocale, $widget, $dbgField == $data['var']);
                    }
                }

                if ($dbgField == $data['var']) {
                    die;
                }
            }

            if (isset($widget['meta']['backEnd']) && $widget['meta']['backEnd']) {
                $backend_data_main = $widget['meta']['backEnd'];

                if (count(array_filter(array_keys($backend_data_main), 'is_string')) > 0) {
                    $data = $this->processBackendData($backend_data_main, $widget['config']);
                    $widget['config'][$this->locale]['data'] = $data;
                } else {
                    for ($x = 0; $x < count($backend_data_main); ++$x) {
                        $data = $this->processBackendData($backend_data_main[$x], $widget['config']);

                        if (array_key_exists('alias', $backend_data_main[$x])) {
                            $alias = $backend_data_main[$x]['alias'];
                        } else {
                            $alias = $backend_data_main[$x]['class'];

                            $method = 'getData';
                            if (array_key_exists('method', $backend_data_main[$x])) {
                                $method = $backend_data_main[$x]['method'];
                            }

                            $alias .= "_$method";
                        }
                        $widget['config'][$this->locale]['data'][$alias] = $data;
                    }
                }

                $widget['path'] = str_replace($this->rootDir . '/Resources/views/', '', $widget['path']);

                $comment = '';

                $dataAdmin = '';

                if ($this->isAdmin && $this->authorizationChecker->isGranted($widget['requiredRole'])) {
                    $comment = "\n" . '<!-- ' . $widget['name'] . ' - v.' . $widget['codice'] . '-->' . "\n";

                    $dataAdmin = ' data-admin="/admin/layoutcontent/edit/' . $widget['id'] . '"';
                }

                $widget['out'][$this->locale] = $comment . '<div id="' . $widget['name'] . '_' . $widget['instance'] . '" ' . $dataAdmin . '>' . $this->twig->render($widget['path'],
                        array_merge(['instance' => $widget['name'] . '_' . $widget['instance']],
                            $widget['config'][$this->locale])) . '</div>';
            } else {
                $data = [];
                if (isset($widget['config'][$this->locale])) {
                    $data = $widget['config'][$this->locale];
                }

                $data['instance'] = $widget['name'] . '_' . $widget['instance'];

                $widget['path'] = str_replace($this->rootDir . '/Resources/views/', '', $widget['path']);

                $comment = '';

                $dataAdmin = '';

                if ($this->isAdmin && $this->authorizationChecker->isGranted($widget['requiredRole'])) {
                    $comment = "\n" . '<!-- ' . $widget['name'] . ' - v.' . $widget['codice'] . '-->' . "\n";
                    $dataAdmin = ' data-admin="/admin/layoutcontent/edit/' . $widget['id'] . '"';
                }

                $widget['out'][$this->locale] = $comment . '<div id="' . $widget['name'] . '_' . $widget['instance'] . '"' . $dataAdmin . '>' . $this->twig->render($widget['path'],
                        $data) . '</div>';
            }
        }
    }

    private function getMetaData(&$widget)
    {
        $widget['metaFile'] = $widget['configDir'] . '/config.json';

        if (!file_exists($widget['metaFile'])) {
            throw new \Exception('File non trovato (' . $widget['metaFile'] . ')');
        }

        $meta = json_decode(file_get_contents($widget['metaFile']), true);

        if (!$meta) {
            throw new \Exception('Controlla il formato del file ' . $widget['metaFile']);
        }

        $widget['meta'] = [];
        $widget['meta']['config'] = [];

        foreach ($meta as $key => $data) {
            if ('config' == $key) {
                foreach ($data as $k => $val) {
                    $widget['meta'][$key][$val['var']] = $val;
                }
            } else {
                $widget['meta'][$key] = $data;
            }
        }

        // carico gli assets da index.json se specificati.
        $widget['assets'] = [];

        $widget['indexFile'] = $widget['directory'] . '/index.json';

        if (!file_exists($widget['indexFile'])) {
            throw new \Exception('File non trovato (' . $widget['indexFile'] . ')');
        }

        $indexJsonData = json_decode(file_get_contents($widget['indexFile']), true);

        if (!$indexJsonData) {
            throw new \Exception('Controlla il formato del file ' . $widget['indexFile']);
        }

        if (isset($indexJsonData['assets'])) {
            $widget['assets'] = $indexJsonData['assets'];
        }
    }

    private function getTemplateFile(&$widget)
    {
        $widget['configDir'] = $this->rootDir . '/Resources/views/modules/' . $widget['category'] . '/' . $widget['name'];

        $widget['directory'] = $widget['configDir'] . '/' . $widget['codice'];

        $widget['file'] = $widget['name'] . '.twig';

        $widget['path'] = $widget['directory'] . '/' . $widget['file'];
    }

    private function loadAssets()
    {
        $assetsLayoutFile = $this->rootDir . '/Resources/views/public/layouts/' . $this->layout . '/assets.json';

        $data['css'] = [];
        $data['js'] = [];

        if ($assetsLayoutFile) {
            $data = json_decode(file_get_contents($assetsLayoutFile), true);
        }

        return $data;
    }

    private function getCSSVars($widget)
    {
        $ScssParse = new ScssParser();
        $css = $widget['directory'] . '/' . $widget['name'] . '.scss';

        $vars = $ScssParse->getVariables($css);

        return $vars;
    }
}
