<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PageHelper
{
    /**
     * @var EntityManager
     */
    private $em;
    /** @var string $locale */
    private $locale;
    /**
     * @var WebDir
     */
    private $webDir;

    /**
     * PageHelper constructor.
     *
     * @param ContainerInterface $container
     * @param Languages $languages
     */
    public function __construct(ContainerInterface $container, Languages $languages)
    {
        $this->em = $container->get('doctrine.orm.default_entity_manager');
        $this->webDir = $container->get('app.web_dir');
        $this->container = $container;

        $this->locale = $languages->getLanguage()->getShortCode();
    }

    /**
     * @param array $dati
     * @param null $parent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function riordina(array $dati, $parent = null)
    {
        foreach ($dati['children'] as $k => $nodo) {
            $repo = $this->em->getRepository('AppBundle:Page');
            $id = $nodo['id'];
            $Page = $repo->findOneBy(['id' => $id]);
            if ($parent) {
                $Parent = $repo->findOneBy(['id' => $parent]);
                $Page->setParentNode($Parent);
                $Page->setParent($Parent);
            } else {
                $Page->setParent(null);
                $Page->setMaterializedPath('');
            }
            $Page->setSort($k);
            $this->em->persist($Page);
            if (isset($nodo['children'])) {
                $this->riordina($nodo, $Page);
            }
        }
        if (!$parent) {
            $this->em->flush();
        }
    }

    public function getTree(Page $parent = null, $getDisabled = false, $excludedPage = null)
    {
        $data = [];
        if (!$parent) {
            $data['[000] + Livello Base'] = 0;
            $Pages = $this->em->getRepository('AppBundle:Page')
                ->getByParent(null, $getDisabled, $excludedPage, true);
        } else {
            $Pages = $parent->getChildren();
        }
        /** @var Page $Page */
        foreach ($Pages as $Page) {
            /** @var PageTranslation $page_translation */
            $page_translation = $Page->translate($this->locale);

            $prefix = '+' . str_repeat('--', $Page->getNodeLevel() - 1);
            if ($Page->isLeafNode()) {
                $prefix .= '-';
            } else {
                $prefix .= '+';
            }
            $data['[' . str_pad($Page->getId(), 3, '0') . '] ' . $prefix . ' ' . $page_translation->getTitolo()]
                = $Page->getId();
            $data = array_merge($data, $this->getTree($Page, $getDisabled, $excludedPage));
        }

        return $data;
    }

    /**
     * @param Page $Page
     *
     * @return string
     */
    public function getParentTitle(Page $Page)
    {
        if ($Page && $Page->getParent()) {
            $Page = $this->em->getRepository('AppBundle:Page')
                ->findOneBy(['id' => $Page->getParent()]);
            /** @var PageTranslation $page_translation */
            $page_translation = $Page->translate($this->locale);

            return $page_translation->getTitolo();
        }

        return 'Livello base';
    }

    public function jsonTree(Page $Page, $allElements, $arr = [])
    {
        $json['text'] = (string)$Page;
        $json['id'] = $Page->getId();
        $json['children'] = [];
        $Childs = $this->em->getRepository('AppBundle:Page')
            ->findBy(['parent' => $Page->getId()], ['sort' => 'asc']);
        if ($Childs) {
            foreach ($Childs as $child) {
                /*
                 * @var $child Page
                 */
                $json['children'][] = array_merge($this->jsonTree($child, $allElements, $arr), $arr);
            }
        }
        $json = array_merge($json, $arr);

        return $json;
    }

    public function correggiDimensioni(Page $page, $field, $dimensioni)
    {
        $method = 'get' . $field . 'Filename';
        $percorso = $this->webDir->get() . '/' . $page->getUploadDir() . $page->$method();
        if (file_exists($percorso)) {
            $imageHelper = $this->container->get('app.image_helper');
            $resizeData = json_decode($dimensioni, true);
            $imageHelper->resize('/' . $page->getUploadDir() . $page->$method(), $resizeData);
        }
    }
}
