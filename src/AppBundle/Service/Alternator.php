<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Service;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoTranslation;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\NewsCategoryTranslation;
use AppBundle\Entity\NewsTranslation;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use OfferteBundle\Entity\Offerta;
use OfferteBundle\Entity\OffertaTranslation;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Service\PagesUrlGenerator;

/**
 * Crea gli hreflang per l'entità passata come parametro al metodo "generateAlts".
 *
 * @author  Vkfan <class-dead@hotmail.com>
 *
 * @version 1.0
 */
class Alternator
{
    /** @var ObjectManager $em */
    private $em;
    /** @var Languages $language_service */
    private $language_service;
    /** @var Router $router */
    private $router;
    /** @var PagesUrlGenerator */
    private $pageUrlGenerator;

    /**
     * Alternator constructor.
     *
     * @param Languages $language
     * @param Router $router
     * @param mixed $em
     */
    public function __construct($em, $language, $router, $pageUrlGenerator)
    {
        $this->em = $em;
        $this->language_service = $language;
        $this->router = $router;
        $this->pageUrlGenerator = $pageUrlGenerator;
    }

    /**
     * @param Translatable $entity
     *
     * @return array
     */
    public function generateAlts($entity)
    {
        $alts = [];

        //Ottiene i dati di base
        // Se si tratta del blog, prende SOLO le lingue nelle quali il blog è attivo
        if ($entity instanceof NewsCategory || $entity instanceof News) {
            $href_langs = $this->getBlogLangs();
        } else {
            $href_langs = $this->language_service->getHrefLangOrder();
        }

        foreach ($href_langs as $locale) {
            $translation = $entity->translate($locale, false);
            if (true === $translation->isEnabled()) {
                $alts[$locale] = $this->generateRoute($entity, $locale);
            }
        }
        $x_default_url = reset($alts);
        $alts['x-default'] = $x_default_url;

        return $alts;
    }

    private function generateRoute($entity, $locale)
    {
        $route_parameters = [];

        if ($entity instanceof Appartamento) {
            /** @var AppartamentoTranslation $appartamento_translation */
            $appartamento_translation = $entity->translate($locale);

            $route_name = 'appartamento';
            $route_parameters = ['slug' => $appartamento_translation->getSlug()];
        } elseif ($entity instanceof News) {
            /** @var NewsTranslation $news_translation */
            $news_translation = $entity->translate($locale);

            $category = $entity->getPrimaryCategory();
            /** @var NewsCategoryTranslation $category_translation */
            $category_translation = $category->translate($locale);

            $route_name = 'blog_read_news';
            $route_parameters = [
                'slug' => $category_translation->getSlug(),
                'slugnotizia' => $news_translation->getSlug(),
            ];
        } elseif ($entity instanceof NewsCategory) {
            /** @var NewsCategoryTranslation $category_translation */
            $category_translation = $entity->translate($locale);

            $route_name = 'blog_category';
            $route_parameters = ['slug' => $category_translation->getSlug()];
        } elseif ($entity instanceof Offerta) {
            /** @var OffertaTranslation $offerta_translation */
            $offerta_translation = $entity->translate($locale);

            $route_name = 'offerta';
            $route_parameters = ['slug' => $offerta_translation->getSlug()];
        } elseif ($entity instanceof Page) {
            if ('home' === $entity->getTemplate()) {
                $route_name = 'home';
            } elseif ('blog' === $entity->getTemplate()) {
                $route_name = 'blog';
            } else {
                /** @var PageTranslation $page_translation */
                $page_translation = $entity->translate($locale);

                $route_name = 'page';
                return $this->pageUrlGenerator->generaUrl($entity, $locale, true);
            }
        } else {
            throw new Exception('La classe ' . get_class($entity) . ' non è supportata dalla generazione degli alt');
        }

        if ('it' === $locale) {
            $route_name .= '_it';
        }

        $route_parameters = array_merge($route_parameters, ['_locale' => $locale]);


        return $this->router->generate($route_name, $route_parameters);
    }

    /**
     * Ottiene le lingue abilitate nel blog.
     */
    private function getBlogLangs()
    {
        $page = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('blog');
        $active_locales = [];

        $href_langs = $this->language_service->getHrefLangOrder();

        foreach ($href_langs as $lang) {
            /** @var PageTranslation $page_translation */
            $page_translation = $page->translate($lang, false);

            if (true === $page_translation->isEnabled()) {
                $active_locales[] = $lang;
            }
        }

        return $active_locales;
    }
}