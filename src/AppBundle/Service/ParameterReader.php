<?php
// 21/06/17, 9.07
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class ParameterReader
{

    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }

    function getParameter($key, $subkey = '')
    {

        $parameter = $this->container->getParameter($key);

        if ($subkey) {
            return $parameter[$subkey];
        }

        return $parameter;

    }
}