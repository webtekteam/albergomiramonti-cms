<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Service;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoTranslation;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\NewsCategoryTranslation;
use AppBundle\Entity\NewsTranslation;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Doctrine\Common\Persistence\ObjectManager;
use OfferteBundle\Entity\Offerta;
use OfferteBundle\Entity\OffertaTranslation;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Crea in automatico l'array dei meta tag seo.
 *
 * @author  Vkfan <class-dead@hotmail.com>
 *
 * @version 1.0
 */
class SEOManager
{
    /** @var Alternator $alternator */
    private $alternator;
    /** @var ObjectManager $em */
    private $em;
    /** @var string $locale */
    private $locale;
    /** @var Request $request */
    private $request;

    public function __construct(Alternator $alternator, ObjectManager $em, RequestStack $request_stack)
    {
        $this->alternator = $alternator;
        $this->em = $em;
        $this->request = $request_stack->getCurrentRequest();

        if (null !== $this->request) {
            $this->locale = $this->request->getLocale();
        } else {
            $this->locale = 'it';
        }
    }

    /**
     * Restituisce un array con campi title (meta title), description (meta description) e alternate (array associativo con chiave le sigle delle lingue e valore l'url tradotto il quella lingua).
     *
     * @param Appartamento|News|NewsCategory|Offerta|Page $entity
     *
     * @return array
     */
    public function run($entity)
    {
        /** @var AppartamentoTranslation|NewsCategoryTranslation|NewsTranslation|OffertaTranslation|PageTranslation $entity_translation */
        $entity_translation = $entity->translate($this->locale);

        $alternates = $this->alternator->generateAlts($entity);

        $robots = 'index,follow';
        if ($this->hasMethod($entity, 'getRobots')) {
            $robots = $entity->getRobots();
        }

        $meta = [
            'alternate' => $alternates,
            'description' => $entity_translation->getMetaDescription(),
            'robots' => $robots,
            'title' => $entity_translation->getMetaTitle(),
        ];

        return $meta;
    }

    private function hasMethod($entity, $method_name)
    {
        try {
            $reflection_class = new ReflectionClass(get_class($entity));

            return $reflection_class->hasMethod($method_name);
        } catch (ReflectionException $ex) {
            return false;
        }
    }
}
