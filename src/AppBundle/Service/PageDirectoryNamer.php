<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 28/12/16
 * Time: 9.27
 */

namespace AppBundle\Service;


use AppBundle\Entity\PageRows;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class PageDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $Page = $object;

        if ($Page instanceof PageRows) {

            $Page = $object->getPage();

        }

        $dir = $Page->getId();


        return $dir;

    }


}