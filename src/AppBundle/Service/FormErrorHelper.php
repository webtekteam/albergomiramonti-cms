<?php
// 07/07/17, 10.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Translation\Translator;

class FormErrorHelper
{

    /**
     * @var Translator
     */
    private $translator;

    /**
     * FormErrorHelper constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {

        $this->translator = $translator;
    }

    /**
     * @param Form $form
     */
    public function getErrors(Form $form, $format = 'string')
    {

        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            /**
             * @var $error FormError
             */

            /**
             * @var $Config FormConfigInterface
             */
            $Config = $error->getOrigin()->getConfig();

            $lbl = $Config->getOptions()['label'];
            if ($lbl) {
                $lbl = $this->translator->trans($lbl);
            } else {
                $lbl = ucfirst($Config->getName());
            }
            $errors[] = $lbl . ': ' . $error->getMessage();
        }

        $errors = array_unique($errors);
        if ($format == 'string') {
            $errors = implode('<br />', $errors);
        }

        return $errors;

    }

}