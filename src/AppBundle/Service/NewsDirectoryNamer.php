<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 28/12/16
 * Time: 9.27
 */

namespace AppBundle\Service;

use AppBundle\Entity\NewsRows;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class NewsDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $News = $object;

        if ($News instanceof NewsRows) {

            $News = $object->getNews();

        }

        $dir = $News->getId();

        return $dir;

    }


}