<?php
// 06/11/17, 9.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Traits;

/**
 * Trait NotDeleted
 * Da utilizzare nelle classi che implementano SoftDeletable
 * @package AppBundle\Traits
 */
trait NotDeleted
{

    /**
     * @param null $active
     * @return mixed Elenco di Entity che soddisfano la query
     */
    function findAllNotDeleted($active = null)
    {

        $qb = $this->createQueryBuilder('entity')->andWhere('entity.deletedAt is NULL');
        if ($active) {
            $qb->andWhere('entity.isEnabled = 1');
        }

        return $qb->getQuery()->execute();
    }

}