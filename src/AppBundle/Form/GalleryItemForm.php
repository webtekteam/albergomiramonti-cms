<?php
// 07/07/17, 17.44
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\GalleryItems;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryItemForm extends AbstractType
{

    private $em;

    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id', HiddenType::class);
        $builder->add('gallery', HiddenType::class);
        $fields = [
            'alt' => [
                'label' => 'gallery.labels.alt',
                'required' => true,
            ],
            'description' => [
                'label' => 'gallery.labels.description',
                'required' => false,
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

        $builder->get('gallery')->addModelTransformer(new CallbackTransformer(
            function ($gallery) {

                return $gallery;
            },
            function ($gallery_id) {

                return $this->em->getRepository('AppBundle:Gallery')->findOneBy(['id' => $gallery_id]);

            }
        ));


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => GalleryItems::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}