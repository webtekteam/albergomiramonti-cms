<?php
// 10/01/17, 9.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\Vetrina;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VetrinaFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'name',
            TextType::class,
            [
                'help' => 'vetrina.help.nome',
                'label' => 'vetrina.labels.name',
            ]
        );

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'vetrina.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'category',
            null,
            [
                'choice_attr' => function ($allChoices, $currentChoiceKey) {

                    return ['data-dimensioni' => $allChoices->getDimensioni()];
                },
            ]
        );

        $fields = [
            'titolo' => [
                'label' => 'vetrina.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'vetrina.labels.sottotitolo',
                'required' => false,
            ],
            'url' => [
                'label' => 'vetrina.labels.url',
                'required' => false,
            ],
            'testo' => [
                'label' => 'vetrina.labels.testo',
                'required' => false,
                'attr' => ['class' => 'ck'],

            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'label' => false,
                'exclude_fields' => ['altImage'],
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Vetrina::class,
                'allow_extra_fields' => true,
                'langs' => ['it' => 'Italiano'],
            ]
        );

    }


}