<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 13/12/16
 * Time: 11.45
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class ResetPassForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => true,
            'first_options' => ['label' => 'operatori.labels.password'],
            'second_options' => ['label' => 'operatori.labels.repeat_password'],
            'error_bubbling' => true,
            'invalid_message' => 'operatori.pass_mismatch'
        ])->add('uuid', HiddenType::class)
            ->add('code', HiddenType::class);

    }


}