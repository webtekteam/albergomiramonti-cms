<?php
// 10/01/17, 14.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\EntityListener;

use AppBundle\Entity\Vetrina;
use AppBundle\Service\WebDir;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;


class VetrinaListener
{

    private $rootDir;

    public function __construct(WebDir $webDir)
    {

        $this->rootDir = $webDir->get();

    }

    /**
     * @ORM\PreRemove()
     */
    public function clean(Vetrina $Vetrina, LifecycleEventArgs $event)
    {

        if ($Vetrina->getDeletedAt()) {

            $dir = $this->rootDir . '/' . $Vetrina->getUploadDir();

            $fs = new Filesystem();
            $fs->remove($dir);

        }


    }

    /**
     * @ORM\PostPersist()
     */
    public function postPersist(Vetrina $vetrina, LifecycleEventArgs $event)
    {

        if ($vetrina->getListImgFileName()) {

            $from = $this->rootDir . '/' . $vetrina->getUploadDir() . '../' . $vetrina->getListImgFileName();

            if (!is_dir(dirname($this->rootDir . '/' . $vetrina->getUploadDir() . $vetrina->getListImgFileName()))) {
                mkdir(dirname($this->rootDir . '/' . $vetrina->getUploadDir() . $vetrina->getListImgFileName()), 0755,
                    true);
            }

            if (file_exists($from)) {
                $to = $this->rootDir . '/' . $vetrina->getUploadDir() . $vetrina->getListImgFileName();
                rename($from, $to);
            }

        }
    }

}