<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

class PageRepository extends EntityRepository
{
    use ORMBehaviors\Filterable\FilterableRepository;
    use ORMBehaviors\Tree\Tree;

    public function findAllNotDeleted($onlyActive = false)
    {
        $qb = $this->createQueryBuilder('page')
            ->andWhere('page.deletedAt is NULL');
        if ($onlyActive) {
            $qb->andWhere('page.isEnabled = 1');
        }

        return $qb->getQuery()
            ->execute();
    }

    public function countActivePages()
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->andWhere('p.deletedAt IS NULL')
            ->andWhere('p.isEnabled =1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getActivePages($locale = 'it')
    {
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('AppBundle\Entity\PageTranslation', 'ot', \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale')
            ->setParameter('locale', $locale)
            ->andWhere('p.deletedAt IS NULL')
            ->andWhere('pt.isEnabled = 1')
            ->andWhere('p.isEnabled = 1');

        return $qb->getQuery()
            ->getResult();
    }

    public function findBySlugPathButNotId($slug, $locale, $path, $id)
    {
        $query = $this->createQueryBuilder('page')
            ->leftJoin('AppBundle\Entity\PageTranslation', 'pt', \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale')
            ->setParameter('locale', $locale)
            ->andWhere('pt.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('page.id != :id')
            ->setParameter('id', $id)
            ->andWhere('page.materializedPath = :path')
            ->setParameter('path', $path)
            ->andWhere('page.deletedAt IS NULL')
            ->getQuery();

        return $query->execute();
    }

    public function getFlatTree($path, $rootAlias = 't')
    {
        $query = $this->getFlatTreeQB($path, $rootAlias)
            ->getQuery();

        return $query->execute();
    }

    public function getLikeFilterColumns()
    {
        return [];
    }

    public function getEqualFilterColumns()
    {
        return ['pt:slug'];
    }

    public function getILikeFilterColumns()
    {
        return [];
    }

    public function getInFilterColumns()
    {
        return [];
    }

    public function getBySlug($criteria)
    {
        $qb = $this->filterBy($criteria);
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function getByParent(Page $parent = null, $getDisabled = false, $excludePage = null, $locale = 'it')
    {
        $QB = $this->createQueryBuilder('page')
            ->leftJoin('AppBundle\Entity\PageTranslation', 'pt', \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale')
            ->setParameter('locale', $locale);
        if (!$getDisabled) {
            $QB->andWhere('page.isEnabled = 1')
                ->andWhere('pt.isEnabled = 1');
        }
        if ($excludePage && $excludePage->getId()) {
            $QB->andWhere('page.id != :excludedPage')
                ->setParameter('excludedPage', $excludePage);
        }
        if (!$parent) {
            $QB->andWhere('page.parent IS NULL');
        } else {
            $QB->andWhere('page.parent = :parent')
                ->setParameter('parent', $parent);
        }

        return $QB->getQuery()
            ->execute();
    }

    public function getByParentExcluded(
        $parent = 0,
        $exclude = [],
        $limit = 0,
        $sort = 'updatedAt:DESC',
        $getDisabled = false,
        $locale = 'it',
        $debug = 0
    ) {
        $QB = $this->createQueryBuilder('page')
            ->leftJoin('AppBundle\Entity\PageTranslation', 'pt', \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale')
            ->setParameter('locale', $locale);
        $QB->where('page.parent = :parent')
            ->setParameter('parent', $parent);
        if (!$getDisabled) {
            $QB->andWhere('page.isEnabled = 1')
                ->andWhere('pt.isEnabled = 1');
        }
        if ($exclude) {
            $QB->andWhere('page.id NOT in (:exclude)');
            $QB->setParameter('exclude', $exclude);
        }
        $sort = explode(':', $sort);
        $QB->addOrderBy('page.' . $sort[0], $sort[1]);
        if ($debug) {
            die;
        }

        return $QB->getQuery()
            ->execute();
    }

    /**
     * @param string $template
     * @param string $locale
     *
     * @return null|Page
     */
    public function findOneByTemplate($template, $locale = 'it')
    {
        $pages = $this->createQueryBuilder('p')
            ->innerJoin('p.translations', 'pt')
            ->where('pt.locale = :locale')
            ->andWhere('p.template = :template')
            ->andWhere('p.deletedAt IS NULL')
            ->andWhere('pt.isEnabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('template', $template)
            ->getQuery()
            ->getResult();

        if (0 === count($pages)) {
            return null;
        }

        return $pages[0];
    }

    protected function createFilterQueryBuilder()
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.translations', 'pt');
    }
}
