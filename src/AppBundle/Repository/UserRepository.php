<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findAllButUser(User $user)
    {
        return $this->createQueryBuilder('user')->andWhere('user.role != :role')->setParameter(
            'role',
            'ROLE_ANAGRAFICA'
        )->andWhere('user.id != :userId')->setParameter(
            'userId',
            $user->getId()
        )->andWhere('user.username != :username')->setParameter('username', 'system')->getQuery()->execute();
    }

    public function getByEmailCount($email, $role, $excluded = 0)
    {
        $qb = $this->createQueryBuilder('a')->select('count(a.id)')->where('a.email = :email')->andWhere(
            'a.role = :role'
        )->setParameter('role', $role)->setParameter('email', $email);
        if ($excluded) {
            $qb->andWhere('a.id != :excluded')->setParameter('excluded', $excluded);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
