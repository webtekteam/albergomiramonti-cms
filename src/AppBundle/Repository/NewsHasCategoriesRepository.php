<?php
// 17/01/17, 10.20
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use AppBundle\Entity\NewsCategory;
use Doctrine\ORM\EntityRepository;

class NewsHasCategoriesRepository extends EntityRepository
{

    public function associationExists($newsId, $categoryId, $isMain)
    {

        return $this->createQueryBuilder('nhc')->select('count(nhc.id)')->andWhere('nhc.news = :news')->andWhere(
            'nhc.category = :category'
        )->andWhere('nhc.isMain = :isMain')->setParameter('news', $newsId)->setParameter(
            'category',
            $categoryId
        )->setParameter('isMain', $isMain)->getQuery()->getSingleScalarResult();

    }

    public function getNewsQuery($category_id, $locale)
    {

        $qb = $this->createQueryBuilder('nhc');

        $qb->leftJoin(
            'AppBundle\Entity\News',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n = nhc.news'
        )->leftJoin(
            'AppBundle\Entity\NewsTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = nhc.news AND nt.locale = :locale'
        )->andWhere('nhc.category = :category_id')->setParameter('category_id', $category_id)->andWhere(
            'n.isEnabled = 1'
        )->andWhere('n.publishAt < :publishAt')->andWhere('nt.isEnabled = 1')->setParameter(
            'publishAt',
            new \DateTime()
        )->setParameter('locale', $locale)->orderBy('n.publishAt', 'DESC');

        $query = $qb->getQuery();


        return $query;

    }

    public function getNews($category_id, $locale, $start = false, $limit = 10, $excluded = [])
    {

        $qb = $this->createQueryBuilder('nhc');

        $qb->leftJoin(
            'AppBundle\Entity\News',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n.id = nhc.news'
        )->leftJoin(
            'AppBundle\Entity\NewsTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = nhc.news AND nt.locale = :locale'
        )->leftJoin(
            'AppBundle\Entity\NewsCategory',
            'nc',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nc.id = nhc.category'
        )->setParameter('locale', $locale)->andWhere('nc.id = :category_id')->setParameter(
            'category_id',
            $category_id
        )->andWhere('n.isEnabled = 1')->andWhere('n.publishAt < :publishAt')->setParameter(
            'publishAt',
            new \DateTime()
        )->andWhere('nt.isEnabled = 1');
        if ($excluded) {
            $qb->andWhere('n NOT IN (:news)')->setParameter('news', $excluded);
        }

        if ($start !== false && $limit) {
            $qb->setMaxResults($limit)->setFirstResult($start);
        }

        $qb->orderBy('n.publishAt', 'DESC');

        $query = $qb->getQuery();

        return $query->getResult();

    }

    public function getNewsCount(NewsCategory $category, $locale)
    {

        $qb = $this->createQueryBuilder('nhc');
        $qb->select('count(nhc.news)');
        $qb->leftJoin(
            'AppBundle\Entity\News',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n = nhc.news'
        )->leftJoin(
            'AppBundle\Entity\NewsTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = nhc.news AND nt.locale = :locale'
        )->andWhere('nhc.category = :category')->setParameter('category', $category)->andWhere(
            'n.isEnabled = 1'
        )->andWhere('n.publishAt < :publishAt')->andWhere('nt.isEnabled = 1')->setParameter(
            'publishAt',
            new \DateTime()
        )->setParameter('locale', $locale);

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();


    }


}