<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;
use Doctrine\ORM\EntityRepository;

class PageRowRepository extends EntityRepository
{
    /**
     * Restituisce tutte le righe della pagina passata come parametro, nel locale specificato.
     *
     * @param Page $page
     * @param string $locale
     *
     * @return null|PageRows[]
     */
    public function findByPage($page, $locale = 'it')
    {
        return $this->createQueryBuilder('pr')
            ->where('pr.page = :page')
            ->andWhere('pr.locale = :locale')
            ->orderBy('pr.sort', 'ASC')
            ->setParameter('locale', $locale)
            ->setParameter('page', $page)
            ->getQuery()
            ->getResult();
    }

    /**
     * Restituisce una riga da una pagina, del locale specificato, con l'indice specificato.
     *
     * L'indice è 0-indexed
     *
     * @param Page $page
     * @param string $locale
     * @param int $numero_riga
     *
     * @return null|PageRows
     */
    public function findOneByPageAndIndex($page, $locale = 'it', $numero_riga = 0)
    {
        $rows = $this->findByPage($page, $locale);

        if (count($rows) <= $numero_riga) {
            return null;
        }

        return $rows[$numero_riga];
    }
}
