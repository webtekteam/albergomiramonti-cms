<?php
// 09/01/17, 16.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategorieVetrinaRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('catvetrina')
            ->andWhere('catvetrina.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function countAllNotDeleted()
    {

        $qb = $this->createQueryBuilder('catvetrina');

        $qb->select($qb->expr()->count('catvetrina'))
            ->where('catvetrina.deletedAt is NULL');

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

}