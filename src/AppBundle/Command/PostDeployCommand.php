<?php
// 08/09/17, 23.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class PostDeployCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this->setName('app:postdeploy')
            ->setDescription('Tool per la rigenerazione dei file di settings e per la rigenerazione del security.yml')
            ->setHelp('Chiamato al termine di un deploy rigenera i file in base ai dati del db');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();

        $em = $container->get('doctrine.orm.default_entity_manager');

        $Settings = $em->getRepository('AppBundle:Settings')->findAll();
        $container->get('app.settings_helper')->toYamlFile($Settings);

        $GroupHelper = $container->get('app.services.group_helper')->writeSecurity();
        $fs = new Filesystem();
        $fs->remove($container->getParameter('kernel.cache_dir'));

    }

}