<?php
// 20/03/17, 17.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\EventListener;


use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{

    /**
     * @var Container
     */
    private $container;


    /**
     * ExceptionListener constructor.
     */
    public function __construct(Container $container)
    {

        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        // You get the exception object from the received event
        $exception = $event->getException();

        if ($exception instanceof HttpException) {

            if (!in_array($this->container->getParameter('kernel.environment'), ['test', 'dev']) &&
                in_array($exception->getStatusCode(), ['404', '500'])
            ) {
                $em = $this->container->get('doctrine')->getManager();

                $translator = $this->container->get('translator');

                $twigParams = [];

                $META['title'] = $translator->trans('default.labels.title_404', [], 'public');
                $META['description'] = '';

                $twigParams['twigs'] = [];

                $TemplateLoader = $this->container->get('app.template_loader');

                $Languages = $this->container->get('app.languages');

                $twigs = $TemplateLoader->getTwigs(
                    '404',
                    $Languages->getActivePublicLanguages(),
                    ['langs' => $Languages->getActivePublicLanguages()]
                );

                $twigs['assets']['css'][] = '/public/css/404.css';


                $twigs['showHeader'] = false;
                $twigs['showFooter'] = false;


                // Customize your response object to display the exception details
                $response = new Response();

                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());

                $response->setContent(
                    $this->container->get('templating')->render(
                        'public/site.html.twig',
                        ['twigs' => $twigs, 'META' => $META]
                    )
                );

                $event->setResponse($response);

            }
        }

    }

}