<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 16.24
 */

namespace AppBundle\security;


use AppBundle\Form\LoginForm;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{

    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;


    /**
     * LoginFormAuthenticator constructor.
     */
    public function __construct(
        FormFactory $formFactory,
        EntityManager $em,
        RouterInterface $router,
        UserPasswordEncoder $passwordEncoder
    ) {

        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getCredentials(Request $request)
    {

        $isLoginSubmit = $request->getPathInfo() == '/admin/login' && $request->isMethod('POST');

        if (!$isLoginSubmit) {
            return null;
        }

        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);
        $data = $form->getData();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_username']
        );

        return $data;


    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        $username = $credentials['_username'];

        $user = $this->em->getRepository('AppBundle:User')->findOneBy(['username' => $username]);

        if ($user && $user->getIsEnabled()) {
            return $user;
        }

        return null;

    }

    public function checkCredentials($credentials, UserInterface $user)
    {

        $password = $credentials['_password'];


        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            return true;
        }

        return false;

    }

    protected function getLoginUrl()
    {


        return $this->router->generate('security_login');

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        // if the user hit a secure page and start() was called, this was
        // the URL they were on, and probably where you want to redirect to
        $targetPath = false;
        if ($request->getSession() instanceof SessionInterface) {
            $targetPath = $request->getSession()->get('_security.' . $providerKey . '.target_path');
        }

        if (!$targetPath) {
            $targetPath = $this->router->generate('dashboard');
        }

        return new RedirectResponse($targetPath);

    }

    public function getDefaultSuccessRedirectUrl()
    {

        return $this->router->generate('dashboard');

    }

}