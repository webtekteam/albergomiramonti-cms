<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;

use AppBundle\Entity\Attachment;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;

class PageFormatter extends DataFormatter
{
    private $parent = 0;
    private $limit = 0;
    private $exclude = [];
    private $sortField = 'updatedAt';

    /**
     * @var Page
     */
    private $Page;

    public function getData()
    {
        $data = [];
        $data['page'] = [];
        $data['page']['titolo'] = 'N.A.';
        $data['page']['sottotitolo'] = 'N.A.';
        $data['page']['rows'] = [];
        $data['page']['listImg'] = [];
        $data['page']['listImg']['src'] = false;
        $data['page']['listImg']['alt'] = false;

        if ($this->Page) {
            $data['page']['titolo'] = $this->Page->translate()
                ->getTitolo();
            $data['page']['sottotitolo'] = $this->Page->translate()
                ->getSottotitolo();
            if (!$data['page']['sottotitolo']) {
                $data['page']['sottotitolo'] = $data['page']['titolo'];
            }

            if ($this->Page->getListImgFileName()) {
                $data['page']['listImg']['src'] = '/' . $this->Page->getUploadDir() . '/' . $this->Page->getListImgFileName();
                $data['page']['listImg']['alt'] = $this->Page->getListImgAlt();
            }

            $rows = $this->Page->getPageRows();

            foreach ($rows as $PageRow) {
                /*
                 * @var PageRows
                 */
                if ($PageRow->getLocale() == $this->locale) {
                    $data['page']['rows'][] = $PageRow;
                }
            }
        }

        return $data;
    }

    public function getDataWithImages()
    {
        $data = $this->getData();

        if ($this->Page) {
            $this->attachments = $this->Page->getAttachments();

            foreach ($this->attachments as $attachment) {
                /*
                 * @var Attachment;
                 */
                if (in_array($attachment->getType(), ['jpeg', 'png', 'jpg', 'gif'])) {
                    $data['immagini'][] = $attachment;
                }
            }
        }

        return $data;
    }

    public function extractPages()
    {
        return $this->em->getRepository('AppBundle:Page')
            ->getByParentExcluded($this->parent, $this->exclude, $this->limit, $this->sortField, false, $this->locale);
    }

    public function getPages()
    {
        if ($this->Page) {
            $this->parent = $this->Page->getId();
        }

        $this->sortField = 'sort:asc';

        $data['pages'] = $this->extractPages();

        return $data;
    }

    public function getPagesNextAndPrev()
    {
        if ($this->Page) {
            $this->parent = $this->Page->getParent();
        }

        $this->limit = 0;

        $pages = $this->em->getRepository('AppBundle:Page')
            ->getByParentExcluded($this->parent, $this->exclude, $this->limit, 'sort', false, $this->locale);

        $data = [];
        $data['next'] = false;
        $data['prev'] = false;

        $PageUrlGenerator = $this->container->get('app.page_url_generator');

        foreach ($pages as $k => $Page) {
            if ($Page == $this->Page) {
                if (isset($pages[$k - 1])) {
                    $data['prev'] = $PageUrlGenerator->generaUrl($pages[$k - 1], $this->locale);
                }
                if (isset($pages[$k + 1])) {
                    $data['next'] = $PageUrlGenerator->generaUrl($pages[$k + 1], $this->locale);
                }
            }
        }

        return $data;
    }

    public function getRootParent()
    {
        $data = [];
        $data['Page'] = false;

        if ($this->Page) {
            $ids = explode('/', trim($this->Page->getMaterializedPath(), '/'));

            if (isset($ids[$this->data[$this->locale]['profondita']['val']])) {
                $firstParent = $ids[$this->data[$this->locale]['profondita']['val']];
                $data['Page'] = $this->em->getRepository('AppBundle:Page')
                    ->findOneBy(['id' => $firstParent]);
            }
        }

        return $data;
    }

    public function getPagesLevel()
    {
        $data = [];

        $this->parent = 0;

        if ($this->data[$this->locale]['livello']['val'] == -1) {
            if ($this->Page) {
                $this->parent = $this->Page->getParent();
            }
        } else {
            $this->parent = (int)($this->data[$this->locale]['livello']['val']);
        }

        $this->limit = 0;

        if (is_numeric($this->data[$this->locale]['limit']['val'])) {
            $this->limit = (int)($this->data[$this->locale]['limit']['val']);
        }

        $this->exclude = [];

        if ($this->data[$this->locale]['limit']['val'] && $this->Page) {
            $this->exclude[] = $this->Page->getId();
        }

        $data['pages'] = $this->extractPages();

        return $data;
    }

    public function getUrl()
    {
        /* Se non Ã¨ impostata una pagina, o la pagina impostata Ã¨ vuota ottiene un valore di default;
         * quello della pagina home */
        if (array_key_exists('pid', $this->data['it'])) {
            $pid = $this->data['it']['pid']['val'];
            $toRet = $this->getUrlFromPageId($pid);
        } else {
            $index = 1;
            $toRet = [];
            while (array_key_exists('page' . $index, $this->data['it'])) {
                $pid = $this->data['it']['page' . $index]['val'];
                $toRet[] = $this->getUrlFromPageId($pid);

                ++$index;
            }
        }

        return $toRet;
    }

    public function extractData()
    {
        if (isset($this->AdditionalData['Entity'])) {
            $this->Page = $this->AdditionalData['Entity'];
        }
    }

    /**
     * C'è un motivo se l'ho chiamato in questo modo:
     * Se è presente (e impostato) il parametro "template" prende la pagina con il template impostato
     * Se non è presente, restituisce la pagina che si sta visitando in questo momento.
     *
     * @return Page|string[]
     */
    public function getPage()
    {
        $template = $this->getParameter('template', '');

        if ('' === $template) {
            if (null === $this->Page) {
                return ['error' => 'Non ti trovi in una pagina e non hai fornito un template'];
            }

            return $this->Page;
        }

        $page = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate($template, $this->locale);

        if (null === $page) {
            return ['error' => 'La pagina con template "' . $template . '" non è stata trovata'];
        }

        return $page;
    }

    private function getUrlFromPageId($pid)
    {
        $page = $this->em->getRepository('AppBundle\\Entity\\Page')
            ->findOneBy([
                'id' => $pid,
            ]);

        $toRet = [
            'abs' => '',
            'rel' => '',
        ];
        if (null !== $page) {
            $router = $this->container->get('app.page_url_generator');
            $toRet = [
                'abs' => $router->generaUrl($page, $this->locale, true),
                'rel' => $router->generaUrl($page, $this->locale),
            ];
        }

        return $toRet;
    }
}
