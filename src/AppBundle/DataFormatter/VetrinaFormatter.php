<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\DataFormatter;

use AppBundle\Entity\Vetrina;
use AppBundle\Entity\VetrinaTranslation;
use Cocur\Slugify\Slugify;

class VetrinaFormatter extends DataFormatter
{
    /** @var Vetrina[] $vetrine */
    private $vetrine;

    public function getSlide()
    {
        /**
         * @var $vetrina Vetrina
         */
        $vetrina = $this->em->getRepository('AppBundle:Vetrina')
            ->findOneBy(['name' => $this->data['it']['id_slide']['val']]);

        $slug = new Slugify();
        $slug->addRule('à', 'a');
        $slug->addRule('è', 'e');
        $slug->addRule('é', 'e');
        $slug->addRule('ì', 'i');
        $slug->addRule('ò', 'o');
        $slug->addRule('ù', 'u');

        $record = [];
        $record['titolo'] = $vetrina->translate($this->locale)
            ->getTitolo();
        $record['slug'] = $slug->slugify($record['titolo']);
        $record['sottotitolo'] = $vetrina->translate($this->locale)
            ->getSottotitolo();
        $record['testo'] = $vetrina->translate($this->locale)
            ->getTesto();
        $record['url'] = $vetrina->translate($this->locale)
            ->getUrl();
        $record['image'] = [];
        $record['image']['src'] = '/' . $vetrina->getUploadDir() . $vetrina->getListImgFileName();
        $record['image']['alt'] = $vetrina->getListImgAlt();

        return $record;
    }

    public function getData()
    {
        $desired_category = $this->getParameter('category', '');
        if (null === $desired_category) {
            return ['error' => 'Non è stata specificata una vetrina da prelevare'];
        }

        $vc = $this->em->getRepository('AppBundle:VetrinaCategory')
            ->findOneBy(['nome' => $desired_category]);

        $data = ['error' => false];

        if (!$vc) {
            $data['error'] = 'Non è stato configurato correttamente il modulo vetrina (categoria errata)';
        } else {
            $this->vetrine = $vc->getVetrine();

            $data['slides'] = [];
            $data['column'] = isset($this->data['it']['column']) ? $this->data['it']['column']['val'] : 1;
            $data['titolo'] = isset($this->data[$this->locale]['titolo']) ? $this->data[$this->locale]['titolo']['val'] : 1;
            $data['testo'] = isset($this->data[$this->locale]['testo']) ? $this->data[$this->locale]['testo']['val'] : 1;

            foreach ($this->vetrine as $vetrina) {
                /**
                 * @var Vetrina $vetrina
                 */
                if ($vetrina->isEnabled() && !$vetrina->getDeletedAt()) {
                    /** @var VetrinaTranslation $vetrina_translation */
                    $vetrina_translation = $vetrina->translate($this->locale);

                    $slug = new Slugify();
                    $slug->addRule('à', 'a');
                    $slug->addRule('è', 'e');
                    $slug->addRule('é', 'e');
                    $slug->addRule('ì', 'i');
                    $slug->addRule('ò', 'o');
                    $slug->addRule('ù', 'u');

                    $record = [
                        'id' => $vetrina->getId(),
                        'image' => [
                            'alt' => $vetrina->getListImgAlt(),
                            'src' => $vetrina->getUploadDir() . $vetrina->getListImgFileName(),
                        ],
                        'sottotitolo' => $vetrina_translation->getSottotitolo(),
                        'testo' => $vetrina_translation->getTesto(),
                        'titolo' => $vetrina_translation->getTitolo(),
                        'url' => $vetrina_translation->getUrl(),
                    ];
                    $record['slug'] = $slug->slugify($record['titolo']);

                    $data['slides'][] = $record;
                }
            }
        }

        return $data;
    }

    protected function extractData()
    {
    }
}
