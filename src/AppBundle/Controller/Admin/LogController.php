<?php
// 28/09/17, 15.11
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_SUPER_ADMIN')")
 */
class LogController extends Controller
{

    /**
     * @Route("/logs", name="logs")
     */
    public function listAction()
    {

        return $this->render(':admin/logs:list.html.twig');

    }

    /**
     * @Route("/logs/json", name="log_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.services.logs_helper')->getList();

        return new JsonResponse($return);

    }


}