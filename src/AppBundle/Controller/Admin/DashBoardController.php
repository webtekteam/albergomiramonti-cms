<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/admin")
 */
class DashBoardController extends Controller
{

    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository('AppBundle:Page')->countActivePages();

        $news = $em->getRepository('AppBundle:News')->countActiveNews();

        return $this->render('admin/dashboard/dashboard.html.twig', [
            'pages' => $pages,
            'news' => $news,
            'rss_notizie' => $this->getParameter('generali')['rss_notizie'],
            'rss_offerte' => $this->getParameter('generali')['rss_offerte']
        ]);

    }
}
