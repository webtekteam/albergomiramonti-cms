<?php
// 11/01/17, 14.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Language;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_TRADUZIONI')")
 */
class TranslationController extends Controller
{

    /**
     * @Route("/translation", name="translation")
     */
    public function manageTranslations()
    {

        $em = $this->getDoctrine()->getManager();

        $Languages = $em->getRepository('AppBundle:Language')->findBy([], ['sort' => 'ASC']);

        $langs = [];

        foreach ($Languages as $language) {
            $langs[] = [
                'short' => $language->getShortCode(),
                'long' => $language->getLanguageName()
            ];
        }


        return $this->render('admin/translation/list.html.twig',
            ['langs' => json_encode($langs), 'languages' => $langs]);

    }

    /**
     * @Route("/translation/json", name="translation_list_json")
     */
    public function listJson()
    {

        $em = $this->getDoctrine()->getManager();

        $Languages = $em->getRepository('AppBundle:Language')->findBy([], ['sort' => 'ASC']);

        $yamlDirectory = $this->get('kernel')->getRootDir() . '/' . 'Resources' . '/' . 'translations' . '/';

        $files = ['messages', 'public'];

        $all = [];

        foreach ($files as $file) {

            foreach ($Languages as $Language) {

                $yamlFile = $yamlDirectory . $file . '.' . $Language->getShortCode() . '.yml';

                if (file_exists($yamlFile)) {

                    $all[$Language->getShortCode()][$file] = Yaml::parse(file_get_contents($yamlFile));

                } else {

                    $all[$Language->getShortCode()][$file] = [];

                }

            }


        }

        $data = [];

        //dump($all);
        foreach ($all['it'] as $file => $moduleData) {


            //dump('$file: '.$file);

            foreach ($moduleData as $section => $keys) {

                //dump('$section: '.$section);
                foreach ($keys as $subSection => $keyData) {
                    //dump('$subSection: '.$subSection);

                    //dump($keyData);

                    if (!is_array($keyData)) {
                        die;
                    }

                    foreach ($keyData as $key => $translation) {
                        ////dump('$key: '.$key);
                        // //dump('$translation: '.$translation);

                        $record = [];
                        $record['file'] = $file;
                        $record['section'] = $section;
                        $record['subSection'] = $subSection;
                        $record['key'] = $key;
                        $record['it'] = $translation;
                        $record['id'] = $file . "<br />" . $section . "<br />" . $subSection . "<br />" . $key;

                        foreach ($Languages as $Language) {
                            if ($Language->getShortCode() !== 'it') {
//                                //dump('$Language->getShortCode(): '.$Language->getShortCode());
//                                //dump('$all['.$Language->getShortCode().']['.$file.']['.$section.']['.$subSection.']['.$key.']');
                                if (isset($all[$Language->getShortCode()][$file][$section][$subSection][$key])) {
                                    $trans = $all[$Language->getShortCode()][$file][$section][$subSection][$key];
                                } else {
                                    $trans = $record['it'];
                                }
                                $record[$Language->getShortCode()] = $trans;
                            }
                        }

                        $data[] = $record;

                    }

                }

            }

        }


        $retData = [];
        $retData['data'] = $data;

        return new JsonResponse($retData);

    }


    /**
     * @Route("/translation/update", name="translation_update")
     */
    function updateKey(Request $request)
    {

        $return = [];

        if ($request->request->has('id') && $request->request->has('lang') && $request->request->has('value')) {

            $return['result'] = true;

            //"messages.default.labels.abilitato"
            list($file, $section, $subSection, $key) = explode('.', trim($request->request->get('id')));

            $translationSaver = $this->get('translation_save');

            $translationSaver->setValue($request->request->get('lang'), $file, $section, $subSection, $key,
                $request->request->get('value'));

        } else {

            $return['result'] = false;

        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/translation/save", name="translation_save")
     */
    function saveKey(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $Languages = $em->getRepository('AppBundle:Language')->findBy([], ['sort' => 'ASC']);

        $langs = [];

        foreach ($Languages as $language) {
            $langs[] = [
                'short' => $language->getShortCode(),
                'long' => $language->getLanguageName()
            ];
        }

        $errors = [];

        $re = '/^([a-zA-Z_0-9]+)\.([a-zA-Z_0-9]+)\.([a-zA-Z_0-9]+)\.([a-zA-Z_0-9]+)$/';

        if (!$request->request->has('translation_id') || !preg_match($re,
                trim($request->request->get('translation_id')), $m)) {
            $errors[] = 'BadData (translation_id)';
        }

        if ($request->request->has('translation')) {
            $translation = $request->request->get('translation');
            foreach ($Languages as $language) {
                if (!isset($translation[$language->getShortCode()])) {
                    $errors[] = 'BadData (' . $language->getShortCode() . ')';
                }
            }
        } else {
            $errors[] = 'BadData';
        }

        $return = [];

        if (count($errors)) {
            $return['result'] = false;
            $return['errors'] = $errors;
        } else {

            $fileName = $m[1];
            $section = $m[2];
            $subSection = $m[3];
            $key = $m[4];

            $translationSaver = $this->get('translation_save');

            foreach ($Languages as $language) {

                $translationSaver->setValue($language->getShortCode(), $fileName, $section, $subSection, $key,
                    $translation[$language->getShortCode()]);

            }

            $return['result'] = true;

        }

        return new JsonResponse($return);

    }


}