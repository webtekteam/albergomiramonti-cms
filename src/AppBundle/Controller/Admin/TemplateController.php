<?php
// 30/01/17, 15.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Template;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_TEMPLATE')")
 */
class TemplateController extends Controller
{

    /**
     * @Route("/template", name="template")
     */
    public function defaultAction(Request $request)
    {

        $moduleLoader = $this->get('app.module_loader');

        $modules = $moduleLoader->loadModules();

        // elenco dei template
        // directory di base
        $baseDir = $this->get('kernel')->getRootDir() . '/' . 'Resources' . '/' . 'views' . '/' . 'public' . '/' . 'layouts' . '/' . $this->getParameter('generali')['layout'] . '/';

        // 1.Headers
        $finder = new Finder();
        $files = $finder->files()->in($baseDir . 'HEADER' . '/');

        $templates = [];
        $templates['HEADER'] = [];

        foreach ($files as $file) {
            $data = explode('.', $file->getBasename());
            $templates['HEADER'][] = array_shift($data);
        }

        // 2.Footers
        $finder = new Finder();
        $files = $finder->files()->in($baseDir . 'FOOTER' . '/');

        $templates['FOOTER'] = [];
        foreach ($files as $file) {
            $data = explode('.', $file->getBasename());
            $templates['FOOTER'][] = array_shift($data);
        }

        // 3.Content
        $finder = new Finder();
        $files = $finder->files()->in($baseDir . 'CONTENT' . '/');

        $templates['CONTENT'] = [];
        foreach ($files as $file) {
            $data = explode('.', $file->getBasename());
            $templates['CONTENT'][] = array_shift($data);
        }

        foreach ($templates as $k => $data) {
            sort($templates[$k]);
        }

        return $this->render(
            'admin/template/edit.html.twig',
            [
                'data' => '',
                'modules' => $modules,
                'templates' => $templates,
            ]
        );

    }

    /**
     * @Route("/template/load_configured_modules")
     */
    public function loadConfiguredModules(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $dbData = $em->getRepository('AppBundle:Template')->findBy(
            [
                'type' => $request->request->get('type'),
                'template' => $request->request->get('template'),
                'layout' => $this->getParameter('generali')['layout'],
            ],
            ['position' => 'ASC']
        );


        $data = [];
        $data['result'] = true;

        // directory moduli
        $dirModuli = $this->get('kernel')->getRootDir() . '/' . 'Resources' . '/' . 'views' . '/' . 'modules';

        $moduleLoader = $this->get('app.module_loader');

        $data['moduli'] = [];
        $data['sezioni'] = $moduleLoader->loadSlots($request->request->get('type'), $request->request->get('template'));

        foreach ($dbData as $module) {

            $dir = $dirModuli . '/' . $module->getCategory() . '/' . $module->getModule() . '/' . $module->getCodice() . '/';

            $modulo = $moduleLoader->loadDescription($dir);

            $modulo['parent'] = $module->getContainer();
            $modulo['instance'] = $module->getInstance();
            $modulo['id'] = $module->getId();

            $data['moduli'][$module->getInstance()] = $modulo;
            $data['sezioni'][] = $module->getContainer();

        }

        $data['sezioni'] = array_values(array_unique($data['sezioni']));

        return new JsonResponse($data);

    }

    /**
     * @Route("/template/remove_module/{id}")
     */
    public function removeModule(Request $request, Template $template)
    {

        $trans = $this->get('translator');

        $data = [];

        if ($template) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($template);
            $em->flush();

            $data['result'] = true;


        } else {
            $data['result'] = false;
            $data['errors'] = [];
            $data['errors'][] = $trans->trans('template.errors.not_found');
        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/template/save_sort_order")
     */
    public function saveSortOrder(Request $request)
    {

        $return = [];
        $return['result'] = true;

        $em = $this->getDoctrine()->getManager();
        $res = $em->getRepository('AppBundle:Template')->getSortedByIds($request->request->get('sort'));

        /**
         * @var $Template Template
         */
        foreach ($res as $k => $Template) {

            $Template->setPosition($k);
            $em->persist($Template);
        }

        $em->flush();

        return new JsonResponse($return);


    }

    /**
     * @Route("/template/insert_template")
     */
    public function insertTemplate(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $instance = $request->get('module') . '_' . uniqid();

        $Template = true;

        while ($Template) {
            $Template = $em->getRepository('AppBundle:Template')->findOneBy(
                ['module' => $request->get('module'), 'instance' => $instance]
            );
            $instance = uniqid();
        }

        $Template = new Template();

        $Template->setTemplate($request->get('template'));
        $Template->setContainer($request->get('container'));
        $Template->setCategory(strtoupper($request->get('category')));
        $Template->setCodice($request->get('codice'));
        $Template->setView('-');
        $Template->setModule($request->get('module'));
        $Template->setPosition(0);
        $Template->setInstance($instance);
        $Template->setRequiredRole('ROLE_USER');
        $Template->setType($request->get('type'));
        $Template->setConfig(json_decode("{}"));
        $Template->setIsEnabled(1);
        $Template->setLayout($this->getParameter('generali')['layout']);

        $em->persist($Template);
        $em->flush();

        $return = [];
        $return['result'] = true;
        $return['id'] = $Template->getId();

        $this->get('app.assets_manager')->compile();

        return new JsonResponse($return);

    }
}