<?php
/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_USERS')")
 */
class UserController extends Controller
{

    /**
     * @Route("/operatori", name="operatori_list")
     */
    public function listAction()
    {

        return $this->render('admin/operatori/list.html.twig');
    }

    /**
     * @Route("/operatori/json", name="operatori_list_json")
     */
    public function listJson()
    {

        $em = $this->getDoctrine()->getManager();
        $me = $this->getUser();
        try {
            $data = $this->get('app.services.json_list_helper')->setEntity(User::class)->setFields(
                [
                    ['avatar' => 'listImgFileName'],
                    'nome',
                    'cognome',
                    'username',
                    ['gruppo' => 'role'],
                    'email',
                    'isEnabled',
                    'createdAt',
                    'updatedAt'
                ]
            )->setModifier(
                'avatar',
                function ($avatar, User $User) {

                    if (!$avatar) {
                        $avatar = '/media/users/user.png';
                    } else {
                        $avatar = '/' . $User->getUploadDir() . $avatar;
                    }
                    return $avatar;
                }
            )->setModifier(
                'gruppo',
                function ($role) use ($em) {

                    if ($role === 'ROLE_SUPER_ADMIN') {
                        return 'Super Admin';
                    }
                    $Group = $em->getRepository('AppBundle:Group')->findOneBy(['role' => $role]);

                    return (string)$Group;
                }
            )->addFieldToRow('allowedToSwitch', function () {
                return $this->get('security.authorization_checker')->isGranted('ROLE_ALLOWED_TO_SWITCH');
            })->setRepositoryMethod('findAllButUser', [$me])->getList();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        $retData['data'] = $data;

        return new JsonResponse($retData);
    }

    /**
     * @Route("/operatori/new", name="operatori_new")
     * @Route("/operatori/edit/{id}",  name="operatori_edit")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newEditAction(Request $request)
    {

        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $User = new User();
        $id = null;
        $supportData = [];
        $supportData['listImgUrl'] = false;
        if ($request->get('id')) {
            $id = $request->get('id');
            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);
            if (!$User) {
                return $this->redirectToRoute('operatori_new');
            }
            if ($User->getListImgFileName()) {
                $supportData['listImgUrl'] = '/' . $User->getUploadDir() . $User->getListImgFileName();
            }
        }
        $form = $this->createForm(
            UserForm::class,
            $User
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var User
             */
            $User = $form->getData();
            $new = false;
            if (!$User->getId()) {
                $new = true;
            }
            $cancellaListPrecedente = $request->get('user_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($User, 'listImg');
                $User->setListImg(null);
            }
            $em->persist($User);
            $em->flush();
            $elemento = $User->getNome();
            if (!$new) {
                $this->addFlash(
                    'success',
                    'User "' . $elemento . '" ' . $translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'User "' . $elemento . '" ' . $translator->trans('default.labels.creato'));
            }

            return $this->redirectToRoute('operatori_list');
        }
        $view = ':admin/operatori:new.html.twig';
        if ($User->getId()) {
            $view = ':admin/operatori:edit.html.twig';
        }

        return $this->render(
            $view,
            ['form' => $form->createView(), 'supportData' => $supportData]
        );
    }

    /**
     * @Route("/operatori/delete/{id}", name="operatori_delete")
     *
     * @param Request $request
     * @param User $User
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, User $User)
    {

        $elemento = $User->getNome() . ' ' . $User->getCognome() . ' (' . $User->getId() . ')';
        $imageHandler = $this->get('app.base64_image');
        $imageHandler->saveTo('', $User, $User->getUsername() . '.jpg', 1);
        $em = $this->getDoctrine()->getManager();
        $em->remove($User);
        $em->flush();
        $this->addFlash('success', 'Operatore ' . $elemento . ' eliminato');

        return $this->redirectToRoute('operatori_list');
    }

    /**
     * @Route("/operatori/toggle-enabled/{id}", name="operatori_toggle_enabled")
     *
     * @param Request $request
     * @param User $User
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toggleIsEnabledAction(Request $request, User $User)
    {

        $elemento = $User->getNome() . ' ' . $User->getCognome() . ' (' . $User->getId() . ')';
        $em = $this->getDoctrine()->getManager();
        $flash = 'Operatore ' . $elemento . ' ';
        if ($User->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }
        $User->setIsEnabled(!$User->getIsEnabled());
        $em->persist($User);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('operatori_list');
    }
}
