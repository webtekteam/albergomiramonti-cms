<?php
// 09/01/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\VetrinaCategory;
use AppBundle\Form\CategorieNewsForm;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_CATNEWS')")
 */
class CategorieNewsController extends Controller
{

    /**
     * @Route("/newscat", name="newscat")
     */
    public function listAction()
    {

        return $this->render('admin/newscat/list.html.twig');

    }

    /**
     * @Route("/newscat/new", name="newscat_new")
     */
    public function newAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(CategorieNewsForm::class, null,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $NewsCategory NewsCategory
             */

            $NewsCategory = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($NewsCategory);
            $em->flush();

            $elemento = '"' . $NewsCategory->translate('it')->getTitolo() . '" (' . $NewsCategory->getId() . ')';

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "' . $elemento . '" ' . $translator->trans('default.labels.creata'));

            return $this->redirectToRoute('newscat');

        }

        return $this->render('admin/newscat/new.html.twig', ['newsCatForm' => $form->createView()]);

    }

    /**
     * @Route("/newscat/toggle-enabled/{id}", name="newscat_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, NewsCategory $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = '"' . $newsCategory->translate($request->getLocale())->getTitolo() . '" (' . $newsCategory->getId() . ')';

        if (!$newsCategory->getIsEnabled() && $em->getRepository('AppBundle:NewsCategory')->countAllActive() >= $this->getParameter('blog') ['max_active_category']) {

            $this->addFlash('error', $translator->trans('newscat.messages.cant_enable_more',
                ['howmany' => $this->getParameter('max_active_category')]));

            return $this->redirectToRoute('newscat');

        }

        $flash = 'Categoria ' . $elemento . ' ';

        if ($newsCategory->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $newsCategory->setIsEnabled(!$newsCategory->getIsEnabled());

        $em->persist($newsCategory);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('newscat');


    }


    /**
     * @Route("/newscat/edit/{id}", name="newscat_edit")
     */
    public function editAction(Request $request, NewsCategory $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(CategorieNewsForm::class, $newsCategory,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($newsCategory);
            $em->flush();

            $elemento = '"' . $newsCategory->translate($request->getLocale())->getTitolo() . '" (' . $newsCategory->getId() . ')';

            $translator = $this->get('translator');

            $this->addFlash('success',
                'Categoria ' . $elemento . ' ' . $translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('newscat');

        }

        return $this->render('admin/newscat/new.html.twig', ['newsCatForm' => $form->createView()]);

    }


    /**
     * @Route("/newscat/json", name="newscat_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $CategorieNewsList = $em->getRepository('AppBundle:NewsCategory')->findAll();
        } else {
            $CategorieNewsList = $em->getRepository('AppBundle:NewsCategory')->findAllNotDeleted();
        }

        $retData = [];

        $vetrine = [];

        foreach ($CategorieNewsList as $NewsCategory) {
            /**
             * @var $NewsCategory NewsCategory
             */
            $record = [];
            $record['id'] = $NewsCategory->getId();
            $record['titolo'] = $NewsCategory->translate($request->getLocale())->getTitolo();
            $record['template'] = $NewsCategory->getTemplate();
            $record['deleted'] = $NewsCategory->isDeleted();
            $record['isEnabled'] = $NewsCategory->getIsEnabled();
            $record['createdAt'] = $NewsCategory->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $NewsCategory->getUpdatedAt()->format('d/m/Y H:i:s');

            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/newscat/restore/{id}", name="newscat_restore")
     */
    public function restoreAction(Request $request, NewsCategory $newsCategory)
    {

        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"' . $newsCategory->translate($request->getLocale())->getTitolo() . '" (' . $newsCategory->getId() . ')';

            $newsCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success',
                'Categoria ' . $elemento . ' ' . $translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('newscat');

    }


    /**
     * @Route("/newscat/delete/{id}/{force}", name="newscat_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, NewsCategory $newsCategory)
    {

        $elemento = '"' . $newsCategory->translate($request->getLocale())->getTitolo() . '" (' . $newsCategory->getId() . ')';

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();
        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($newsCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success',
                    'Categoria "' . $elemento . '" ' . $translator->trans('default.labels.eliminata'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash('error',
                    'Categoria "' . $elemento . '" ' . $translator->trans('news.errors.non_cancellabile'));

            }


        } elseif (!$newsCategory->isDeleted()) {

            $newsCategory->setIsEnabled(0);
            $em->flush();

            $em->persist($newsCategory);

            $translator = $this->get('translator');

            $this->addFlash('success',
                'Categoria "' . $elemento . '" ' . $translator->trans('default.labels.eliminata'));

            $em->remove($newsCategory);
            $em->flush();


        }


        return $this->redirectToRoute('newscat');

    }

    /**
     * @Route("/newscat/sort", name="newscat_sort")
     */
    public function sortAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $categorie = [];

        $Categories = $em->getRepository('AppBundle:NewsCategory')->findAllNotDeleted();

        foreach ($Categories as $CategoriaNews) {

            /**
             * @var $CategoriaNews NewsCategory
             */
            $categorie[$CategoriaNews->getId()] = $CategoriaNews->translate($request->getLocale())->getTitolo();
        }

        return $this->render('admin/newscat/sort.html.twig', ['categorie' => $categorie]);

    }

    /**
     * @Route("/newscat/save-sort", name="newscat_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('AppBundle:NewsCategory')->getSortedByIds($request->get('sorted'));

            foreach ($res as $k => $Category) {
                /**
                 * @var $Category NewsCategory
                 */

                $Category->setSort($k);
                $em->persist($Category);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}
