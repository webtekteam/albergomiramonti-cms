<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Controller\Front;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
     * @throws Exception
     *
     * @return Response
     * @Route("/", defaults={"_locale"="it"}, name="home_it")
     * @Route("/{_locale}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="home")
     */
    public function indexController()
    {
        $em = $this->getDoctrine()
            ->getManager();
        $seo_manager = $this->get('app.seo-manager');
        $template_loader = $this->get('app.template_loader');

        $page = $em->getRepository('AppBundle:Page')
            ->findOneBy(['template' => 'home']);
        if (!$page) {
            throw new Exception('Devi creare una pagina con template "home"');
        }

        $meta = $seo_manager->run($page);

        $Languages = $this->get('app.languages');

        $AdditionalData = [];
        $AdditionalData['Entity'] = $page;
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

        $AdditionalData['META'] = $meta;

        $twigs = $template_loader->getTwigs('home', $Languages->getActivePublicLanguages(), $AdditionalData);

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $meta]);
    }
}
