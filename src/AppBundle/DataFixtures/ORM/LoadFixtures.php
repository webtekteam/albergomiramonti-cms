<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 14.15
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Template;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures implements FixtureInterface
{

    private $manager;

    public function load(ObjectManager $manager)
    {

        $this->manager = $manager;
        $this->loadLanguages();
        $this->loadTemplates();
        Fixtures::load(
            __DIR__ . '/*.yml',
            $manager,
            ['providers' => [$this]]
        );
    }

    public function now()
    {

        return new \DateTime();
    }


    private function loadLanguages()
    {

        $this->languages = [
            [
                'shortCode' => 'it',
                'languageName' => 'Italiano',
                'locale' => 'it_IT',
                'isEnabled' => 1,
            ],
            [
                'shortCode' => 'en',
                'languageName' => 'Inglese',
                'locale' => 'en_EN',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'fr',
                'languageName' => 'Francese',
                'locale' => 'fr_FR',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'de',
                'languageName' => 'Tedesco',
                'locale' => 'de_DE',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'ru',
                'languageName' => 'Russo',
                'locale' => 'ru_RU',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'pt',
                'languageName' => 'Portoghese',
                'locale' => 'pt_PT',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'nl',
                'languageName' => 'Olandese',
                'locale' => 'nl_NL',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'zh',
                'languageName' => 'Cinese',
                'locale' => 'zh_CN',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'jp',
                'languageName' => 'Giapponese',
                'locale' => 'ja_JP',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'pl',
                'languageName' => 'Polacco',
                'locale' => 'pl_PL',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'cs',
                'languageName' => 'Cecoslovacco',
                'locale' => 'cs_CS',
                'isEnabled' => 0,
            ],
            [
                'shortCode' => 'es',
                'languageName' => 'Espanol',
                'locale' => 'es_ES',
                'isEnabled' => 0,
            ],
        ];

    }

    public function languages($index, $key)
    {

        return $this->languages[$index][$key];
    }

    public function loadTemplates()
    {

        $data = Yaml::parse(file_get_contents(__DIR__ . '/05_template.yml_raw'));
        foreach ($data['AppBundle\Entity\Template'] as $template) {
            if (!is_array($template['config'])) {
                $template['config'] = json_decode($template['config'], true);
            } else {
                $template['config'] = [];
            }
            $Template = new Template();
            $Template->setTemplate($template['template']);
            $Template->setModule($template['module']);
            $Template->setView($template['view']);
            $Template->setCategory($template['category']);
            $Template->setPosition($template['position']);
            $Template->setInstance($template['instance']);
            $Template->setType($template['type']);
            $Template->setContainer($template['container']);
            $Template->setConfig($template['config']);
            $Template->setCodice($template['codice']);
            $Template->setIsEnabled($template['isEnabled']);
            $Template->setLayout($template['layout']);
            $Template->setRequiredRole($template['requiredRole']);
            $this->manager->persist($Template);
        }
        $this->manager->flush();
    }

}