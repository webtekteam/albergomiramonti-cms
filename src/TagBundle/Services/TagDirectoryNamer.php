<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 28/12/16
 * Time: 9.27
 */

namespace TagBundle\Services;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class TagDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $Tag = $object;

        $dir = $Tag->getId();

        return $dir;

    }


}