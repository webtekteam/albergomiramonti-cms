<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Controller\Front;

use AppartamentiBundle\Entity\Appartamento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AppartamentoController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/camera/{slug}", defaults={"_locale"="it"}, name="appartamento_it")
     * @Route("/{_locale}/camera/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl"}, defaults={"_locale"="it"}, name="appartamento")
     */
    public function readAction(Request $request)
    {
        $seo_manager = $this->get('app.seo-manager');

        $em = $this->getDoctrine()
            ->getManager();
        $appartamento_translation = $em->getRepository('AppartamentiBundle:AppartamentoTranslation')
            ->findOneBy(['slug' => $request->get('slug')]);

        if ($appartamento_translation) {
            /**
             * @var Appartamento $appartamento
             */
            $appartamento = $appartamento_translation->getTranslatable();
            if ($appartamento && $appartamento->getIsEnabled()) {
                $TemplateLoader = $this->get('app.template_loader');
                $Languages = $this->get('app.languages');
                $AdditionalData = [];
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
                $AdditionalData['Entity'] = $appartamento;

                $meta = $seo_manager->run($appartamento);
                $meta = array_merge($meta, $this->get('app.open_graph')
                    ->generateOpenGraphData($appartamento));

                $AdditionalData['META'] = $meta;
                $twigs = $TemplateLoader->getTwigs('appartamento', $Languages->getActivePublicLanguages(),
                    $AdditionalData);

                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $meta]);
            }
        }

        throw new NotFoundHttpException('Pagina non trovata');
    }
}
