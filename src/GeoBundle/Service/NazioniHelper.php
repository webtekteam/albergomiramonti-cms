<?php
// 19/04/17, 11.27
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Intl\Intl;

class NazioniHelper
{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * NazioniHelper constructor.
     */
    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function getNazioniChoices()
    {

        $country = $this->em->getRepository("GeoBundle:Nazioni")->findAll();

        $cache = [];
        foreach ($country as $Nazione) {
            $cache[$Nazione->getCountryCode()] = $Nazione;
        }

        $nazioniSymfony = Intl::getRegionBundle()->getCountryNames();

        $return = [];

        foreach ($nazioniSymfony as $code => $nazioneStrng) {
            $cache[$code]->setNome($nazioneStrng);
            $return[] = $cache[$code];
        }

        return $return;


    }

}