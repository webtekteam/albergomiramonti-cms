<?php
// 07/06/17, 11.40
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\RecordOrderRepository")
 * @ORM\Table(name="orders_records")
 */
class RecordOrder
{

    use Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Order", inversedBy="records")
     */
    private $Order;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\VarianteProdotto")
     */
    private $variante;

    /**
     * @ORM\Column(type="string")
     */
    private $descrizione;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $descrizioneVariante;

    /**
     * @ORM\Column(type="decimal",  precision=12, scale=6)
     */
    private $prezzo;

    /**
     * @ORM\Column(type="integer")
     */
    private $tax;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product")
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $qty;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {

        return $this->Order;
    }

    /**
     * @param mixed $Order
     */
    public function setOrder($Order)
    {

        $this->Order = $Order;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {

        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getVariante()
    {

        return $this->variante;
    }

    /**
     * @param mixed $variante
     */
    public function setVariante(VarianteProdotto $variante = null)
    {

        $this->variante = $variante;
    }

    /**
     * @return mixed
     */
    public function getPrezzo()
    {

        return $this->prezzo;
    }

    /**
     * @param mixed $prezzo
     */
    public function setPrezzo($prezzo)
    {

        $this->prezzo = $prezzo;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {

        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {

        $this->tax = $tax;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {

        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty)
    {

        $this->qty = $qty;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {

        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {

        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getDescrizioneVariante()
    {

        return $this->descrizioneVariante;
    }

    /**
     * @param mixed $descrizioneVariante
     */
    public function setDescrizioneVariante($descrizioneVariante)
    {

        $this->descrizioneVariante = $descrizioneVariante;
    }


}