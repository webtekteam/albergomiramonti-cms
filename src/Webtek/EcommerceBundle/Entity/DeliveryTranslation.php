<?php

namespace Webtek\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="delivery_translations")
 */
class DeliveryTranslation
{

    use ORMBehaviours\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;
    }


}

