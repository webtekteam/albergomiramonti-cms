<?php
// 07/06/17, 11.00
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{

    use ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $billing;

    /**
     * @ORM\Column(type="string")
     */
    private $delivery;

    /**
     * @ORM\Column(type="string")
     */
    private $deliverBy;

    /**
     * @ORM\ManyToOne(targetEntity="AnagraficaBundle\Entity\IndirizzoAnagrafica")
     */
    private $indirizzoAnagrafica;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Delivery")
     */
    private $deliverById;

    /**
     * @ORM\Column(type="string")
     */
    private $deliveryCost;

    /**
     * @ORM\Column(type="string")
     */
    private $deliveryTax;

    /**
     * @ORM\Column(type="decimal",  precision=10, scale=2)
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="AnagraficaBundle\Entity\Anagrafica")
     */
    private $anagraficaId;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\RecordOrder", mappedBy="Order", cascade={"persist"})
     */
    private $records;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\OrdersStatus")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\MetodiPagamento")
     */
    private $payment;

    /**
     * @ORM\Column(type="string")
     */
    private $paymentDescription;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $paymentDate;

    /**
     * @ORM\Column(type="string")
     */
    private $trackinUrl;

    /**
     * @ORM\Column(type="decimal",  precision=10, scale=2)
     */
    private $payedAmount;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $paymentUrl;

    private $recensibile = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $trackingUrl;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Coupon")
     */
    private $coupon;

    /**
     * @ORM\Column(type="decimal",  precision=5, scale=2)
     */
    private $couponScontoEuro;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $couponDescription;

    /**
     * @return bool
     */
    public function isRecensibile()
    {

        return $this->recensibile;
    }

    /**
     * @param bool $recensibile
     */
    public function setRecensibile($recensibile)
    {

        $this->recensibile = $recensibile;
    }

    /**
     * Order constructor.
     */
    public function __construct()
    {

        $this->records = new ArrayCollection();
        $this->setPaymentDate(null);
        $this->setTrackinUrl('');
        $this->setPayedAmount(0);

    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBilling()
    {

        return $this->billing;
    }

    /**
     * @param mixed $billing
     */
    public function setBilling($billing)
    {

        $this->billing = $billing;
    }

    /**
     * @return mixed
     */
    public function getDelivery()
    {

        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery($delivery)
    {

        $this->delivery = $delivery;
    }

    /**
     * @return mixed
     */
    public function getDeliverBy()
    {

        return $this->deliverBy;
    }

    /**
     * @param mixed $deliverBy
     */
    public function setDeliverBy($deliverBy)
    {

        $this->deliverBy = $deliverBy;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCost()
    {

        return $this->deliveryCost;
    }

    /**
     * @param mixed $deliveryCost
     */
    public function setDeliveryCost($deliveryCost)
    {

        $this->deliveryCost = $deliveryCost;
    }

    /**
     * @return mixed
     */
    public function getDeliveryTax()
    {

        return $this->deliveryTax;
    }

    /**
     * @param mixed $deliveryTax
     */
    public function setDeliveryTax($deliveryTax)
    {

        $this->deliveryTax = $deliveryTax;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {

        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {

        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getAnagraficaId()
    {

        return $this->anagraficaId;
    }

    /**
     * @param mixed $anagraficaId
     */
    public function setAnagraficaId($anagraficaId)
    {

        $this->anagraficaId = $anagraficaId;
    }

    /**
     * @return mixed
     */
    public function getRecords()
    {

        return $this->records;
    }

    /**
     * @param mixed $records
     */
    public function setRecords($records)
    {

        $this->records = $records;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {

        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {

        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {

        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {

        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {

        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     */
    public function setPaymentDate($paymentDate)
    {

        $this->paymentDate = $paymentDate;
    }

    /**
     * @return mixed
     */
    public function getTrackinUrl()
    {

        return $this->trackinUrl;
    }

    /**
     * @param mixed $trackinUrl
     */
    public function setTrackinUrl($trackinUrl)
    {

        $this->trackinUrl = $trackinUrl;
    }

    /**
     * @return mixed
     */
    public function getPayedAmount()
    {

        return $this->payedAmount;
    }

    /**
     * @param mixed $payedAmount
     */
    public function setPayedAmount($payedAmount)
    {

        $this->payedAmount = $payedAmount;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {

        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {

        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescription()
    {

        return $this->paymentDescription;
    }

    /**
     * @param mixed $paymentDescription
     */
    public function setPaymentDescription($paymentDescription)
    {

        $this->paymentDescription = $paymentDescription;
    }

    /**
     * @return mixed
     */
    public function getDeliverById()
    {

        return $this->deliverById;
    }

    /**
     * @param mixed $deliverById
     */
    public function setDeliverById($deliverById)
    {

        $this->deliverById = $deliverById;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {

        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {

        $this->locale = $locale;
    }


    public function getGranTotale()
    {

        return $this->getTotal() + $this->getDeliveryCost();
    }

    public function getPagato()
    {

        return $this->getPayedAmount() == $this->getGranTotale();
    }

    /**
     * @return mixed
     */
    public function getPaymentUrl()
    {

        return $this->paymentUrl;
    }

    /**
     * @param mixed $paymentUrl
     */
    public function setPaymentUrl($paymentUrl)
    {

        $this->paymentUrl = $paymentUrl;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }


    /**
     * @param $pagato bool
     */
    public function setPagato($pagato)
    {


        if ($pagato) {


            if ($this->getPayedAmount() == 0.00) {
                $this->setPayedAmount($this->getGranTotale());
                $this->setPaymentDate(new \DateTime());
            }

        } else {
            $this->setPayedAmount(0);
            $this->setPaymentDate(null);
        }
    }

    /**
     * @return mixed
     */
    public function getNote()
    {

        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {

        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getTrackingUrl()
    {

        return $this->trackingUrl;
    }

    /**
     * @param mixed $trackingUrl
     */
    public function setTrackingUrl($trackingUrl)
    {

        $this->trackingUrl = $trackingUrl;
    }

    /**
     * @return mixed
     */
    public function getIndirizzoAnagrafica()
    {

        return $this->indirizzoAnagrafica;
    }

    /**
     * @param mixed $indirizzoAnagrafica
     */
    public function setIndirizzoAnagrafica($indirizzoAnagrafica)
    {

        $this->indirizzoAnagrafica = $indirizzoAnagrafica;
    }

    /**
     * @return mixed
     */
    public function getCoupon()
    {

        return $this->coupon;
    }

    /**
     * @param mixed $coupon
     */
    public function setCoupon($coupon)
    {

        $this->coupon = $coupon;
    }

    /**
     * @return mixed
     */
    public function getCouponDescription()
    {

        return $this->couponDescription;
    }

    /**
     * @param mixed $couponDescription
     */
    public function setCouponDescription($couponDescription)
    {

        $this->couponDescription = $couponDescription;
    }

    /**
     * @return mixed
     */
    public function getCouponScontoEuro()
    {

        return $this->couponScontoEuro;
    }

    /**
     * @param mixed $couponScontoEuro
     */
    public function setCouponScontoEuro($couponScontoEuro)
    {

        $this->couponScontoEuro = $couponScontoEuro;
    }

}