<?php
// 12/01/17, 16.21
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use AppBundle\Traits\Seo;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners("Webtek\EcommerceBundle\EntityListener\BrandTranslationListener")
 * @ORM\Table(name="brands_translations")
 */
class BrandTranslation
{

    use ORMBehaviours\Translatable\Translation;
    use Seo, Loggable;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titolo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalUrl;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {

        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {

        $this->testo = $testo;
    }

    /**
     * @return mixed
     */
    public function getExternalUrl()
    {

        return $this->externalUrl;
    }

    /**
     * @param mixed $externalUrl
     */
    public function setExternalUrl($externalUrl)
    {

        $this->externalUrl = $externalUrl;
    }


}