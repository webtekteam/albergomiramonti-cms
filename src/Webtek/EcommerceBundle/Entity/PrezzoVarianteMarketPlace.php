<?php
// 15/05/17, 16.13
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\PrezzoVarianteRepository")
 * @ORM\Table(name="prezzi_varianti_marketplace")
 */
class PrezzoVarianteMarketPlace
{

    use ORMBehaviours\Timestampable\Timestampable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal",  precision=12, scale=6)
     */
    private $valore;

    private $valoreIvato;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\MarketPlace")
     * @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     */
    private $marketPlace;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\VarianteProdotto")
     * @ORM\JoinColumn(name="variante_prodotto_id", referencedColumnName="id")
     */
    private $varianteProdotto;

    public function __construct()
    {
    }

    function __toString()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $valore
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    /**
     * @return mixed
     */
    public function getMarketPlace()
    {

        return $this->marketPlace;
    }

    /**
     * @param mixed $marketPlace
     */
    public function setMarketPlace($marketPlace)
    {

        $this->marketPlace = $marketPlace;
    }

    /**
     * @return mixed
     */
    public function getVarianteProdotto()
    {

        return $this->varianteProdotto;
    }

    /**
     * @param mixed $varianteProdotto
     */
    public function setVarianteProdotto($varianteProdotto)
    {

        $this->varianteProdotto = $varianteProdotto;
    }

    /**
     * @return mixed
     */
    public function getValoreIvato()
    {

        return $this->valoreIvato;
    }

    /**
     * @param mixed $valoreIvato
     */
    public function setValoreIvato($valoreIvato)
    {

        $this->valoreIvato = $valoreIvato;
    }


}