<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="variante_prodotto_translations")
 */
class VarianteProdottoTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @return mixed
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;
    }


}

