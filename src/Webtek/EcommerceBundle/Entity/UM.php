<?php

namespace Webtek\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\UMRepository")
 * @ORM\Table(name="um")
 */
class UM
{

    use Timestampable, SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $label;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {

        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {

        $this->label = $label;
    }

    public function __toString()
    {

        return $this->getLabel();
    }


}