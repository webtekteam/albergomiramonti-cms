<?php
// 06/11/17, 8.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="coupon_translations")
 */
class CouponTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titolo;

    /**
     * @Assert\NotBlank()
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

}

