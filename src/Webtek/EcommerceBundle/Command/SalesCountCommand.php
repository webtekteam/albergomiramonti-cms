<?php
// 13/06/17, 17.24
// @author : gabricom

namespace Webtek\EcommerceBundle\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Webtek\EcommerceBundle\Service\SalesCounter;


class SalesCountCommand extends ContainerAwareCommand
{
    private $container;


    protected function configure()
    {

        $this->setName('ecommerce:sales:count')
            ->setDescription('Tool per il conteggio dei prodotti venduti')
            ->setHelp('Conteggia i prodotti venduti e li salva in db');
    }


    private function setUp(OutputInterface $output)
    {

        $style = new OutputFormatterStyle('white', 'green', ['blink']);
        $output->getFormatter()->setStyle('webtek', $style);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();

        $this->setUp($output);

        $salesCounter = $this->container->get('app.webtek_ecommerce.services.sales_counter');

        /**
         * @var $salesCounter SalesCounter
         */

        $salesCounter->countSales();
        $output->writeln("Calcolo Completato e dati salvati su db");
    }


}