<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 03/08/17
 * Time: 16.45
 */

namespace Webtek\EcommerceBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Webtek\EcommerceBundle\Entity\Order;


/**
 * The order.placed event is dispatched each time an order is created
 * in the system.
 */
class OrderStatusChangeEvent extends Event
{
    const NAME = 'order.statusChanged';

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }
}