<?php
// 18/09/17, 7.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Event;

use AnagraficaBundle\Entity\Anagrafica;
use Symfony\Component\EventDispatcher\Event;


class AnagraficaSavedEvent extends Event
{

    const NAME = 'anagrafica.saved';

    protected $Anagrafica;

    public function __construct(Anagrafica $Anagrafica)
    {

        $this->Anagrafica = $Anagrafica;
    }

    public function getAnagrafica()
    {

        return $this->Anagrafica;
    }
}