<?php
// 03/05/17, 9.06
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\DeliveryPrices;
use GeoBundle\Service\NazioniHelper;

class DeliveryPricesForm extends AbstractType
{

    /**
     * @var NazioniHelper
     */
    private $nazioniHelper;
    /**
     * @var EntityManager
     */
    private $em;


    /**
     * DeliveryPricesForm constructor.
     */
    public function __construct(EntityManager $em, NazioniHelper $nazioniHelper)
    {

        $this->nazioniHelper = $nazioniHelper;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'sogliaMin',
            NumberType::class,
            [
                'label' => 'delivery.labels.soglia_min',
                'scale' => 2,
            ]
        );
        $builder->add(
            'sogliaMax',
            NumberType::class,
            [
                'label' => 'delivery.labels.soglia_max',
                'scale' => 2,
            ]
        );
        $builder->add(
            'cost',
            NumberType::class,
            [
                'label' => 'delivery.labels.costo',
                'scale' => 2,
            ]
        );
        $builder->add(
            'tutte',
            ChoiceType::class,
            [
                'label' => 'delivery.labels.tutte_le_nazioni',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'attr' => [
                    'class' => 'tutteLeNazioni',
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'nations',
            EntityType::class,
            [
                'class' => 'GeoBundle\Entity\Nazioni',
                'choices' => $this->nazioniHelper->getNazioniChoices(),
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'pannello-nazioni',
                ],
                'preferred_choices' => [$this->em->getRepository("GeoBundle:Nazioni")->find("IT")],
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => DeliveryPrices::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}