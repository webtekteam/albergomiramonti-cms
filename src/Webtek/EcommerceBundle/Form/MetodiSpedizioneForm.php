<?php
// 16/03/2017 , 09:53
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Form;

use Webtek\EcommerceBundle\Entity\MetodiSpedizione;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Webtek\EcommerceBundle\Form\Type\ScontoPrezzoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Intl\Intl;
use Webtek\EcommerceBundle\Service\NazioniHelper;

class MetodiSpedizioneForm extends AbstractType
{

    private $em;
    /**
     * @var NazioniHelper
     */
    private $nazioniHelper;

    /**
     * NewsForm constructor.
     */
    public function __construct(EntityManager $em, NazioniHelper $nazioniHelper)
    {

        $this->em = $em;
        $this->nazioniHelper = $nazioniHelper;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /*
         * PARAMETRI GENERALI
         */
        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'metodi_spedizione_ecommerce.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add(
            'nome',
            TextType::class,
            ['label' => 'metodi_spedizione_ecommerce.labels.nome', 'attr' => ['class' => ''], 'required' => true]
        );
        $builder->add(
            'tipo',
            ChoiceType::class,
            [
                'label' => 'metodi_spedizione_ecommerce.labels.tipo',
                'attr' => ['class' => ''],
                'required' => true,
                'choices' => [
                    "metodi_spedizione_ecommerce.labels.perordine" => 'ordine',
                    "metodi_spedizione_ecommerce.labels.perquantita" => "quantita",
                ],
            ]
        );
        $builder->add(
            'descrizione',
            TextareaType::class,
            [
                'label' => 'metodi_spedizione_ecommerce.labels.descrizione',
                'attr' => ['class' => 'ck'],
                'required' => true,
            ]
        );
        $builder->add(
            'validitaNazioni',
            ChoiceType::class,
            [
                'label' => 'metodi_spedizione_ecommerce.labels.validitaNazioni',
                'choices' => [
                    'metodi_spedizione_ecommerce.labels.validopertuttelenazioni' => true,
                    'metodi_spedizione_ecommerce.labels.validopernazionispecifiche' => false,
                ],
                'placeholder' => false,
                'attr' => [
                    'class' => 'scelta-nazioni',
                ],
                'required' => false,
            ]
        );
        $builder->add(
            'nazioni',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\Nazioni',
                'choices' => $this->nazioniHelper->getNazioniChoices(),
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'pannello-nazioni',
                ],
                'label_attr' => ['class' => 'pannello-nazioni'],
                'preferred_choices' => [$this->em->getRepository("WebtekEcommerceBundle:Nazioni")->find("IT")],
            ]
        );
        /*
         * PREZZO
         */
        $builder->add(
            'prezzo',
            MoneyType::class,
            ['label' => 'ecommerce.labels.prezzo', 'attr' => ['class' => 'js-prezzo'], 'required' => true]
        );
        $builder->add(
            'codiceIva',
            EntityType::class,
            [
                'class' => "Webtek\EcommerceBundle\Entity\Tax",
                'choice_label' => "codice",
                'attr' => ['class' => 'js-codiceiva'],
            ]
        );

        /*
         * PREZZI BASATI SU UNA SOGLIA DI PREZZO
         */

        $builder->add(
            'sconti_prezzo',
            CollectionType::class,
            [
                'entry_type' => ScontoPrezzoType::class,
                'allow_add' => true,
                "allow_delete" => true,
                'by_reference' => false,
            ]
        );

        /*
         * INDIRIZZO DI FATTURAZIONE DEL METODO DI SPEDIZIONE
         */

        $builder->add(
            'nomeFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.nome', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'indirizzoFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.indirizzo', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'capFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.cap', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'cittaFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.citta', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'provinciaFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.provincia', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'nazioneFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.nazione', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'telefonoFatturazione',
            TextType::class,
            ['label' => 'ecommerce.labels.telefono', 'attr' => ['class' => ''], 'required' => false]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => MetodiSpedizione::class,
                'error_bubbling' => true,
                'locale' => 'en',
            ]
        );
    }


}
