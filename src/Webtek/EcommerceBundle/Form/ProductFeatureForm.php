<?php
// 10/05/17, 10.33
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFeatureForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('id', HiddenType::class);
        $builder->add(
            'featureGroup',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\GroupFeature',
                'attr' => ['class' => 'group-feature'],
                'label' => false,
            ]
        );

        $builder->add(
            'feature',
            ChoiceType::class,
            [
                'choices' => ['product.labels.select_group_feature' => 0],
                'attr' => ['class' => 'predefined-features'],
                'label' => false,
            ]
        );

        $builder->add(
            'textValue',
            TextType::class,
            [
                'attr' => ['class' => 'free-feature disabled', 'disabled' => true],
                'label' => false,
            ]
        );

        $builder->get('feature')->resetViewTransformers();

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'label' => false,
            ]
        );
    }

}