<?php
// 06/06/17, 17.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Webtek\EcommerceBundle\Repository\PaymentRepository;

class SceltaPagamentoForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('Address', HiddenType::class);
        $builder->add('Delivery', HiddenType::class);

        $builder->add(
            'note',
            TextareaType::class,
            ['attr' => ['placeholder' => 'ecommerce.labels.note_ordine'], 'required' => false]
        );
        $builder->add('condizioni_vendita', CheckboxType::class, ['label' => 'ecommerce.labels.condizioni_vendita']);

        $builder->add(
            'Payment',
            EntityType::class,
            [
                'label' => false,
                'expanded' => true,
                'class' => 'Webtek\EcommerceBundle\Entity\MetodiPagamento',
                'query_builder' => function ($er) {

                    /**
                     * @var $er PaymentRepository
                     */
                    return $er->getActivePaymentsQB();

                },
            ]
        );

    }
}