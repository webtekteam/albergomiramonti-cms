<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use Webtek\EcommerceBundle\Entity\Setup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SetupForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'ragioneSociale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.ragioneSociale', 'attr' => ['class' => ''], 'required' => true]
        );
        $builder->add('email', EmailType::class, ['label' => 'Email', 'attr' => ['class' => ''], 'required' => true]);
        $builder->add(
            'partitaIva',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.partitaIva', 'attr' => ['class' => ''], 'required' => true]
        );

        $builder->add(
            'indirizzoLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.indirizzo', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'capLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.cap', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'cittaLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.citta', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'provinciaLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.provincia', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'nazioneLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.nazione', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'telefonoLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.telefono', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'faxLegale',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.fax', 'attr' => ['class' => ''], 'required' => false]
        );

        $builder->add(
            'indirizzoOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.indirizzo', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'capOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.cap', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'cittaOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.citta', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'provinciaOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.provincia', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'nazioneOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.nazione', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'telefonoOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.telefono', 'attr' => ['class' => ''], 'required' => false]
        );
        $builder->add(
            'faxOperativa',
            TextType::class,
            ['label' => 'setup_ecommerce.labels.fax', 'attr' => ['class' => ''], 'required' => false]
        );

        $builder->add(
            'privacyPolicy',
            TextareaType::class,
            ['label' => 'setup_ecommerce.labels.privacyPolicy', 'attr' => ['class' => 'ck'], 'required' => false]
        );
        $builder->add(
            'condizioniVendita',
            TextareaType::class,
            ['label' => 'setup_ecommerce.labels.condizioniVendita', 'attr' => ['class' => 'ck'], 'required' => false]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Setup::class,
                'error_bubbling' => true,
            ]
        );
    }


}
