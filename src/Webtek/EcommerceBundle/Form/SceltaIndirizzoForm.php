<?php
// 05/06/17, 12.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SceltaIndirizzoForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'indirizzoId',
            EntityType::class,
            [
                'label' => false,
                'expanded' => true,
                'class' => 'AnagraficaBundle\Entity\IndirizzoAnagrafica',
                'query_builder' => function (EntityRepository $er) use ($options) {

                    $qb = $er->createQueryBuilder('u');
                    if ($options['anagrafica']) {
                        $qb->andWhere('u.anagrafica = :anagrafica')
                            ->setParameter('anagrafica', $options['anagrafica']);
                    }

                    return $qb;

                },
            ]
        );


    }


    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'error_bubbling' => false,
                'locale' => 'en',
                'anagrafica' => false,
            ]
        );
    }


}