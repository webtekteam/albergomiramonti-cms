var modulo = 'categorie_prodotti';
var form_name = 'form[name=\'categorie_prodotti_form\']';

var loader = new function() {
  this.start = function() {
    $('.ajax_loader').show();
    $('#pannello').addClass('loading-ajax');
  };
  this.stop = function() {
    $('.ajax_loader').hide();
    $('#pannello').removeClass('loading-ajax');
  };
};

function rigeneraCk() {
  if ($('.tab-pane').length) {
    var $tabPane = $('.tab-pane');
    $tabPane.find('.ck').each(function() {
      attivaCkEditor(this);
    });
  }
}

$(function() {

  loader.stop();

  $(document).on('keyup', '.slug', function(e) {
    this.value = this.value.toLowerCase().replace(/\s/g, '-');
  });
  $(document).on('change', '.slug', function(e) {

    this.value = this.value.toLowerCase().replace(/\s/g, '-');
  });

  function inizializzaAjaxForm() {
    $('form[name=\'categorie_prodotti_form\']').ajaxForm({
      beforeSerialize: function($form, options) {
        $form.find('.ck').each(function(index, el) {
          $(el).val(CKEDITOR.instances[el.id].getData());
        });
        HandleBarHelper.lockScreen();
      },
      dataType: 'json',
      success: function(r) {
        $alberoCategorie.jstree(true).refresh();
        if (typeof r.form !== 'undefined') {
          destroyCkEditor();
          $('form[name=\'categorie_prodotti_form\']').html(r.form);
          rigeneraCk();
        }

        if (typeof r.nodo_creato !== 'undefined') {
          $alberoCategorie.one('refresh.jstree', function() {

            $alberoCategorie.jstree(true).deselect_all();
            $alberoCategorie.jstree(true).select_node(r.nodo_creato);
          });

        }
        HandleBarHelper.unlockScreen();
      },
      error: function(r) {
        if (typeof r.responseJSON.form !== 'undefined') {
          $('form[name=\'categorie_prodotti_form\']').
              html('').
              html(r.responseJSON.form);
          rigeneraCk();
        }
        HandleBarHelper.unlockScreen();
      },
    });
  }

  rigeneraCk();
  inizializzaAjaxForm();

  var $alberoCategorie = $('.js-tree.category_list');

  $alberoCategorie.jstree({
    'core': {
      'check_callback': true,
      'data': {
        'url': window.location.href + '/json-tree',
        'type': 'post',
        'dataType': 'json',
        'error': function(r) {

        },
        'data': function(node) {
          return {
            'id': node.id,
          };
        },

      },
    },
    'contextmenu': {
      'items': function($node) {
        var tree = $alberoCategorie.jstree(true);
        return {
          'Create': {
            'separator_before': false,
            'separator_after': false,
            'label': 'Crea',
            'action': function(obj) {
              $('#nuova_sottocategoria').click();
            },
          },
          'Remove': {
            'separator_before': false,
            'separator_after': false,
            'label': 'Elimina',
            'action': function(obj) {
              var nodi = $alberoCategorie.jstree(true).get_selected();
              if (nodi.length > 1) {
                eliminazione_nodi(nodi);
              }
              else {
                eliminaNodo(window.location.href + '/delete/' + id + '/1',
                    text);
              }

            },
          },
        };
      },
    },
    'force_text': true,
    'plugins': ['contextmenu', 'dnd', 'search', 'wholerow'],
  });

  $alberoCategorie.on('ready.jstree', function(e, data) {
    $alberoCategorie.jstree().open_all();
  });

  $alberoCategorie.on('move_node.jstree', function(e, data) {
    $.ajax({
      url: window.location.href + '/move',
      type: 'POST',
      dataType: 'json',
      data: {
        'categoria': data.node.id,
        'old_parent': data.old_parent,
        'old_position': data.old_position,
        'parent': data.parent,
        'position': data.position,
      },
    }).done(function() {

    }).fail(function() {

    }).always(function() {

    });

  });

  var text = 0;
  $alberoCategorie.on('changed.jstree', function(e, data) {
    if (typeof data.node !== 'undefined') {
      text = data.node.text;
      id = data.selected[0];
      if (typeof id !== 'undefined') {
        destroyCkEditor();
        loader.start();
        $(form_name).
            parent('.x_content').
            load(window.location.href + '/edit/' + id, function(r) {
              $('.label_new').hide();
              $('.label_new_child').hide();
              $('.label_edit span').text(text);
              $('.label_edit').show();

              rigeneraCk();
              inizializzaAjaxForm();
              loader.stop();
              $('#nuova_sottocategoria').attr('disabled', false);
            });
      }
    }

  });

  $('#nuova_sottocategoria').click(function(event) {
    destroyCkEditor();
    loader.start();
    $(form_name).
        parent('.x_content').
        load(window.location.href + '/new/child/' + id, function(r) {
          $('.label_edit').hide();
          $('.label_new').hide();
          $('.label_new_child').show();
          $('.label_new_child span').text(' di ' + text).show();
          rigeneraCk();
          inizializzaAjaxForm();
          loader.stop();
        });
  });

  $('#nuova_categoria').click(function(event) {
    destroyCkEditor();
    loader.start();
    $(form_name).
        parent('.x_content').
        load(window.location.href + '/new', function(r) {
          $('.label_edit').hide();
          $('.label_new_child').hide();
          $('.label_new').show();
          rigeneraCk();
          inizializzaAjaxForm();

          loader.stop();
        });
  });

  function eliminaNodo(path, title) {
    var question = _('Vuoi davvero eliminare l\'elemento "%s"?', 'default',
        title);

    var opts = {
      'type': 'danger',
      'titolo': _('Conferma cancellazione'),
      'content': question,
      'OK': 'Ok',
      'CANCEL': 'Annulla',
      'onOK': function($modal) {
        $modal.modal('hide');
        $.ajax({
          url: path,
          type: 'POST',
          dataType: 'json',
        }).done(function(r) {

          $alberoCategorie.jstree(true).delete_node(id);
          $('#nuova_categoria').click();
        }).fail(function() {

        }).always(function(r) {

        });
      },
    };
    HandleBarHelper.confirm(opts);
  }

  function eliminazione_nodi(nodi) {
    var question = _('Vuoi davvero eliminare gli elementi selezionati?',
        'default');

    var opts = {
      'type': 'danger',
      'titolo': _('Conferma cancellazione'),
      'content': question,
      'OK': 'Ok',
      'CANCEL': 'Annulla',
      'onOK': function($modal) {
        $modal.modal('hide');
        $alberoCategorie.jstree(true).deselect_all();
        var funzioni = [];
        $.each(nodi, function(index, el) {
          funzioni.push($.ajax({
            url: window.location.href + '/delete/' + el + '/1',
            type: 'POST',
            dataType: 'json',
          }));

        });
        $.when.apply($, funzioni).done(function() {
          $alberoCategorie.jstree(true).delete_node(nodi);
          $('#nuova_categoria').click();
        });

      },
    };
    HandleBarHelper.confirm(opts);
  }

  $('#pannello').on('click', '.delete', function() {
    var path = $(this).data('path');

    var title = $(this).data('title');

    eliminaNodo(path, title);

  });

  $('#pannello').on('click', '.btn-delete-list-img', function(e) {
    e.preventDefault();
    var opts = {
      'type': 'danger',
      'titolo': _('Cancellazione immagine elenchi', 'news'),
      'content': _(
          'Vuoi eliminare l\'immagine per gli elenchi?<br /><strong>N.B.<br />L\'operazione sarà irreversibile</strong>'),
      'onOK': function($modal) {
        $('#CategoryImg').find('img').remove();
        $('#CategoryImg').find('.form-group').remove();
        $('#categorie_prodotti_form_CategoryImg').val('');
        $('#categorie_prodotti_form_CategoryImgFileName').val('');
        $('#news_list_img').find('.missingImage').removeClass('hide');
        $('categorie_prodotti_form_deleteCategoryImg').val(1);
        $modal.modal('hide');
      },
    };
    HandleBarHelper.confirm(opts);
  });

  $('#pannello').on('change', '#upload_list', function() {
    var dimensioni = $(this).closest('.panelImg').data('dimensions');
    readFileB64(this, dimensioni, 'elenchi');
  });

});

/** FINE document.ready **/

/**
 * Riceve in ingresso il file selezionato per l'upload
 */
function readFileB64(input, dimensioni, type) {

  if (typeof type == 'undefined') {
    type = 'header';
  }

  var d = dimensioni.split('x');

  if (input.files && input.files[0]) {

    if (input.files[0].size > $body.data('max_upload_file_size')) {

      var opts = {
        'type': 'danger',
        'titolo': _('Dimensioni file non consentite'),
        'content': _(
            'Non posso permetterti di caricare un file cosi grande (%s), <strong>il massimo consentito è %s</strong>',
            'default', humanFileSize(input.files[0].size, true),
            humanFileSize(1000000, true)),
      };
      HandleBarHelper.alert(opts);

    } else {

      var modalTitle;

      modalTitle = _('Caricamento immagine per le categorie',
          'category_products');
      defaultAlt = 'Immagine Categoria';

      var reader = new FileReader();

      reader.onload = function(e) {

        var opts = {
          'id': 'modalCroppie',
          'data': {'title': modalTitle},
          'onShow': function($modal) {

            $modal.find('#categorie_prodotti_form_CategoryImgFilenameAlt').
                closest('.form-group').
                remove();

            var w = d[0] / 2;
            var h = d[1] / 2;

            var alt = '';

            $('.titolo').each(function() {
              if ($.trim($(this).val()) != '') {
                alt += $.trim($(this).val());
                alt += ': ';
                return false;
              }
            });

            alt += defaultAlt;

            $modal.find('input[type="text"]').val(alt);

            $('.croppie').height(h + h * (20 / 100));

            var croppieOpts = {
              viewport: {
                width: w,
                height: h,
                type: 'square',
              },
              boundary: {
                width: w,
                height: h,
              },
              enableExif: true,
            };

            var $croppie = $modal.find('.croppie').croppie(croppieOpts);

            $croppie.croppie('bind', {
              url: e.target.result,
            });

            $modal.find('.btn-cancel').on('click', function(e) {
              $modal.modal('hide');
            });

            $modal.find('.btn-confirm').on('click', function(e) {
              e.preventDefault();
              $croppie.croppie('result', {
                type: 'base64',
                size: {width: d[0], height: d[1]},
                format: 'jpeg',
              }).then(function(resp) {

                var AltImpostato = $modal.find('#alt').val();

                $('#categorie_prodotti_form_CategoryImg').val(resp);
                var $listImg = $('#CategoryImg');
                $listImg.find('.missingImage').addClass('hide');
                $listImg.find('.img-responsive').remove();
                $('#categorie_prodotti_form_deleteCategoryImg').val('0');
                $('.btn-delete-list-img').removeClass('disabled');
                $('<img class="img-responsive" src="' + resp + '" />').
                    prependTo($('#CategoryImg'));
                $('#categorie_prodotti_form_CategoryImgFileNameAlt').
                    closest('.form-group').
                    parent().
                    removeClass('hide');
                $('#categorie_prodotti_form_CategoryImgFileNameAlt').
                    val(AltImpostato);
                $('#categorie_prodotti_form_CategoryImgFileName').
                    val(input.files[0].name);

                $croppie.croppie('destroy');
                $modal.modal('hide');

              });
            });
          },
        };
        HandleBarHelper.modal(opts);
      };

      reader.readAsDataURL(input.files[0]);

    }
  }
  else {
    HandleBarHelper.alert({
      'content': _(
          'Il tuo browser non supporta le funzionalità richieste da questo componente.'),
    });
  }
}

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {
  var id = textarea.id;
  // var data=$(textarea).val();

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {
    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' +
      $body.data('seo') + '.js',
    });
  } else {
    CKEDITOR.instances[id].destroy();
    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' +
      $body.data('seo') + '.js',
    });
  }

}

function destroyCkEditor() {
  for (var instance in CKEDITOR.instances) {
    CKEDITOR.instances[instance].destroy();
  }

}
