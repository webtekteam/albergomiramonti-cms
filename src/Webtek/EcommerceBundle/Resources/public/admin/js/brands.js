var modulo = 'brands';

$(function() {

  $(document).on('keyup', '.slug', function(e) {
    this.value = this.value.toLowerCase().replace(/\s/g, '-');
  });
  $(document).on('change', '.slug', function(e) {
    console.log(e);
    this.value = this.value.toLowerCase().replace(/\s/g, '-');
  });

  /** ATTIVAZIONE CK EDITOR **/

  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    var $tabPane = $($(e.target).data('target'));
    $tabPane.find('.ck').each(function() {
      attivaCkEditor(this);
    });
  });

  /** Aggiunge le istanze di ckeditor per la tab attiva al caricamento **/
  if ($('.tab-pane.active').length) {
    var $tabPane = $('.tab-pane.active');
    $tabPane.find('.ck').each(function() {
      attivaCkEditor(this);
    });
  }

  /* Immagine di elenco - inizio */

  $('#upload_list').on('change', function() {
    var dimensioni = $(this).closest('.panelImg').data('dimensions');
    readFileB64(this, dimensioni, 'elenchi');
  });

  $('.btn-delete-list-img').on('click', function(e) {
    e.preventDefault();
    var opts = {
      'type': 'danger',
      'titolo': _('Cancellazione immagine elenchi', 'news'),
      'content': _(
          'Vuoi eliminare l\'immagine per gli elenchi?<br /><strong>N.B.<br />L\'operazione sarà irreversibile</strong>'),
      'onOK': function($modal) {
        $('#listImg').find('img').remove();
        $('#listImg').find('.form-group').remove();
        $('#brand_form_listImg').val('');
        $('#brand_form_listImgFileName').val('');
        $('#brands_list_img').find('.missingImage').removeClass('hide');
        $('#brand_form_deleteListImg').val(1);
        $modal.modal('hide');
      },
    };
    HandleBarHelper.confirm(opts);
  });

  /* Immagine di elenco - fine */

  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false,
    };
    cols.push(col);

    col = {
      'title': 'Titolo',
      'className': 'dt2',
      searchable: true,
      data: 'titolo',
    };

    cols.push(col);

    col = {
      'title': 'Creato',
      'className': 'dt6',
      searchable: true,
      data: 'createdAt',
    };

    cols.push(col);

    col = {
      'title': 'Ultima modifica',
      'className': 'dt7',
      searchable: true,
      data: 'updatedAt',
    };

    cols.push(col);
    col = {'className': 'dt-body-center'};

    // placeholder toggle
    cols.push(col);

    // placeholder cancellazione
    cols.push(col);

    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        return '<a href="/admin/' + modulo + '/edit/' + full.id +
            '" title="Modifica record: ' + toString +
            '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      },
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 2,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        var icon = 'check-square-o';
        var title = 'Disabilita: ';
        if (!full.isEnabled) {
          var icon = 'square-o';
          var title = 'Abilita: ';
        }
        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id +
            '" title="' + title + toString +
            '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
        ret += '<i class="fa fa-' + icon + '"></i>';
        ret += '</a>';
        return ret;
      },
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id +
              '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id +
              '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id +
              '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      },
    };

    columnDefs.push(columnDef);

    $('#datatable').dataTable({
      ajax: {
        'url': '/admin/' + modulo + '/json',
      },
      aaSorting: [[2, 'asc']],
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass('danger');
        }
      },
    });

  }

});

/** FINE document.ready **/

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

  var id = textarea.id;

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {

    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' +
      $body.data('seo') + '.js',
    });

  }

}

/**
 * Riceve in ingresso il file selezionato per l'upload
 */
function readFileB64(input, dimensioni, type) {

  if (typeof type == 'undefined') {
    type = 'header';
  }

  console.log(dimensioni);

  var d = dimensioni.split('x');

  if (input.files && input.files[0]) {

    if (input.files[0].size > $body.data('max_upload_file_size')) {

      var opts = {
        'type': 'danger',
        'titolo': _('Dimensioni file non consentite'),
        'content': _(
            'Non posso permetterti di caricare un file cosi grande (%s), <strong>il massimo consentito è %s</strong>',
            'default', humanFileSize(input.files[0].size, true),
            humanFileSize(1000000, true)),
      };
      HandleBarHelper.alert(opts);

    } else {

      var modalTitle;

      if (type == 'header') {

        modalTitle = _('Caricamento immagine di testata', 'news');
        defaultAlt = 'Immagine Header';

      } else {

        modalTitle = _('Caricamento immagine per gli elenchi', 'news');
        defaultAlt = 'Immagine Elenchi';

      }

      var reader = new FileReader();

      reader.onload = function(e) {

        var opts = {
          'id': 'modalCroppie',
          'data': {'title': modalTitle},
          'onShow': function($modal) {

            if (type == 'header') {
              $modal.find('#brand_form_listImgAlt').
                  closest('.form-group').
                  remove();
            } else {
              $modal.find('#brand_form_headerImgAlt').
                  closest('.form-group').
                  remove();
            }

            var w = d[0] / 2;
            var h = d[1] / 2;

            var alt = '';

            $('.titolo').each(function() {
              if ($.trim($(this).val()) != '') {
                alt += $.trim($(this).val());
                alt += ': ';
                return false;
              }
            });

            alt += defaultAlt;

            $modal.find('input[type="text"]').val(alt);

            $('.croppie').height(h + h * (20 / 100));

            var croppieOpts = {
              viewport: {
                width: w,
                height: h,
                type: 'square',
              },
              boundary: {
                width: w,
                height: h,
              },
              enableExif: true,
            };

            console.log(croppieOpts);

            var $croppie = $modal.find('.croppie').croppie(croppieOpts);

            $croppie.croppie('bind', {
              url: e.target.result,
            });

            $modal.find('.btn-cancel').on('click', function(e) {
              $modal.modal('hide');
            });

            $modal.find('.btn-confirm').on('click', function(e) {
              e.preventDefault();
              $croppie.croppie('result', {
                type: 'base64',
                size: {width: d[0], height: d[1]},
                format: 'jpeg',
              }).then(function(resp) {

                var AltImpostato = $modal.find('#alt').val();

                if (type == 'header') {
                  $('#brand_form_headerImg').val(resp);
                  var $headerImg = $('#headerImg');
                  $headerImg.find('.missingImage').addClass('hide');
                  $headerImg.find('.img-responsive').remove();
                  $('#brand_form_deleteHeaderImg').val('0');
                  $('.btn-delete-header-img').removeClass('disabled');
                  $('<img class="img-responsive" src="' + resp + '" />').
                      prependTo($('#headerImg'));
                  $('#brand_form_headerImgAlt').
                      closest('.form-group').
                      parent().
                      removeClass('hide');
                  $('#brand_form_headerImgAlt').val(AltImpostato);
                  $('#brand_form_headerImgFileName').val(input.files[0].name);
                } else {
                  $('#brand_form_listImg').val(resp);
                  var $listImg = $('#listImg');
                  $listImg.find('.missingImage').addClass('hide');
                  $listImg.find('.img-responsive').remove();
                  $('#brand_form_deleteListImg').val('0');
                  $('.btn-delete-list-img').removeClass('disabled');
                  $('<img class="img-responsive" src="' + resp + '" />').
                      prependTo($('#listImg'));
                  $('#brand_form_listImgAlt').
                      closest('.form-group').
                      parent().
                      removeClass('hide');
                  $('#brand_form_listImgAlt').val(AltImpostato);
                  $('#brand_form_listImgFileName').val(input.files[0].name);
                }

                $croppie.croppie('destroy');
                $modal.modal('hide');

              });
            });
          },
        };
        HandleBarHelper.modal(opts);
      };

      reader.readAsDataURL(input.files[0]);

    }
  }
  else {
    HandleBarHelper.alert({
      'content': _(
          'Il tuo browser non supporta le funzionalità richieste da questo componente.'),
    });
  }
}


