<?php
// 27/04/17, 11.37
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class GroupFeatureRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('fg')
            ->andWhere('fg.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function countAllNotDeleted()
    {

        $qb = $this->createQueryBuilder('grp');

        $qb->select($qb->expr()->count('grp'))
            ->where('grp.deletedAt is NULL');

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }


}