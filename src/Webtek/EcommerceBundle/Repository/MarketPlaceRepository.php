<?php
// 27/04/17, 14.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MarketPlaceRepository extends EntityRepository
{

    function findAllNotDeleted($onlyActive = false)
    {

        $qb = $this->createQueryBuilder('m')
            ->andWhere('m.deletedAt is NULL');

        if ($onlyActive) {
            $qb->andWhere('m.isEnabled = 1');
        };

        return $qb->getQuery()
            ->execute();
    }

}