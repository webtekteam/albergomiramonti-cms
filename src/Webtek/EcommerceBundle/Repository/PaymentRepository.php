<?php
// 06/06/17, 17.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class PaymentRepository extends EntityRepository
{

    public function getActivePaymentsQB()
    {

        return $this->createQueryBuilder('pagamenti')
            ->andWhere('pagamenti.isEnabled = 1');

    }

    public function getActivePaymentsForDeliveryMethod($Delivery)
    {
        $results = $this->createQueryBuilder('pagamenti')
            ->join("pagamenti.deliveryMethods", "delivery")
            ->andWhere('pagamenti.isEnabled = 1')
            ->andWhere('delivery.id = :delivery')
            ->setParameter("delivery", $Delivery)->getQuery()->execute();

        return $results;

    }

    public function getActivePayments()
    {

        return $this->getActivePaymentsQB()->getQuery()
            ->execute();

    }
}