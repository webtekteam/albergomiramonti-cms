<?php
// 24/04/17, 10.38
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class BrandsRepository extends EntityRepository
{

    function findBySlug($locale, $slug)
    {

        return $this->createQueryBuilder('brands')->andWhere('brands.deletedAt is NULL')->join(
            "brands.translations",
            "brand_trans"
        )->andWhere("brand_trans.slug = :slug")->andWhere("brand_trans.locale = :locale")->setParameter(
            "slug",
            $slug
        )->setParameter("locale", $locale)->getQuery()->getOneOrNullResult();
    }

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('brands')->andWhere('brands.deletedAt is NULL')->getQuery()->execute();
    }

    function findAllNotDeletedAndFormatted($locale, $limit = false, $alfabetico = false, $escludiVuote = false)
    {

        if ($limit === true) {
            $limit = 24; //TODO AGGIUNGERE VARIABILE AI PARAMETRI ECOMMERCE
        }
        $q = $this->createQueryBuilder('brands')->addSelect("bt.titolo,bt.slug")->andWhere(
            'brands.deletedAt is NULL'
        )
            ->andWhere("brands.isEnabled = 1")
            ->join("brands.translations", "bt")->andWhere("bt.locale = :locale")
            ->setParameter("locale", $locale);

        if (!$alfabetico) {
            $q = $q->orderBy(
                "brands.sort",
                "asc"
            );
        } else {
            $q = $q->orderBy(
                "bt.titolo",
                "asc"
            );
        }

        if ($escludiVuote) {
            $q = $q
                ->andWhere("brands IN (
                    SELECT b FROM Webtek\\EcommerceBundle\\Entity\\Brand as b JOIN b.products as products WHERE products.isEnabled =1 AND products.deletedAt IS NULL)");
        }

        if ($limit) {
            $q = $q->setMaxResults($limit);
        }
        return $q->getQuery()->execute();
    }

    function countAllNotDeleted()
    {

        $qb = $this->createQueryBuilder('brands');

        $qb->select($qb->expr()->count('brands'))->where('brands.deletedAt is NULL');

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    function getBySlug($slug, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('brand')->leftJoin(
            'Webtek\EcommerceBundle\Entity\BrandTranslation',
            'brandT',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'brand = brandT.translatable and brandT.locale = :locale'
        )->andWhere('brandT.slug = :slug')->setParameter('slug', $slug)->setParameter('locale', $locale);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();

    }

}
