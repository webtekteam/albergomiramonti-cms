<?php
// 27/05/17, 11.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Webtek\EcommerceBundle\Entity\Coupon;

class CarrelloRepository extends EntityRepository
{

    public function contaCoupons(Coupon $coupon)
    {

        $qb = $this->createQueryBuilder('c')->andWhere('c.coupon = :coupon')->setParameter('coupon', $coupon)->select(
            'count(c.id)'
        );

        return $qb->getQuery()->getSingleScalarResult();

    }

}