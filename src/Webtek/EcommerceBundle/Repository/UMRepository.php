<?php

namespace Webtek\EcommerceBundle\Repository;

use AppBundle\Traits\NotDeleted;
use Doctrine\ORM\EntityRepository;

class UMRepository extends EntityRepository
{

    use NotDeleted;

}