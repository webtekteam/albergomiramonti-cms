<?php
// 27/04/17, 14.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ListinoRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('listino')
            ->andWhere('listino.deletedAt is NULL')
            ->orderBy('listino.isDefault', 'desc')
            ->getQuery()
            ->execute();
    }


    public function setDefault($id)
    {

        $query = $this->createQueryBuilder('listino')
            ->update('WebtekEcommerceBundle:Listino', 'listino')
            ->set('listino.isDefault', 0)
            ->where('listino.id != :id')
            ->setParameter('id', $id)->getQuery();

        $query->execute();

    }

    function countAllNotDeleted()
    {

        $qb = $this->createQueryBuilder('listino');

        $qb->select($qb->expr()->count('listino'))
            ->where('listino.deletedAt is NULL');

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

}