<?php
// 27/04/17, 14.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class AttributeRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('attributes')->andWhere('attributes.deletedAt is NULL')->getQuery()->execute();
    }

}