<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\MarketPlace;

class MarketPlaceHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * MarketPlacesHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $MarketPlaces = $this->entityManager->getRepository('WebtekEcommerceBundle:MarketPlace')->findAll();
        } else {
            $MarketPlaces = $this->entityManager->getRepository('WebtekEcommerceBundle:MarketPlace')->findAllNotDeleted();
        }

        $records = [];

        foreach ($MarketPlaces as $MarketPlace) {


            /**
             * @var $MarketPlace MarketPlace;
             */

            $record = [];
            $record['id'] = $MarketPlace->getId();
            $record['nome'] = $MarketPlace->getNome();
            $record['deleted'] = $MarketPlace->isDeleted();
            $record['isEnabled'] = $MarketPlace->getIsEnabled();
            $record['createdAt'] = $MarketPlace->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $MarketPlace->getUpdatedAt()->format('d/m/Y H:i:s');

            $records[] = $record;
        }

        return $records;

    }

    public function getSimpleArray()
    {

        $MarketPlaces = $this->entityManager->getRepository(
            'WebtekEcommerceBundle:MarketPlace'
        )->findAllNotDeleted(true);

        $data = [];

        foreach ($MarketPlaces as $marketPlace) {

            /**
             * @var $marketPlace MarketPlace;
             */
            $data[$marketPlace->getId()] = $marketPlace->getNome();

        }

        return $data;

    }

}