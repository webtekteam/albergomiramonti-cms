<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 16.24
 */

namespace Webtek\EcommerceBundle\Service;


use AppBundle\Form\LoginForm;
use AppBundle\Service\PathManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class LoginEcommerceFormAuthenticator extends AbstractFormLoginAuthenticator
{

    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;
    /**
     * @var PathManager
     */
    private $pathManager;


    /**
     * LoginFormAuthenticator constructor.
     */
    public function __construct(
        FormFactory $formFactory,
        EntityManager $em,
        PathManager $pathManager,
        UserPasswordEncoder $passwordEncoder
    ) {

        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->pathManager = $pathManager;
    }

    public function getCredentials(Request $request)
    {

        $isLoginSubmit = $request->isMethod('POST');
        if (!$isLoginSubmit) {
            return null;
        }

        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);
        $data = $form->getData();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_username']
        );

        return $data;


    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['_username'];

        $user = $this->em->getRepository('AppBundle:User')->findOneBy(['username' => $username]);

        if ($user && $user->getIsEnabled()) {
            return $user;
        }

        return null;

    }

    public function checkCredentials($credentials, UserInterface $user)
    {

        $password = $credentials['_password'];

        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            return true;
        }

        return false;

    }

    protected function getLoginUrl()
    {
//        Nel caso un utente non abbia i permessi per entrare nel pannello (e quindi non è logato) rediredcto alla home
        return $this->pathManager->generateUrl('home');
//        return $this->pathManager->generateUrl('security_login');

    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->getSession() instanceof SessionInterface) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        }

        $url = $request->headers->get('referer');

        return new RedirectResponse($url);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        // if the user hit a secure page and start() was called, this was
        // the URL they were on, and probably where you want to redirect to
//        $targetPath = false;
//        if ($request->getSession() instanceof SessionInterface) {
//            $targetPath = $request->getSession()->get('_security.' . $providerKey . '.target_path');
//        }

//        if (!$targetPath) {
//            $targetPath = $this->pathManager->generateUrl('checkout');
//        }

        return new RedirectResponse($request->headers->get('referer'));

    }

    public function getDefaultSuccessRedirectUrl()
    {
        return $this->pathManager->generateUrl('checkout');

    }

}