<?php

namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\OrdersStatus;

class OrdersStatusHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * OrdersStatussHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $OrdersStatuss = $this->entityManager->getRepository('WebtekEcommerceBundle:OrdersStatus')->findAll();
        } else {
            $OrdersStatuss = $this->entityManager->getRepository(
                'WebtekEcommerceBundle:OrdersStatus'
            )->findAllNotDeleted();
        }

        $records = [];

        foreach ($OrdersStatuss as $OrdersStatus) {


            /**
             * @var $OrdersStatus OrdersStatus;
             */

            $record = [];
            $record['id'] = $OrdersStatus->getId();
            $record['titolo'] = $OrdersStatus->translate()->getTitolo();
            $record['codice'] = $OrdersStatus->getCodice();
            $record['deleted'] = $OrdersStatus->isDeleted();

            $records[] = $record;
        }

        return $records;

    }


}