<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 19/07/17
 * Time: 15.33
 */

namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;

class SalesCounter
{
    private $entitiManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function countSales()
    {
        $select = $this->entityManager
            ->createQueryBuilder()
            ->select("COUNT(p.id) as vendite")
            ->addSelect("product.id")
            ->from("WebtekEcommerceBundle:RecordOrder", "p")
            ->join("p.Order", "ordine")
            ->join("p.product", "product")
            ->andWhere("ordine.paymentDate IS NOT NULL")
            ->groupBy("p.product")
            ->getQuery()->getResult();
        foreach ($select as $s) {
            $Prod = $this->entityManager->getRepository("WebtekEcommerceBundle:Product")->find($s['id']);
            $Prod->setSaleCounter($s['vendite']);
            $this->entityManager->persist($Prod);
        }
        $this->entityManager->flush();
    }

}