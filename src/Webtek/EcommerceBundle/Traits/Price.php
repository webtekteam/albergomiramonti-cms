<?php
// 18/01/17, 11.05
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait Price
{


    /**
     * @ORM\Column(type="decimal", precision=10,scale=2)
     * @Assert\NotBlank()
     */
    protected $prezzo;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Tax")
     */
    protected $codiceIva;

    /**
     * @ORM\Column(type="decimal", precision=10,scale=4)
     */
    protected $imposta;

    /**
     * @ORM\Column(type="decimal", precision=10,scale=2)
     */
    protected $prezzoTassato;


    public function setPrezzo($prezzo)
    {

        $this->prezzo = round($prezzo, 2);

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return string
     */
    public function getPrezzo()
    {

        return $this->prezzo;
    }

    /**
     * @return mixed
     */
    public function getCodiceIva()
    {

        return $this->codiceIva;
    }

    /**
     * @param mixed $codiceIva
     */
    public function setCodiceIva($codiceIva)
    {

        $this->codiceIva = $codiceIva;
    }

    /**
     * @return mixed
     */
    public function getImposta()
    {

        return $this->imposta;
    }

    /**
     * @param mixed $imposta
     */
    public function setImposta($imposta)
    {

        $this->imposta = round($imposta, 4);
    }

    /**
     * @return mixed
     */
    public function getPrezzoTassato()
    {

        return $this->prezzoTassato;
    }

    /**
     * @param mixed $prezzoTassato
     */
    public function setPrezzoTassato($prezzoTassato)
    {

        $this->prezzoTassato = round($prezzoTassato, 2);
    }

}
