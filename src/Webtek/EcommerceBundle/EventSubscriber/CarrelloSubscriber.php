<?php
// 18/09/17, 7.41
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\EventSubscriber;

use AppBundle\Entity\User;
use AppBundle\Service\EmailTemplateHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Event\CarrelloAddEvent;

class CarrelloSubscriber implements EventSubscriberInterface
{

    /**
     * @var EmailTemplateHelper
     */
    private $emailTemplateHelper;
    /**
     * Impostazioni derivanti dal pannello amministrativo
     *
     * @var $parametriEmails
     */
    private $parametriEmails;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * CarrelloSubscriber constructor.
     *
     * @param EmailTemplateHelper $emailTemplateHelper
     */
    public function __construct(TokenStorage $tokenStorage, EmailTemplateHelper $emailTemplateHelper, $parametriEmails)
    {

        $this->emailTemplateHelper = $emailTemplateHelper;
        $this->parametriEmails = $parametriEmails;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {

        $events = [
            CarrelloAddEvent::NAME => 'onCarrelloAdd',
        ];

        return $events;

    }

    /**
     * Metodo per l'invio dell'email di aggiunta al carrello
     *
     * Al momento dell'aggiunta al carrello viene creata un'email da mandare al gestore del sito con i dati del prodotto aggiunto al carrello
     *
     * @param CarrelloAddEvent $event
     */
    public function onCarrelloAdd(CarrelloAddEvent $event)
    {

        /**
         * @var $Carrello Carrello
         */
        $Product = $event->getProduct();
        $Variante = $event->getVarianteProdotto();
        $User = $this->tokenStorage->getToken()->getUser();
        $to = explode(';', $this->parametriEmails['alert_azioni_utente']);
        $to = array_map('trim', $to);
        $to = array_filter($to);
        $replace = [
            'NOME' => (string)$User,
            'PRODOTTO' => (string)$Product,
            'VARIANTE' => (string)$Variante,
        ];
        $this->emailTemplateHelper->send(
            'ECOMMERCE_CARRELLO',
            $to,
            $replace,
            'it',
            false,
            [],
            'ordini'
        );

    }

}