<?php
// 09/06/17, 15.11
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use Webtek\EcommerceBundle\Entity\Order;

class OrderFormatter extends DataFormatter
{


    /**
     * @var $Order Order
     */
    private $Order = null;


    public function getData()
    {

        $data = [];
        $data['Order'] = $this->Order;

        return $data;
    }

    public function extractData()
    {

        if ($this->request->query->has('order-id') && $this->request->query->has('transactionId')) {

            $this->Order = $this->em->getRepository('WebtekEcommerceBundle:Order')->findOneBy(
                [
                    'id' => $this->request->query->get('order-id'),
                    'transactionId' => $this->request->query->get('transactionId'),
                ]
            );

        }

    }


}