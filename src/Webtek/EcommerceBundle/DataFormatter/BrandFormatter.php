<?php
// 25/05/17, 17.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use Webtek\EcommerceBundle\Entity\Brand;
use AppBundle\Entity\Attachment;

class BrandFormatter extends DataFormatter
{


    public function getProducts()
    {
        $data = [];
        $limit = false;
        $brand = null;
        if (isset($this->data[$this->locale]['limit']['val']) && $this->data[$this->locale]['limit']['val'] != "") {
            $limit = intval($this->data[$this->locale]['limit']['val']);
        }
        if (isset($this->data[$this->locale]['brand']['val']) && $this->data[$this->locale]['brand']['val'] != "") {
            $brand = $this->data[$this->locale]['brand']['val'];
        }
        if (isset($brand)) {


            $brand_entity = $this->em->getRepository("WebtekEcommerceBundle:Brand")->findBySlug("it", $brand);
            $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->findByBrand($this->locale, $brand,
                $limit, true);
            if ($products) {
                $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlistArray($products);

            }
            $data['prodotti'] = $products;
            $data['brand'] = $brand_entity;
            return $data;
        }
        return null;

    }

    public function getBrandInController()
    {
        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Brand) {
            return [
                "nome" => $this->AdditionalData['Entity']->translate()->getTitolo()
            ];
        }
    }

    public function getProductsInController()
    {
        $returnData = [];
        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Brand) {
            $paginator = $this->container->get('knp_paginator');
            $brandHelper = $this->container->get('app.webtek_ecommerce.services.brand_helper');
            $translator = $this->container->get('translator');

            $next = false;
            $prev = false;

            $currentPage = $this->request->query->getInt('page', 1);
            $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];

            /**
             * @var $Category Category
             */
            $Brand = $this->AdditionalData['Entity'];
            $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];


            $order = $this->request->get("orderby");
            $dir = $this->request->get("dir");


            $prodotti = $this->em->getRepository("WebtekEcommerceBundle:Product")->findByBrand($this->locale,
                $Brand->translate($this->locale)->getSlug(), false, false, false, $order, $dir);


            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlistArray($prodotti);
            $pagination = $paginator->paginate(
                $prodotti,
                $currentPage,
                $elementiPerPagina
            );

            $paginationData = $pagination->getPaginationData();

            if ($paginationData['current'] > 1) {

                $prev = $this->container->get("app.path_manager")->generateUrl("brand_ecommerce",
                    ["slug" => $Brand->translate($this->locale)->getSlug(), "page" => $paginationData['current'] - 1]);
            }
            if ($paginationData['current'] < $paginationData['last']) {

                $next = $this->container->get("app.path_manager")->generateUrl("brand_ecommerce",
                    ["slug" => $Brand->translate($this->locale)->getSlug(), "page" => $paginationData['current'] + 1]);


            }

            $returnData['META']['prev'] = $prev;
            $returnData['META']['next'] = $next;
            $returnData['pagination'] = $pagination;
            $returnData['page'] = $currentPage;
            $returnData['brand'] = $Brand->getId();
            $returnData['nome'] = $Brand->translate()->getTitolo();
            $returnData['testo'] = $Brand->translate()->getTesto();

        }

        return $returnData;

    }

//    public function getProductsInController()
//    {
//        $Brand = $this->AdditionalData['Entity'];
//        $data['prodotti'] = $this->em->getRepository("WebtekEcommerceBundle:Product")->findByBrand($this->locale, $Brand->translate($this->locale)->getSlug());
//        $data['brand'] = $Brand;
//        return $data;
//    }


    public function getData()
    {
        $data = [];
        $limit = false;
        $escludi_vuote = 0;
        if (isset($this->data[$this->locale]['escludi_vuote']['val']) && $this->data[$this->locale]['escludi_vuote']['val'] != "") {
            $escludi_vuote = intval($this->data[$this->locale]['escludi_vuote']['val']);
        }
        if ($this->data[$this->locale]['limit']['val'] != "") {
            $limit = intval($this->data[$this->locale]['limit']['val']);
        }
        $data['brands'] = $this->em->getRepository("WebtekEcommerceBundle:Brand")->findAllNotDeletedAndFormatted($this->locale,
            $limit, false, $escludi_vuote);
        return $data;

    }


    public function extractData()
    {

    }
}