<?php
/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace Webtek\EcommerceBundle\Controller\Admin;

use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\UM;
use Webtek\EcommerceBundle\Form\UMForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_UM')")
 */
class UMController extends Controller
{

    /**
     * @Route("/um", name="um")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/um/list.html.twig');
    }

    /**
     * @Route("/um/json", name="um_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.um_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);
    }

    /**
     * @Route("/um/new", name="um_new")
     * @Route("/um/edit/{id}",  name="um_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $UM = new UM();
        $id = null;
        if ($request->get('id')) {
            $id = $request->get('id');
            $UM = $em->getRepository('WebtekEcommerceBundle:UM')->findOneBy(['id' => $id]);
            if (!$UM) {
                return $this->redirectToRoute('um_new');
            }
        }
        $form = $this->createForm(
            UMForm::class,
            $UM
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UM
             */
            $UM = $form->getData();
            $new = false;
            if (!$UM->getId()) {
                $new = true;
            }
            $em->persist($UM);
            $em->flush();
            $elemento = (string)$UM->getLabel();
            if (!$new) {
                $this->addFlash(
                    'success',
                    'UM "' . $elemento . '" ' . $translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'UM "' . $elemento . '" ' . $translator->trans('default.labels.creato'));
            }

            return $this->redirectToRoute('um');
        }
        $errors = $this->get('app.form_error_helper')->getErrors($form);
        $view = '@WebtekEcommerce/admin/um/new.html.twig';
        if ($UM->getId()) {
            $view = '@WebtekEcommerce/admin/um/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'errors' => $errors]);
    }

    /**
     * @Route("/um/toggle-enabled/{id}", name="um_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, UM $UM)
    {

        $elemento = '"' . $UM->getLabel() . '"';
        $em = $this->getDoctrine()->getManager();
        $flash = 'UM ' . $elemento . ' ';
        if ($UM->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }
        $UM->setIsEnabled(!$UM->getIsEnabled());
        $em->persist($UM);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('um');
    }

    /**
     * @Route("/um/delete/{id}/{force}", name="um_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, UM $UM)
    {

        if ($request->get('id')) {
            $em = $this->getDoctrine()->getManager();
            $UM = $em->getRepository('WebtekEcommerceBundle:UM')->findOneBy(['id' => $request->get('id')]);
            if ($UM) {
                $elemento = $UM->getLabel();
                $translator = $this->get('translator');
                $em = $this->getDoctrine()->getManager();
                if ($UM->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && 1 == $request->get(
                        'force'
                    )) {
                    // initiate an array for the removed listeners
                    $originalEventListeners = [];
                    // cycle through all registered event listeners
                    foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                        foreach ($listeners as $listener) {
                            if ($listener instanceof SoftDeletableSubscriber) {
                                // store the event listener, that gets removed
                                $originalEventListeners[$eventName] = $listener;
                                // remove the SoftDeletableSubscriber event listener
                                $em->getEventManager()->removeEventListener($eventName, $listener);
                            }
                        }
                    }
                    // remove the entity
                    $em->remove($UM);
                    try {
                        $em->flush();
                        $translator = $this->get('translator');
                        $this->addFlash(
                            'success',
                            'UM "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                        );
                    } catch (ForeignKeyConstraintViolationException $e) {
                        $this->addFlash(
                            'error',
                            'UM "' . $elemento . '" ' . $translator->trans('categoria_vetrina.errors.non_cancellabile')
                        );
                    }
                } elseif (!$UM->isDeleted()) {
                    $translator = $this->get('translator');
                    $this->addFlash(
                        'success',
                        'UM "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                    );
                    $em->remove($UM);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('um');
    }

    /**
     * @Route("/um/restore/{id}", name="um_restore")
     */
    public function restoreAction(Request $request, UM $UM)
    {

        if ($request->get('id')) {
            $em = $this->getDoctrine()->getManager();
            $UM = $em->getRepository('WebtekEcommerceBundle:UM')->findOneBy(['id' => $request->get('id')]);
            if ($UM->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {
                $em = $this->getDoctrine()->getManager();
                $elemento = $UM->getLabel() . ' (' . $UM->getId() . ')';
                $UM->restore();
                $em->flush();
                $translator = $this->get('translator');
                $this->addFlash('success',
                    'UM "' . $elemento . '" ' . $translator->trans('default.labels.ripristinata'));
            }
        }

        return $this->redirectToRoute('um');
    }
}
