<?php
// 02/10/17, 11.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_FAST_PRODUCTS')")
 */
class MagazzinoPrezziController extends Controller
{

    /**
     * @Route("/magazzino", name="magazzino")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/magazzino/magazzino.html.twig');

    }

    /**
     * @Route("/magazzino/json", name="magazzino_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.magazzino_helper')->getList();

        return new JsonResponse($return);

    }

    /**
     * @Route("/magazzino/save", name="magazzino_save")
     */
    public function save(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $prodotti = $request->request->get('prodotti');
        if (count($prodotti)) {
            foreach ($prodotti as $prodotto) {
                /**
                 * @var $Prodotto Product
                 */
                $Prodotto = $em->getRepository('WebtekEcommerceBundle:Product')->findOneBy(['id' => $prodotto['id']]);
                $Prodotto->setGiacenza($prodotto['giacenza']);
                $em->persist($Prodotto);
            }
        }
        $varianti = $request->request->get('varianti');
        if (count($varianti)) {
            foreach ($varianti as $variante) {
                /**
                 * @var $VarianteProdotto VarianteProdotto
                 */
                $VarianteProdotto = $em->getRepository('WebtekEcommerceBundle:VarianteProdotto')->findOneBy(
                    ['id' => $variante['id']]
                );
                $VarianteProdotto->setGiacenza($variante['giacenza']);
                $em->persist($VarianteProdotto);
            }
        }
        $em->flush();
        $return = [];
        $return['result'] = true;

        return new JsonResponse($return);

    }


}