<?php

namespace Webtek\EcommerceBundle\Controller\Utility;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/*
 * QUESTO CONTROLLER SARA' CHIAMATO DA UN CRONJOB E PERMETTE DI CALCOLARE LE VENDITE DEI PRODOTTI
 */

class SalesCounterController extends Controller
{
    public function countAction()
    {
        $this->get("app.webtek_ecommerce.services.sales_counter")->countSales();
        return new Response("OK!");
    }
}
