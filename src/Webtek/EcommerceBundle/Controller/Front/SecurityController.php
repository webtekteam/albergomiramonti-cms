<?php
// 05/06/17, 10.05
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Front;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{

    /**
     * @Route("/ecommerce/logout", name="security_ecommerce_logout")
     */
    public function logoutAction()
    {

        throw new \Exception('qui non ci devo arrivare');

    }

    /**
     * @Route("/ecommerce/lost-pass", name="lost_password_ecommerce")
     */
    public function lostPassAction(Request $request)
    {

        $Languages = $this->get('app.languages');

        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();


        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];

        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);

        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }

            $META['alternate'][$key] = $pathManager->generateUrl(
                'carrello_list',
                [],
                $sigla
            );
        }

        $AdditionalData['META'] = $META;

        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');

        $twigs = $TemplateLoader->getTwigs(
            'lost_password',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );

        $META = $MetaManager->merge($twigs, $META);

        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];

            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'lost-password',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'lost-password', $META, $metaVars);
            }
        }

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);


    }

//    /**
//     * @Route("ecommerce/reset-pass", name="operatori_reset_pass")
//     */
//    public function resetPassAction(Request $request)
//    {
//
//        $error = false;
//
//        $form = $this->createForm(
//            ResetPassForm::class,
//            [
//                'uuid' => $request->query->get('uuid'),
//                'code' => $request->query->get('code'),
//            ]
//        );
//
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//
//            $data = $form->getData();
//
//            $em = $this->getDoctrine()->getManager();
//
//            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $data['uuid']]);
//
//            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $data['code']) {
//
//                $User->setPlainPassword($data['plainPassword']);
//
//                $em->persist($User);
//                $em->flush();
//
//                $this->addFlash('success', $this->get('translator')->trans('operatori.messages.password_resettata'));
//
//                return $this->get('security.authentication.guard_handler')
//                    ->authenticateUserAndHandleSuccess(
//                        $User,
//                        $request,
//                        $this->get('app.security.login_form_authenticator'),
//                        'main'
//                    );
//
//
//            } else {
//
//                $this->addFlash('error', 'operatori.errors.form_manomesso');
//
//                return $this->redirectToRoute('lost_password');
//
//            }
//
//
//        }
//
//        $uuidConstraint = new Uuid();
//        $errors = $this->get('validator')->validate($request->query->get('uuid'), $uuidConstraint);
//
//
//        if (!count($errors)) {
//
//            $em = $this->getDoctrine()->getManager();
//
//            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $request->query->get('uuid')]);
//
//            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $request->query->get('code')) {
//                return $this->render(
//                    'admin/security/reset_pass.html.twig',
//                    [
//                        'form' => $form->createView(),
//                        'error' => $error,
//                    ]
//                );
//
//
//            } else {
//
//                $this->addFlash('error', 'operatori.errors.url_manomesso');
//
//                return $this->redirectToRoute('lost_password');
//
//            }
//
//        } else {
//            $this->addFlash('error', 'operatori.errors.url_manomesso');
//
//            return $this->redirectToRoute('lost_password');
//        }
//
//
//    }


}