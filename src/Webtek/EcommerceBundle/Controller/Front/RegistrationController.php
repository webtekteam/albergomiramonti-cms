<?php

namespace Webtek\EcommerceBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Webtek\EcommerceBundle\Service\MetaManager;

class RegistrationController extends Controller
{


    /**
     * @Route("/registrazione", defaults={"_locale"="it"}, name="registrazione_it")
     * @Route("/{_locale}/registrazione", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="registrazione")
     */
    public function registrationAction(Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            $Languages = $this->get('app.languages');

            $META = [];
            $META['title'] = '';
            $META['description'] = '';
            $META['robots'] = 'noindex';
            $META['alternate'] = [];
            $AdditionalData = [];
            $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
            $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
            $linguePreferite = explode(',', $linguePreferite);
            $pathManager = $this->get('app.path_manager');

            foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                $key = $sigla;
                if ($key == $linguePreferite[0]) {
                    $key = 'x-default';
                }
                $META['alternate'][$key] = $pathManager->generateUrl(
                    'registrazione',
                    [],
                    $sigla
                );
            }


            $AdditionalData['META'] = $META;

            $TemplateLoader = $this->get('app.template_loader');
            $MetaManager = $this->get('app.meta_manager');

            $twigs = $TemplateLoader->getTwigs(
                'registrazione',
                $Languages->getActivePublicLanguages(),
                $AdditionalData
            );

            $META = $MetaManager->merge($twigs, $META);

            if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

                $metaVars = [];

                if (!isset($META['description']) || !$META['description']) {
                    $META = $MetaManager->manageDefaults(
                        'description',
                        'registrazione',
                        $META,
                        $metaVars
                    );
                }
                if (!isset($META['title']) || !$META['title']) {
                    $META = $MetaManager->manageDefaults('title', 'registrazione', $META, $metaVars);
                }
            }
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);
        } else {
            if ($request->getLocale() == "it") {
                return $this->redirectToRoute("home_it");
            } else {
                return $this->redirectToRoute("home", ["locale" => $request->getLocale()]);
            }

        }


    }
}
