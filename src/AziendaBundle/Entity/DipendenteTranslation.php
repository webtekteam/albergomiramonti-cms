<?php

namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="dipendenti_translations")
 */
class DipendenteTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titolo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $mansione;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getMansione()
    {

        return $this->mansione;
    }

    /**
     * @param mixed $mansione
     */
    public function setMansione($mansione)
    {

        $this->mansione = $mansione;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;
    }

}

