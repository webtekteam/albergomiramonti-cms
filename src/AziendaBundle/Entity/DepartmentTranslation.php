<?php

namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="dipartimento_translations")
 */
class DepartmentTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titolo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $link;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {

        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {

        $this->link = $link;
    }


}

