<?php

namespace AziendaBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AziendaBundle\Entity\Dipendente;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use Doctrine\ORM\EntityManager;
use AziendaBundle\Entity\Sede;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class SedeForm extends AbstractType
{

    public function __construct(
        AuthorizationChecker $auth,
        EntityManager $em
    ) {

        $this->auth = $auth;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'pages.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add('headerImg', FileType::class);
        $builder->add('headerImgData', HiddenType::class);
        $builder->add('headerImgAlt', TextType::class, []);
        $builder->add('headerImgDelete', HiddenType::class, []);

        $builder->add('nome', TextType::class, ['label' => 'sedi.labels.nome']);
        $builder->add('sort', TextType::class, ['label' => 'sedi.labels.ordine']);
        $builder->add('codice', TextType::class, ['label' => 'sedi.labels.codice']);
        $builder->add('email', TextType::class, ['label' => 'sedi.labels.email', 'required' => false]);
        $builder->add('telefono', TextType::class, ['label' => 'sedi.labels.telefono', 'required' => false]);
        $builder->add('fax', TextType::class, ['label' => 'sedi.labels.fax', 'required' => false]);
        $builder->add('indirizzo', TextType::class, ['label' => 'sedi.labels.indirizzo']);
        $builder->add('indirizzo2', TextType::class, ['label' => 'sedi.labels.indirizzo2']);
        $builder->add('coordinate', HiddenType::class, ['attr' => ['class' => 'coordinate']]);
        $builder->add('zoom', HiddenType::class, ['attr' => ['class' => 'zoom']]);

        $builder->add(
            'dipendenti',
            EntityType::class,
            [
                'class' => Dipendente::class,
                'multiple' => true,
                'by_reference' => false,
                'expanded' => true,
            ],
            ['label' => 'dipendenti.labels.dipendenti']

        );


        $fields = [];
        $excluded_fields = [];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug', 'shortUrl'];
        } else {

            $fields = [
                'metaTitle' => [
                    'label' => 'default.labels.meta_title',
                    'required' => false,
                    'field_type' => SeoTitle::class,
                ],
                'metaDescription' => [
                    'label' => 'default.labels.meta_description',
                    'required' => false,
                    'field_type' => SeoDescription::class,
                ],
                'slug' => [
                    'label' => 'default.labels.slug',
                    'required' => false,
                ],
            ];

        }

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );

        $builder->add(
            'departments',
            CollectionType::class,
            [
                'entry_type' => DepartmentForm::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
                'empty_data' => function (FormInterface $form) {

                    return new \Doctrine\Common\Collections\ArrayCollection();
                },
            ]
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Sede::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }

}