<?php
// 29/06/17, 12.19
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Form;


use AziendaBundle\Entity\Orario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrarioForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('etichetta', TextType::class, ['label' => 'orari.labels.etichetta', 'required' => false]);
        $builder->add(
            'tipo',
            ChoiceType::class,
            [
                'label' => 'orari.labels.tipo',
                'choices' => [
                    'orari.labels.aperto' => 'APERTO',
                    'orari.labels.chiuso' => 'CHIUSO',
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add('dalle', TextType::class, ['label' => 'orari.labels.dalle', 'required' => false]);
        $builder->add('alle', TextType::class, ['label' => 'orari.labels.alle', 'required' => false]);
        $builder->add('sort', HiddenType::class, ['attr' => ['class' => 'sort']]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Orario::class,
                'error_bubbling' => true,
            ]
        );
    }

}